#ifndef __LIBUTIL_H__
#define __LIBUTIL_H__

#include <stdio.h>

int     find_wordorder  	 (char*);
void    normalize            (char *,int);

/**\fn int file_exist_in_pz(char *path)
 *
 * Say if a path (relative to $PZ) is a regular file.
 *
 * \param path The relative path according to $PZ
 *
 * \return TRUE if path is a regular file and FALSE otherwise.
 *
 */
int file_exist_in_pz(char *path);

/**\fn void *mem(size_t size)
 *
 * \brief Allocate size bytes.
 *
 * \param size The size of the newly allocated memory
 *
 * \return The allocate memory.
 */
void   *mem(size_t);

/**\fn int strlen_utf8(char *str)
 *
 * \brief Compute size of UTF-8 string
 *
 * \param str An UTF-8 string
 *
 * \return The size of the given UTF-8 string
 */
int strlen_utf8(char *str);

/**\fn char *strcut_utf8(char *str, int maxsize)
 *
 * \brief Cut an UTF-8 string according to given maximum size
 *
 * \param str     The string
 * \param maxsize The maximum allowed size in bytes
 *
 * \return A valid UTF-* string with a maximum length (bytes) of maxsize.
 */
char *strcut_utf8(char *str, int maxsize);

/**\fn void *mem_realloc(void *old,size_t new_size);
 *
 * \brief Change allocated memory size.
 *
 * \param old      The currently allocated memory
 * \param new_size The new memory size
 *
 * \return The new allocated memory
 *
 */
void *mem_realloc(void *old,size_t new_size);

/**\fn char *get_pz_dir()
 *
 * \brief Give the the path of Poles and Zeros directory (Read the PZdir environment variable).
 *
 * The directory contains all files used to generate seed dataless files.
 *
 * \return The path to PZdir
 *
 */
char *get_pz_dir();

/**\fn void set_seed_organization(char *organization)
 *
 * \brief Set the description of the organization generating the seed dataless.
 *
 * \param organization The description of the organization generating the seed dataless.
 */
void set_seed_organization(char *organization);

/**\fn char *get_seed_organization()
 *
 * \brief Give the organization generating the seed dataless.
 *
 * \return A description string of the organization generating the seed dataless.
 */
char *get_seed_organization();


/**\fn int isdir(char *path)
 *
 * \brief Test if a path is a directory.
 *
 * \param path The path to test
 *
 * \return TRUE if path is a directory and false otherwise.
 */
int isdir(char *path);
extern const int upper_char;
extern const int lower_char;
extern const int digit_char;
extern const int punct_char;
extern const int space_char;
extern const int under_char;

#endif
