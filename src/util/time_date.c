/*\file time_date.c
 *
 */
#define _XOPEN_SOURCE 600
#include <time.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "errors.h"

#ifndef SUN
/**\fn timegm_(struct tm *tm)
 *
 * \brief Convert tm struct to epoch time.
 *
 * \param tm The time structure to convert
 *
 * \return   The epoch time corresponding to the given time structure.
 */
static time_t timegm_(struct tm *tm) {
	time_t ret = 0;

	char *tz;
	tz = getenv("TZ");
	setenv("TZ", "", 1);
	tzset();
	ret = mktime(tm);
	if (tz) {
		setenv("TZ", tz, 1);
	} else {
		unsetenv("TZ");
	}
	tzset();
	return ret;
}
#endif



int str2utime(char *str, double *dbltime) {
#ifndef SUN
	struct tm tm_struct;
	double decimal;
	char *decimal_second;
	char *end_second;

	memset(&tm_struct, 0, sizeof(struct tm));
	decimal_second = strptime(str,"%Y-%m-%dT%H:%M:%SZ",&tm_struct);
	if(decimal_second == NULL) {
		errors("Can't convert %s\n",str);
	}
	*dbltime = timegm_(&tm_struct);
	decimal = strtod(decimal_second,&end_second);
	if(*end_second != '\0') {
		errors("Can't convert %s\n",str);
	}
	*dbltime += decimal;
	return 0;
#else
	static int daysInMonth[12] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30,
			31 };
	char temp[5];
	Tstruct ts;
	int sec;
	int msec;
	int year;
	int month;
	int day;
	double dbsec;

	if (strlen(str) < 23) {
		printf("str2utime1: time string too short: '%s'\n", str);
		*dbltime = 0.;
		return 1;
	}

	sprintf(temp, "%4.4s", &str[0]);
	ts.year = atoi(temp);
	sprintf(temp, "%2.2s", &str[5]);
	ts.month = atoi(temp);
	sprintf(temp, "%2.2s", &str[8]);
	ts.day = atoi(temp);
	sprintf(temp, "%2.2s", &str[11]);
	ts.hour = atoi(temp);
	sprintf(temp, "%2.2s", &str[14]);
	ts.min = atoi(temp);
	sprintf(temp, "%2.2s", &str[17]);
	sec = atoi(temp);
	sprintf(temp, "%3.3s", &str[20]);
	msec = atoi(temp);
	ts.sec = (float) sec + (float) msec / 1000.0;
	/*
	 printf("==== str2utime1: %4d %02d %02d %02d %02d %.4f\n",
	 ts.year, ts.month, ts.day, ts.hour, ts.min, ts.sec);
	 */

	ts.month -= 1;
	dbsec = 0.0;
	for (year = 1970; year < ts.year; year++) {
		if (((year & 3) == 0) && (leap_year(year) == 0)) {
			warning("%s\n", "str2utime1: leap year algo disagree");
		}
		if ((leap_year(year) == 0) && ((year & 3) == 0)) {
			warning("%s\n", "str2utime1: leap year algo disagree");
		}

		if (leap_year(year))
			daysInMonth[1] = 29;
		else
			daysInMonth[1] = 28;
		for (month = 0; month < 12; month++) {
			dbsec += (double) daysInMonth[month] * 86400.0;
		}
	}

	if (leap_year(year))
		daysInMonth[1] = 29;
	else
		daysInMonth[1] = 28;

	for (month = 0; month < ts.month; month++) {
		dbsec += (double) daysInMonth[month] * 86400.0;
	}

	for (day = 1; day < ts.day; day++) {
		dbsec += 86400.0;
	}

	dbsec += (double) ts.hour * 3600.0;
	dbsec += (double) ts.min * 60.0;
	dbsec += (double) ts.sec;

	*dbltime = dbsec;
	return 0;
#endif
}



void epoch2str(double dbltime, char *str) {
	char tmp_str[255];
	static struct tm *tms;
	long ltime = (long) dbltime;

	tms = gmtime(&ltime);
	strftime(tmp_str, 255, "%Y,%j,%H:%M:%S", tms);
	sprintf(str, "%s.%04d", tmp_str,
			(short) ((dbltime - (double) ltime) * 10000.0));
}


double asc2dbtime(char *str) {
	char    date_str[30];
	double  dbtime;
	sprintf(date_str,"%s.000",str);
	str2utime(date_str, &dbtime);
	return dbtime;
}


void time_asc0(char *date, char *time, double dbltime) {
	long ltime = (long) dbltime;
	struct tm *tms;
	double msec;
	int year;

	tms = gmtime(&ltime);
	msec = dbltime - (double) ltime;
	year = tms->tm_year + 1900;
	sprintf(date, "%02d.%02d.%04d", tms->tm_mday, tms->tm_mon + 1, year);
	sprintf(time, "%02d:%02d:%02d.%03d", tms->tm_hour, tms->tm_min,
			tms->tm_sec, (int) (1000.0 * msec));
	return;
}

/**\fn void time_asc4(char *str, double dbltime)
 *
 * \brief Convert Epoch time (double) into date and time strings.
 *
 * Output format:  yyyy.mm.dd-hh.mm.ss.mmm
 * Example:        1995.05.14-13.20.35.000
 *
 * \param str     The string used to store result
 * \param dbltime The Epoch time representation
 *
 */
void time_asc4(char *str, double dbltime) {
	static char date[20], time[20];

	time_asc0(date, time, dbltime);
	sprintf(str, "%4.4s.%2.2s.%2.2s-%2.2s.%2.2s.%2.2s.%s", &date[6], &date[3],
			&date[0], &time[0], &time[3], &time[6], &time[9]);
	return;
}

/**\fn void dbt_to_asc(double dbltime, char *str)
 *
 * \brief Convert Epoch time (double) into string representation.
 * The Ouput format have the following form :
 * YYYY,JJJ,HH:MM:SS.ssss
 *
 * YYYY : Year
 * JJJ  : Julien day
 * HH   : Hours in 24 mode
 * MM   : Minutes
 * SS   : seconds
 * ssss : fractional part of second
 *
 *
 * \param dbltime The epoch time to convert
 * \param str     The pointer used to store string
 */
void dbt_to_asc(double dbltime, char *str) {
	static struct tm *tms;
	long ltime = (long) dbltime;

	tms = gmtime(&ltime);

	sprintf(str, "%04d,%03d,%02d:%02d:%02d.%04d", tms->tm_year + 1900,
			tms->tm_yday + 1, tms->tm_hour, tms->tm_min, tms->tm_sec,
			(short) ((dbltime - (double) ltime) * 10000.0));
}


