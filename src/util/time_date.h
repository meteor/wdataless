/*
 * time.h
 *
 *  Created on: 13 sept. 2011
 *      Author: leon
 */

#ifndef TIME_H_
#define TIME_H_

/**\fn int str2utime(char *str, double *dbltime)
 *
 * \brief Convert time string to epoch time.
 * The format of the time string is yyyy.mm.dd-hh.mm.ss.sss.
 *
 * \param str     The time string to convert.
 * \param dbltime A pointer to store result as double.
 *
 * \return 0 if no errors and -1 otherwise.
 */
int str2utime(char *str, double *dbltime);

/**\fn double asc2dbtime(char *str)
 *
 * \brief convert a string date (Format : 1999.11.26-12:00:00) to double value.
 *
 * \param str The string date
 *
 * \return The epoch time as double.
 *
 */
double asc2dbtime(char *str);

/**\fn void time_asc0(char *date, char *time, double dbltime)
 *
 * \brief Convert Epoch time (double) into date and time strings
 *        (SISMALP format).
 *
 * Output format: DATE: dd.mm.yyyy  TIME: hh.mm.ss.mmm
 * Example:             14.05.1995        13:20:35.000
 *
 * \param date    The string used to store date part
 * \param time    The string used to store time part
 * \param dbltime The Epoch time representation
 *
 */
void time_asc0(char *date, char *time, double dbltime);

/**\fn void time_asc4(char *str, double dbltime)
 *
 * \brief Convert Epoch time (double) into date and time strings.
 *
 * Output format:  yyyy.mm.dd-hh.mm.ss.mmm
 * Example:        1995.05.14-13.20.35.000
 *
 * \param str     The string used to store result
 * \param dbltime The Epoch time representation
 *
 */
void time_asc4(char *str, double dbltime);

/**\fn void dbt_to_asc(double dbltime, char *str)
 *
 * \brief Convert Epoch time (double) into string representation.
 * The Ouput format have the following form :
 * YYYY,JJJ,HH:MM:SS.ssss
 *
 * YYYY : Year
 * JJJ  : Julien day
 * HH   : Hours in 24 mode
 * MM   : Minutes
 * SS   : seconds
 * ssss : fractional part of second
 *
 *
 * \param dbltime The epoch time to convert
 * \param str     The pointer used to store string
 */
void dbt_to_asc(double dbltime, char *str);

/**\fn void epoch2str(double dbltime, char *str)
 *
 * \brief Convert epoch time to string.
 *
 * Convert epoch time (double) to string, the format of the string is
 * YYYY,DDD,HH:MM:SS.sss where :
 *
 * - YYYY : The year
 * - DDD  : The julian day
 * - HH   : The hours (24)
 * - MM   : The minutes
 * - SS   : The seconds
 * - sss  : Decimal part of seconds
 *
 * You must note that the string must be allocated by calling.
 *
 * \param dbltime The epoch time as double
 * \param str     The output string
 */
void epoch2str(double dbltime, char *str);
#endif /* TIME_H_ */
