/**\file errors.c
 *
 */

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>


#define display_message(filename,function_name, line,pre_message,format,...) va_list ap; \
		fprintf(stderr, "%s %s (%s:%d) : ",pre_message, function_name, filename, line); \
		va_start(ap, format); \
		vfprintf(stderr, format, ap); \
		va_end(ap);


const int DEBUG_DBIRD_PARSER       = 1;
const int DEBUG_STAGE_PARSER       = 2;
const int DEBUG_DATA_STRUCTURE     = 4;
const int DEBUG_COEF_FILE          = 8;
const int DEBUG_SEED_WRITE         = 16;
const int DEBUG_SENSOR_LIST_PARSER = 32;
const int DEBUG_UNIT_LIST_PARSER   = 64;

const int DEBUG_ALL             = 0xffff;


static int debug_mode = 0;
static int verbose_level = 1;

void set_debug_mode(int mode) {
  debug_mode = mode;
}

void set_verbose_level(int level) {
	verbose_level = level;
}

void debug_real(int mode,const char *filename, const char *function_name, int line, char *format,
		...) {
  if(mode & debug_mode) {
    display_message(filename,function_name,line,"Debug in",format,ap);
  }
}

void errors_real(char *filename, const char *function_name, int line, char *format,
		...) {
	display_message(filename,function_name,line,"Error in",format,ap);
	exit(1);
}

void warnings_real(char *filename, const char *function_name, int line, char *format,
		...) {
	if (verbose_level >= 1) {
	  display_message(filename,function_name,line,"Warning in",format,ap);
	}
}

void messages_real(char *filename, const char *function_name, int line, char *format,
		...) {
	if (verbose_level >= 2) {
	  display_message(filename,function_name,line,"Message from",format,ap);
	}
}
