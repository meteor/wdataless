/**\file list.h
 *
 */

#ifndef LIST_H_
#define LIST_H_

typedef struct list_s *list_t;

struct list_s {
	void   *element;
	list_t  next;
};

/**\fn int get_list_size(list_t list)
 *
 * \brief Give the size of a given list.
 *
 * \param list The list
 *
 * \return The size of the given list
 *
 */
int get_list_size(list_t list);

/**\fn list_t append_element(void *new_element,list_t list)
 *
 * \brief Append a new element to a list
 *
 * \param new_element The new element
 * \param list        The head of the list
 *
 * \return The new head of the list
 */
list_t append_element(void *new_element, list_t list);


/**\fn list_t filter(list_t list, int(*fnc)(void *, void *), void *priv)
 *
 *
 * \brief Filter a list according to filter function
 *
 * \param list  The list to filter
 * \param fnc   The filter function
 *              The first argument is the tested element, the
 *              second argument is private data.
 *              An eleement is keep in resultinglist if fnc return is
 *              not zero.
 * \param priv  Private data given as second parameter to filter function
 *
 * \return        The filtred list
 */
list_t filter(list_t list, int(*fnc)(void *, void *), void *priv);

/**\fn void *find_elt(list_t list, int(*comp)(void *, void *), void *value)
 *
 * \brief Search element according to given compraison function.
 *
 * \param list  The list
 * \param comp  The comparaison function
 * \param value The value used to find element
 *
 * \return      The first matching or NULL.
 */
void *find_elt(list_t list, int(*comp)(void *, void *), void *value);

/**\fn void *find_same_elt(list_t list, int(*comp)(void *, void *))
 *
 * \brief Say if each element in the list is unique.
 *
 * \param list -- The list
 * \param comp -- A function return 0 if his argument are equal.
 *
 * return      -- The first element appearing multiples times.
 */
void *find_same_elt(list_t list, int(*comp)(void *, void *));

#endif /* LIST_H_ */
