/**\file  list.c
 *
 */
#include <stdlib.h>
#include "util.h"
#include "list.h"

int get_list_size(list_t list) {
	int size = 0;
	while(list != NULL) {
		list = list->next;
		size++;
	}
	return size;
}


list_t append_element(void *new_element, list_t list) {
	list_t new_node = (list_t) mem(sizeof(struct list_s));
	list_t tmp      = list;
	new_node->element = new_element;
	new_node->next = NULL;
	if(list == NULL)
	  return new_node;
	while(tmp->next != NULL)
	  tmp = tmp->next;
	tmp->next = new_node;
	return list;
}

list_t filter(list_t list, int(*fnc)(void *, void *), void *priv) {
        list_t result = NULL;
        list_t tmp    = list;

        while(tmp != NULL) {
            if (fnc(tmp->element, priv)) {
                result = append_element(tmp->element, result);
            }
            tmp = tmp->next;
        }
        return result;
}

void *find_elt(list_t list, int(*comp)(void *, void *), void *value) {
	list_t current = list;
	while(current != NULL) {
		if (!comp(current->element, value)) {
			return current->element;
		}
                current = current->next;
	}
	return NULL;
}

void *find_same_elt(list_t list, int(*comp)(void *, void *)) {

  list_t current_ref = list;
  list_t current;

  while(current_ref) {
    current = current_ref->next;
    while(current) {
      if (!comp(current_ref->element, current->element)) {
        return current_ref->element;
      }
      current = current->next;
    }
    current_ref = current_ref->next;
  }
  return NULL;
}
