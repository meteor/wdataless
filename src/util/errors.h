/**\file errors.h
 *
 */

#ifndef ERROR_H_
#define ERROR_H_


/**\fn void errors_real(char *filename,const char *function_name,int line,char *format,...)
 *
 * Print an error message and exit.
 *
 *
 * \param filename      The file where is defined the function
 * \param function_name The function raising the error
 * \param line          The line number where error occur
 * \param format        The error message using printf format
 * \param ...           Additional parameters
 *
 * \warning This method is not designed to be called directly, we must use
 * errors macro instead.
 */
void errors_real(char *filename,const char *function_name,int line,char *format,...);


/**\fn void warnings_real(char *filename,const char *function_name,int line,char *format,...)
 *
 * Print a warning message.
 *
 *
 * \param filename      The file where is defined the function
 * \param function_name The function raising the error
 * \param line          The line number where error occur
 * \param format        The error message using printf format
 * \param ...           Additional parameters
 *
 * \warning This method is not designed to be called directly, we must use
 * warnings macro instead.
 */
void warnings_real(char *filename,const char *function_name,int line,char *format,...);


/**\fn void messages_real(char *filename,const char *function_name,int line,char *format,...)
 *
 * Print a message.
 *
 *
 * \param filename      The file where is defined the function
 * \param function_name The function raising the error
 * \param line          The line number where error occur
 * \param format        The error message using printf format
 * \param ...           Additional parameters
 *
 * \warning This method is not designed to be called directly, we must use
 * messages macro instead.
 */
void messages_real(char *filename,const char *function_name,int line,char *format,...);

/**\fn void debug_real(int mode, char *filename,const char *function_name,int line,char *format,...)
 *
 * Print a message if debug mode is set.
 *
 * \param mode          The mode in which the message is displayed
 * \param filename      The file where is defined the function
 * \param function_name The function raising the error
 * \param line          The line number where error occur
 * \param format        The error message using printf format
 * \param ...           Additional parameters
 *
 * \warning This method is not designed to be called directly, we must use
 * debug macro instead.
 */
void debug_real(int mode,char *filename,const char *function_name,int line,char *format,...);


/**\fn void set_debug_mode(int mode)
 *
 * \brief Set the debuging mode.
 *
 * \param mode The new debuging mode
 */
void set_debug_mode(int mode);

/**\fn void set_verbose_level(int level)
 *
 * \brief Set the verbose level.
 *
 * \param level The new verbose level
 */
void set_verbose_level(int level);

extern const int DEBUG_DBIRD_PARSER;
extern const int DEBUG_STAGE_PARSER;
extern const int DEBUG_SENSOR_LIST_PARSER;
extern const int DEBUG_UNIT_LIST_PARSER;
extern const int DEBUG_DATA_STRUCTURE;
extern const int DEBUG_COEF_FILE;
extern const int DEBUG_SEED_WRITE;

extern const int DEBUG_ALL;

#ifndef NDEBUG

/**\def debug
 *
 * \brief A simple macro that permit to add filename and
 *        line number to error messages.
 */
#define debug(mode,format, ...) debug_real(mode,__FILE__,__FUNCTION__,__LINE__,format,__VA_ARGS__)

#else
#define debug(mode,format, ...)
#endif


/**\def errors
 *
 * \brief A simple macro that permit to add filename and
 *        line number to error messages.
 */
#define errors(format, ...) errors_real(__FILE__,__FUNCTION__,__LINE__,format,__VA_ARGS__)


/**\def warnings
 *
 * \brief A simple macro that permit to add filename and
 *        line number to warning messages.
 *
 */
#define warnings(format, ...) warnings_real(__FILE__,__FUNCTION__,__LINE__,format,__VA_ARGS__)

/**\def messages
 *
 * \brief A simple macro that permit to add filename and
 *        line number to messages.
 */
#define messages(format, ...) messages_real(__FILE__,__FUNCTION__,__LINE__,format,__VA_ARGS__)


#endif /* ERROR_H_ */
