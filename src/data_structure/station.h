/**\file station.h
 *
 */

#ifndef STATION_H_
#define STATION_H_

typedef struct station_s *station_t;

#include "channel.h"
#include "location.h"
#include "sensor.h"
#include "analog_filter.h"
#include "digitizer.h"


/**\var stalist_head
 *
 * \brief The head of station list.
 */
extern station_t stalist_head;

/**\def STALOCID
 *
 * \brief The location code of the location_t used to define the station location.
 */
#define STALOCID "--"

/**\struct station_s
 *
 */
struct station_s {
  char              name[6];        /**< station name; format STA            */
  char              network[3];     /**< network name                        */
  char             *owner;          /**< station owner string                */

  double            beg_utime;      /**< begin valid time:                   */
  double            end_utime;      /**< end valid time:                     */

  list_t            comment;        /**< station and channel comments        */
  int               nlocation;      /**< number of location                  */
  location_t       *location;       /**< array of location                   */

  int               nsensor;        /**< number of sensors                   */
  sensor_t         *sensor;         /**< array of sensor                     */

  int               nanalog_filter; /**< number of amplifier-filter          */
  analog_filter_t  *analog_filter;  /**< array of amplifier-filter           */

  int               ndigitizer;     /**< number of digitizers                */
  digitizer_t      *digitizer;      /**< array of digitizer                  */

  int               ncascade;       /**< number of cascade digital filter    */
  filter_cascade_t *cascade;        /**< array of cascade digital filter     */

  int               nchannel;       /**< number of channels                  */
  channel_t        *channel;        /**< array of channels                   */

  char             *sys;            /**< acquisition system string desc      */
  int               lrec_num;       /**< station blk logical record number   */
  station_t         next;           /**< The next station in the linked list */
};

/**\fn station_t create_station(char *station_code, char *begin_time, char *end_time);
 *
 * \brief Initialize a station according to informations present on the first line of station definition_s.
 *
 * \param station_code -- The code of the station
 * \param begin_time   -- The station begin time
 * \param end_time     -- The station end time
 *
 * \return The newly initialized station
 */
station_t create_station(char *station_code, char *begin_time, char *end_time);


/**\fn sensor_t get_station_sensor(station_t station, char *sensor_label);
 *
 * \brief Retrieve a sensor_t object from a station according to label.
 *
 * \param station      -- The station_t
 * \param sensor_label -- The sensor label
 *
 * \return The sensor_t labelled sensor_label or NULL
 */
sensor_t get_station_sensor(station_t station, char *sensor_label);

/**\fn analog_filter_t get_station_analog_filter(station_t station, char *filter_label);
 *
 * \brief Retrieve an analog_filter_t object from a station according to label.
 *
 * \param station      -- The station_t
 * \param filter_label -- The analog filter label
 *
 * \return The analog_filter_t labelled filter_label or NULL
 */
analog_filter_t get_station_analog_filter(station_t station, char *filter_label);

/**\fn digitizer_t get_station_digitizer(station_t station, char *digitizer_label);
 *
 * \brief Retrieve a digitizer_t object from a station according to label.
 *
 * \param station      -- The station_t
 * \param filter_label -- The digitizer label label
 *
 * \return The digitizer_t labelled digitizer_label or NULL
 */
digitizer_t get_station_digitizer(station_t station, char *digitizer_label);

/**\fn filter_cascade_t get_station_filter_cascade(station_t station, char *cascade_label);
 *
 * \brief Retrieve an filter_cascade_t object from a station according to label.
 *
 * \param station       -- The station_t
 * \param cascade_label -- The cascade label
 *
 * \return The filter_cascade_t labelled cascade_label or NULL
 */
filter_cascade_t get_station_filter_cascade(station_t station, char *cascade_label);


#endif /* STATION_H_ */
