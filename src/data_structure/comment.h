
/**\file comment.h
 *
 * \brief Comment management header file.
 *
 */

#ifndef COMMENT_H_
#define COMMENT_H_

#include "list.h"

typedef enum {Network=1, Station, Channel} comment_type_t;

typedef struct comment_s *comment_t;

struct comment_s {
  char           *label;       /**< The dbird code */
  char           *value;       /**< The comment */
  double         *begin_time;  /**< The comment begin validity date */
  double         *end_time;    /**< The comment end validity date */
  list_t          author;      /**< The comment author list */
  int             seed_key;    /**< The seed lookup key of this comment */
  comment_type_t  type;        /**< The comment type (Network, Station, Channel) */
};

/**\fn int comp_comment(comment_t c1, comment_t c2)
 *
 * \brief Compare comment label with strcmp.
 *
 * \param c1 The first comment
 * \param c2 The second comment
 *
 * \return   0 if comment have same label and a non zero value otherwise.
 */
int comp_comment(comment_t c1, comment_t c2);

/**\fn int have_label(comment_t comment, char *value)
 *
 * \brief Compare comment label with given string.
 *
 * \param comment The comment
 * \param value   The tested string
 *
 * \return        0 if comment have label equal to value and a non zero value otherwise.
 */
int have_label(comment_t comment, char *value);

/**\fn comment_t create_comment(char *value, char *begin_time, char *end_time, list_t author, int lookup_key, comment_type_t type)
 *
 * \brief Create a new author structure.
 *
 * \param label      The dbird code
 * \param value      The comment value
 * \param begin_time The comment validity start time
 * \param end_time   The comment validity end time
 * \param author     The comment author list
 * \param lookup_key The seed lookup key
 * \param type       The comment type (Network, Station or Channel)
 *
 * \return           The new created comment
 */
comment_t create_comment(char *label, char *value, char *begin_time, char *end_time, list_t author, int lookup_key, comment_type_t type);

#endif
