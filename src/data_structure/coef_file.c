/**\file coef_file.h
 *
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include "main.h"
#include "parse.h"
#include "util.h"
#include "errors.h"
#include "coef_file.h"

/**\def MAXTOK
 *
 * \brief The maximum number of coefficient per line.
 */
#define MAXTOK 15


/**\fn static int read_coefficient(FILE *Fp, char *line, double *data, int ndata)
 *
 * \brief Read ndata coefficient in file Fp.
 *
 * \param Fp    The FILE use to read data
 * \param line  char pointer used to store read lines
 * \param data  The array used to read data
 * \param ndata The number of coefficient that must be read
 *
 * \return TRUE if ndata coefficients is read and FALSE otherwise
 *
 */
static int read_coefficient(FILE *Fp, char *line, double *data, int ndata) {
  int i, ntoken;
  int nread = 0;
  char *token[MAXTOK];
  int lineno = 0;
  if (ndata == 0) return TRUE;

  do {
    if (getLine(Fp, line, 255, '#', &lineno)) return FALSE;
    ntoken = sparse(line, token, " \t\n", MAXTOK);
    for (i = 0; i < ntoken; i++) {
      data[nread++] = atof(token[i]);
    }
  } while (nread != ndata);

  return TRUE;
}


int read_sensor_poly_coef(char *coefFname, filter_t filter) {
  FILE *Fp;
  char *PZfname;
  char line[255];
  char line_cp[255];
  int ncoef;
  int foundNcoef;
  int err;
  int lineno;
  int ntokens;
  char *token[MAXTOK];

  /* Create absolute filename path */
  PZfname = (char *)mem(strlen(get_pz_dir()) + 1 +  strlen(coefFname) + 1);
  sprintf(PZfname, "%s/%s", get_pz_dir(), coefFname);

  if (!(Fp = fopen(PZfname, "r"))) {
    errors("can't open '%s'\n", PZfname);
  }

  // First find NUMBER_OF_COEFFICIENTS
  foundNcoef = FALSE;
  ncoef = 0;
  lineno = 0;
  while (1) {
    if ((err = getLine(Fp, line, 250, '#', &lineno) == 1)) break;
    sprintf(line_cp, "%s", line);
    ntokens = sparse(line_cp, token, " :=\t\n", MAXTOK);
    ucase(token[0]);

    if (!strcmp(token[0], "NUMBER_OF_COEFFICIENTS")) {
      if (ntokens < 2) {
        errors("missing arg for %s\n",token[0]);
      }
      foundNcoef = TRUE;
      ncoef = atoi(token[1]);
      break;
    }
  }

  if (foundNcoef == FALSE || ncoef == 0) {
    errors("can't find NUMBER_OF_COEFFICIENTS in %s\n", coefFname);
  }
  filter->ncoef = ncoef;
  filter->coef = (double *) mem(ncoef * sizeof(double));
  if (!read_coefficient(Fp, line, filter->coef, filter->ncoef)) {
    errors("%s\n","Can't read coefficient in %s\n", coefFname);
  }

  return TRUE;
}

int read_fir_coef(char *coefFname, firCoef_t *firCoef, int decim, int type) {
  FILE *Fp;
  char line[255];
  char line_cp[255];
  int ncoef;
  int foundNcoef;
  firCoef_t coef;
  int err;
  int lineno;
  int ntokens;
  char *token[MAXTOK];
  char *absolute_path;

  absolute_path = (char *) mem(strlen(get_pz_dir()) + 1 + strlen(coefFname) + 1);
  sprintf(absolute_path, "%s/%s", get_pz_dir(), coefFname);
  if (!(Fp = fopen(absolute_path, "r"))) {
    errors("%s when open %s\n",strerror(errno),coefFname);
  }

  if (decim <= 0) {
    errors("wrong decim value %d\n", decim);
  }

  foundNcoef = FALSE;
  ncoef = 0;
  lineno = 0;
  while (1) {
    if ((err = getLine(Fp, line, 250, '#', &lineno) == 1)) break;
    sprintf(line_cp, "%s", line);
    ntokens = sparse(line_cp, token, " :=\t\n", MAXTOK);
    ucase(token[0]);

    if (!strcmp(token[0], "NUMBER_OF_COEFFICIENTS")) {
      if (ntokens < 2) {
        errors("missing arg for %s\n",token[0]);
      }
      foundNcoef = TRUE;
      ncoef = atoi(token[1]);
      break;
    }
  }

  if (foundNcoef == FALSE || ncoef == 0) {
    errors("can't find num of coef in %s\n", coefFname);
  }

  if (type == FIR_SYM_1) {
    if ((ncoef % 2) != 1) {
      errors("%s\n","ncoef is odd and type not FIR_SYM_1");
    }
  } else if (type == FIR_SYM_2) {
    if ((ncoef % 2) != 0) {
      errors("ncoef is even and type=%d not FIR_SYM_2\n",type);
    }
  } else if (type != FIR_ASYM && type != COMB) {
    errors("%s\n","unsupported FIR coeffs");
  }

  coef = (firCoef_t) mem(sizeof(struct firCoef_s));
  coef->type = type;
  coef->decim = decim;
  coef->ncoef = ncoef;
  coef->coef = (double *) mem(ncoef * sizeof(double));
  if (!read_coefficient(Fp, line, coef->coef, coef->ncoef)) {
    errors("%s\n","rd_coef failed");
  }
  *firCoef = coef;

  fclose(Fp);
  Fp = NULL;
  return TRUE;
}

int read_iir_coef(char *coefFname, filter_t filter) {
  FILE *Fp;
  char line[255];
  char line_cp[255];
  int foundNnumerator;
  int foundNdenominator;
  int err;
  int lineno;
  int ntokens;
  char *token[MAXTOK];
  char *absolute_path;

  absolute_path = (char *) mem(strlen(get_pz_dir()) + 1 + strlen(coefFname) + 1);
  sprintf(absolute_path, "%s/%s", get_pz_dir(), coefFname);
  if (!(Fp = fopen(absolute_path, "r"))) {
    errors("%s when open %s\n",strerror(errno),coefFname);
  }


  foundNnumerator = FALSE;
  foundNdenominator = FALSE;
  lineno = 0;
  while (1) {
    if ((err = getLine(Fp, line, 250, '#', &lineno) == 1)) break;
    sprintf(line_cp, "%s", line);
    ntokens = sparse(line_cp, token, " :=\t\n", MAXTOK);
    ucase(token[0]);

    debug(DEBUG_COEF_FILE, "token[0] = %s\n", token[0]);
    if (!strcasecmp(token[0], "NUMBER_OF_NUMERATORS")) {
    	debug(DEBUG_COEF_FILE, "find %s\n", token[0]);
    	if (ntokens < 2) {
        errors("missing arg for %s\n",token[0]);
      }
      foundNnumerator = TRUE;
      filter->nzero = atoi(token[1]);
      filter->zero = (double *) mem(sizeof(double) * filter->nzero);
      if (!read_coefficient(Fp, line, filter->zero, filter->nzero)) {
        errors("%s\n","rd_coef failed");
      }
    } else if (!strcasecmp(token[0], "NUMBER_OF_DENOMINATORS")) {
    	debug(DEBUG_COEF_FILE, "find %s\n", token[0]);
      if (ntokens < 2) {
        errors("missing arg for %s\n",token[0]);
      }
      foundNdenominator = TRUE;
      filter->npole = atoi(token[1]);
      filter->pole = (double *)mem(sizeof(double) * filter->npole);
      if (!read_coefficient(Fp, line, filter->pole, filter->npole)) {
        errors("%s\n","rd_coef failed");
      }
    }
  }

  if (foundNnumerator == FALSE || filter->nzero== 0 ) {
    errors("can't find number of numerators in %s\n", coefFname);
  }
  if (foundNdenominator == FALSE || filter->nzero == 0) {
    errors("can't find number of denominators in %s\n", coefFname);
  }
  fclose(Fp);
  Fp = NULL;
  return TRUE;
}

int read_poles_and_zeros(char *pz_filename, filter_t filter) {
  FILE *Fp = NULL;
  char PZfname[511];
  char line[255];
  char line_cp[255];
  int np, nz;
  int err;
  int j;
  char *tok2[3];
  int ntok2;
  char line2[255];
  int fileOffset;
  int ntokens;
  char *token[MAXTOK];
  int lineno;

  debug(DEBUG_COEF_FILE,"%s","Begin readPZ\n");
  /* Open the file if filename is given*/
  if (pz_filename != NULL) {
    sprintf(PZfname, "%s/%s", get_pz_dir(), pz_filename);
    if (!(Fp = fopen(PZfname, "r"))) {
      messages("%s when open %s\n",strerror(errno),pz_filename);
      return FALSE;
    }
    lineno = 0;
  } else {
    errors("%s\n", "Internal error : PZname = NULL");
  }

  /* First of all read number of poles and zeros */
  fileOffset = ftell(Fp);
  while (TRUE) {
    if ((err = getLine(Fp, line, 250, '#', &lineno) == 1)) break;
    sprintf(line_cp, "%s", line);
    ntokens = sparse(line_cp, token, " :=\t\n", MAXTOK);
    ucase(token[0]);

    if (!strcmp(token[0], "NUMBER_OF_ZEROES")) {
      if (ntokens < 2) goto error;
      nz = atoi(token[1]);
      filter->nzero = nz;
    } else if (!strcmp(token[0], "NUMBER_OF_POLES")) {
      if (ntokens < 2) goto error;
      np = atoi(token[1]);
      filter->npole = np;
    }
  }
  fseek(Fp, fileOffset, SEEK_SET);

  /*
   * Allocate memory for poles and zeroes
   */
  if (filter->nzero > 0) {
    filter->zero = (double *) mem(filter->nzero * sizeof(double) * 2);
  }
  if (filter->npole > 0) {
    filter->pole = (double *) mem(filter->npole * sizeof(double) * 2);
  }

  /*
   * Read coefficients
   */
  lineno = 0;
  while (1) {
    if ((err = getLine(Fp, line, 250, '#', &lineno) == 1)) break;
    sprintf(line_cp, "%s", line);
    ntokens = sparse(line_cp, token, " :=\t\n", MAXTOK);
    ucase(token[0]);

    if (!strcmp(token[0], "NUMBER_OF_ZEROES")) {
      if (ntokens < 2) goto error;
      nz = atoi(token[1]);
      for (j = 0; j < nz; j++) {
        if ((err = getLine(Fp, line2, 250, '#', &lineno)) != 0) {
          messages("error reading zeroes in '%s'\n",PZfname);
          return FALSE;
        }
        ntok2 = sparse(line2, tok2, " ", 3);
        if (ntok2 != 2) {
          messages("expecting 2 values for zero '%s' line %d\n",
              PZfname, lineno);
          return FALSE;
        } else {
          char *endptr;

          filter->zero[2 * j] = strtod(tok2[0], &endptr);
          if (*endptr != '\0') {
               messages("Invalid number: %s\n", endptr);
               return FALSE;
          }

          filter->zero[2 * j + 1] = strtod(tok2[1], &endptr);
          if (*endptr != '\0') {
               messages("Invalid number: %s\n", endptr);
               return FALSE;
          }
        }
      }
    } else if (!strcmp(token[0], "NUMBER_OF_POLES")) {
      if (ntokens < 2) goto error;

      np = atoi(token[1]);
      for (j = 0; j < np; j++) {
        if ((err = getLine(Fp, line2, 250, '#', &lineno)) != 0) {
          messages("%s\n","error reading poles");
          return FALSE;
        }
        ntok2 = sparse(line2, tok2, " ", 3);
        if (ntok2 != 2) {
          messages("expecting 2 values for pole'%s' line %d\n",
              PZfname, lineno);
          return FALSE;
        } else {
          char *endptr;
          filter->pole[2 * j] = strtod(tok2[0], &endptr);
          if (*endptr != '\0') {
               messages("Invalid number: %s\n", endptr);
               return FALSE;
          }
          filter->pole[2 * j + 1] = strtod(tok2[1], &endptr);
          if (*endptr != '\0') {
               messages("Invalid number: %s\n", endptr);
               return FALSE;
          }
        }
      }
    }
  }

  fclose(Fp);
  Fp = NULL;

  return TRUE;

  error: errors("missing argument for %s\n", token[0]);
  return FALSE;
}
