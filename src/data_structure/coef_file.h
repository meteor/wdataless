/**\file coef_file.h
 *
 * \brief "poles and zeros" and coefficients files reading.
 */

#ifndef __FILTERFILE_H__
#define __FILTERFILE_H__

#include "filter.h"


/**\struct firCoef_s
 *
 * \brief List of coefficients and filter characteristics data representation.
 */
struct firCoef_s {
	int     type;   /**< coefficient type: FIR_SYM_1 FIR_SYM_2 COMB FIR_ASYM. */
	int     decim;  /**< decimation factor (1 => no decimation).              */
	int     ncoef;  /**< number of coefficients.                              */
	double *coef;   /**< array of coefficients.                               */
};

/**
 * firCoef_t type.
 */
typedef struct firCoef_s *firCoef_t;

/**\fn static int read_poles_and_zeros(char *pz_filename,filter_t filter)
 *
 * \brief Fill filter'a PZ data with the content of a given file.
 *
 * \param pz_filename The name of the pz file
 * \param filter      The filter to fill
 *
 * \return TRUE if filling is ok and FALSE otherwise
 */
int read_poles_and_zeros(char *, filter_t );

/**\fn int read_fir_coef(char *coefFname, firCoef_t *firCoef, int decim, int type)
 *
 * \brief Read coefficient associated with a filter from a file.
 *
 * \param coefFname The name of coefficients file
 * \param firCoef   A pointer to store result of read
 * \param decim     The decimation factor of the filter
 * \param type      The filter type
 *
 * \return TRUE
 */
int read_fir_coef(char *coefFname, firCoef_t *firCoef, int decim, int type);

/**\fn int readSensorPolyCoef(char *coefFname, filter_t *filter)
 *
 * \brief Read polynomial coefficient associated with a sensor from a file.
 *
 * \param coefFname The name of coefficients file
 * \param firter    A pointer to store result of read
 *
 * \return TRUE
 */
int read_sensor_poly_coef(char *coefFname, filter_t filter);


/**\fn int read_iir_coef(char *coefFname, filter_t filter)
 *
 * \brief Read numerator and denominator associated with a IIR filter from a file.
 *
 * \param coefFname The name of coefficients file
 * \param filter    A pointer to store result of read
 *
 * \return TRUE
 */
int read_iir_coef(char *coefFname, filter_t filter);
#endif /* __FILTERFILE_H__ */
