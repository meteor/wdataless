

#include <string.h>
#include "author.h"
#include "util.h"


int author_label_comp(author_t author, char *label_value) {
  return strcmp(author->label, label_value);
}
author_t create_author(char *label, list_t name, list_t agency, list_t email, list_t phone) {
    author_t result;

    result = (author_t) mem(sizeof(struct author_s));
    strncpy(result->label, label, 10);
    result->name = name;
    result->agency = agency;
    result->email = email;
    result->phone = phone;

    return result;
}
