/**\file digital_filter.h
 *
 */


#ifndef _DIGITAL_FILTER_H_
#define _DIGITAL_FILTER_H_

#include "definition.h"
#include "filter.h"




/**\struct digital_filter_s
 *
 */
struct digital_filter_s {
  long      object_type; /**< The object type                       */
  int       decim;       /**< filter decim factor                   */
  char     *filename;    /**< filter file name                      */
  filter_t  filter;      /**< filter                                */
  int       is_fake;     /**< Say if this digital filter is a fake  */
};

/**
 * digital_filter_t type.
 */
typedef struct digital_filter_s *digital_filter_t;

/**\struct filter_cascade_s
 *
 */
struct filter_cascade_s {
  long              object_type;    /**< The object type                      */
	char              label[10];      /**< cascade label                        */
	char             *filename;       /**< cascade filename                     */
	int               nstage;         /**< number of digital filters in array   */
	digital_filter_t *digital_filter; /**< array of digital filters             */
  int               is_fake;        /**< Say if fake digital filter is a fake */
};

/**
 * filter_cascade_t type
 */
typedef struct filter_cascade_s *filter_cascade_t;


/**\fn int comp_cascade(filter_cascade_t cascade1, filter_cascade_t cascade2);
 *
 * \brief say if two filter_cascade_t have the same label.
 *
 * cascade1 -- The first filter cascade
 * cascade2 -- The second filter cascade
 *
 * return     -- 0 if the filter cascades have the same label.
 */
int comp_cascade(filter_cascade_t cascade1, filter_cascade_t cascade2);


/**\fn filter_cascade_t create_digital_filter_cascade(char *label,char *stagefile);
 *
 * \brief Initialize a digital filter according to the content of a given stage file.
 *
 * \param label     The label of the new digital filter cascade
 * \param stagefile The name of stage file defining the new digital filter cascade
 *
 * \return The newly created digital filter cascade
 */
filter_cascade_t create_digital_filter_cascade(char *label,char *stagefile);

/**\fn digital_filter_t create_digital_filter()
 *
 * \brief Create a new digital filter.
 *
 * \return An empty digital filter.
 */
digital_filter_t create_digital_filter();

/**\fn void process_digital_filter(digital_filter_t digital_filter, definition_t definitions_list)
 *
 * \brief Process definition_s present in a digital_filter stage definition_s.
 *
 * \param digital_filter   The digital filter
 * \param definitions_list The list of definition_s present in the digital filter definition_s
 *
 */
void process_digital_filter(digital_filter_t digital_filter,
		definition_t definitions_list);

/**\fn void process_digital_filter_stage(digital_filter_t stage, definition_t definitions_list)
 *
 * \brief Process definition_s in digital filter stage (except the first one)
 *
 * \param stage            The digital filter stage
 * \param definitions_list The list of definition_s present in the stage
 */
void process_digital_filter_stage(digital_filter_t stage,
    definition_t definitions_list);

/**\fn is_cascade(filter_cascade_t cascade)
 *
 * \brief Say if a given pointer is a cascade filter.
 *
 * \param cascade The pointer to test
 *
 * \return TRUE if the given pointer is a filter_cascade_t and FALSE otherwise.
 */
int is_cascade(filter_cascade_t cascade);

/**\fn is_digital_filter(digital_filter_t filter)
 *
 * \brief Say if a given pointer is a digital_filter_t.
 *
 * \param filter The pointer to test
 *
 * \return TRUE if the given pointer is a digital_filter_t and FALSE otherwise.
 */
int is_digital_filter(digital_filter_t filter);

#endif /* _DIGITAL_FILTER_H_ */
