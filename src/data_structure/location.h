/*
 * location.h
 *
 *  Created on: 12 sept. 2011
 *      Author: leon
 */

#ifndef LOCATION_H_
#define LOCATION_H_

#include "uncertainty_double.h"

/**\struct location_s
 *
 * \brief Description of location associated with location code
 */
struct location_s {
  char                 *label; /**< Location label */
	char                 *code;  /**< location code */
	uncertainty_double_t  lat;   /**< latitude,  decimal degress, + => N */
	uncertainty_double_t  lon;   /**< longitude, decimal degress, + => E */
	uncertainty_double_t  elev;  /**< elevation, meters                  */
	double  							depth; /**< depth, meters                      */
	char   							 *desc;  /**< description string                 */
};

/**
 * location_t type definition.
 */
typedef struct location_s *location_t;


/**\fn int comp_location(location_t location1, location_t location2);
 *
 * \brief say if two location have the same label.
 *
 * location1 -- The first location
 * location2 -- The second location
 *
 * return    -- 0 if the locations have the same label.
 */
int comp_location(location_t location1, location_t location2);

/**\fn location_t create_location(char *label, char *code, uncertainty_double_t latitude, uncertainty_double_t longitude, uncertainty_double_t elevation, double depth, char *description)
 *
 * Initialize a location object.
 *
 * \param label         Location code
 * \param code          Location code
 * \param latitude      Latitude
 * \param longitude     Longitude
 * \param elevation     Elevation
 * \param depth         Depth
 * \param description   Description
 *
 * \return the new created location object.
 */
location_t create_location(char *, char *, uncertainty_double_t , uncertainty_double_t,
		uncertainty_double_t , double , char *);

#endif /* LOCATION_H_ */
