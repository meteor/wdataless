/**\file station.c
 *
 *
 */

#include <string.h>
#include "util.h"
#include "time_date.h"
#include "errors.h"
#include "station.h"

station_t stalist_head;

station_t create_station(char *station_code, char *begin_time, char *end_time) {

  station_t  result = (station_t) mem(sizeof(struct station_s));
  sprintf(result->name,"%s",station_code);
  result->beg_utime = asc2dbtime(begin_time);
  if (!strcmp(end_time, "present"))
    result->end_utime = 0.0;
  else
    result->end_utime = asc2dbtime(end_time);

  result->nlocation = 0;
  result->location = NULL;
  result->sensor = NULL;
  result->analog_filter = NULL;
  result->digitizer = NULL;
  result->cascade =  NULL;
  result->channel = NULL;

  return result;
}


sensor_t get_station_sensor(station_t station, char *sensor_label) {

  sensor_t result = NULL;
  int i;

  for (i = 0; i < station->nsensor; i++) {
    if (!strcasecmp(station->sensor[i]->label, sensor_label)) {
      result = station->sensor[i];
      break;
    }
  }
  return result;
}

analog_filter_t get_station_analog_filter(station_t station, char *filter_label) {

  analog_filter_t result = NULL;
  int i;

  for (i = 0; i < station->nanalog_filter; i++) {
    if (!strcasecmp(station->analog_filter[i]->label, filter_label)) {
      result = station->analog_filter[i];
      break;
    }
  }
  return result;
}

digitizer_t get_station_digitizer(station_t station, char *digitizer_label) {

  digitizer_t result = NULL;
  int i;

  for (i = 0; i < station->ndigitizer; i++) {
    if (!strcasecmp(station->digitizer[i]->label, digitizer_label)) {
      result = station->digitizer[i];
      break;
    }
  }
  return result;
}
filter_cascade_t get_station_filter_cascade(station_t station, char *cascade_label) {

  filter_cascade_t result = NULL;
  int i;

  for (i = 0; i < station->ncascade; i++) {
    if (!strcasecmp(station->cascade[i]->label, cascade_label)) {
      result = station->cascade[i];
      break;
    }
  }
  return result;
}
