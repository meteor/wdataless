/**\file uncertainty_double.h
 *
 * \brief Header of uncertainty double
 */

#ifndef UNCERTAINTY_DOUBLE_H_
#define UNCERTAINTY_DOUBLE_H_

/**
 * definition_t type.
 */
typedef struct uncertainty_double_s *uncertainty_double_t;

/**\struct uncertainty_double_s
 *
 * \brief A simple data structure to uncertainty double.
 *
 */
struct uncertainty_double_s {
	double  value;      /**< The double value   */
	double *min_error;  /**< The negative error */
	double *plus_error; /**< The positive error */
};

/**\fn uncertainty_double_t new_uncertaimty_double(double value,double min_error, double plus_error)
 *
 * \brief create a new uncertainty_double_s.
 *
 * \param value      The double value
 * \param min_error  The negative error
 * \param plus_error The positive error
 *
 */
uncertainty_double_t new_uncertaimty_double(double value,double *min_error, double *plus_error);

#endif /* UNCERTAINTY_DOUBLE_H_ */
