/**\file network.h
 *
 * \brief Network management header file.
 *
 */

#ifndef NETWORK_H_
#define NETWORK_H_

/**
 * Define network type.
 */
typedef struct network_s *network_t;

/**\struct network_s
 *
 * \brief Analogic filter data representation.
 */
struct network_s {
  char  label[10];     /**< The dbird label                */
  char   fdsn_code[3]; /**< The network FDSN code          */
  char  *description;  /**< Network description            */
  double open_utime;   /**< open time begin                */
  double close_utime;  /**< close time end                 */
  char *code;          /**< Alternative code               */
  network_t next;      /**< Next element in network list   */
};


/**\fn network_t create_network(char *label, char *fdsn_code, char *description, char *open_time, char *close_time, char *code)
 *
 * \brief Create a new network structure.
 *
 * \param label        The Dbird label of this network
 * \param fdsn_code    The FDSN code
 * \param description  The network description
 * \param open_time    The open date
 * \param close_time   The close date
 * \param code         An alternate code
 *
 *
 * \return             The new created network
 */
network_t create_network(char *label, char *fdsn_code, char *description,
       char *open_time, char *close_time, char *code);

/**\fn network_t find_network(char *label)
 *
 * \brief Find the network with the given label.
 *
 * \param label    The label
 *
 * \return         Network with given label or NULL if not found
 */
network_t find_network(char *label);

#endif /* NETWORK_H_ */
