

#include "phone_number.h"
#include "util.h"

phone_number_t create_phone_number(int *country_code, int area_code, char *phone_number) {
  phone_number_t result;

  result = (phone_number_t) mem(sizeof(struct phone_number_s));
  result->country_code = country_code;
  result->area_code = area_code;
  result->phone_number = phone_number;

  return result;
}
