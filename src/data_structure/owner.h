/*
 * owner.h
 *
 *  Created on: 13 sept. 2011
 *      Author: leon
 */

#ifndef OWNER_H_
#define OWNER_H_

// The list of owner name
struct struct_owner {
	char                *owner_name;
	struct struct_owner *next;
};
typedef struct struct_owner * owner_t;




int  get_owner_abbreviation_index(char *);
void add_owner_name(char *);
char *owner_iterator(int);
#endif /* OWNER_H_ */
