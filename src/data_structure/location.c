/**\file location.c
 *
 *
 */
#include <string.h>
#include "util.h"
#include "location.h"
#include "errors.h"

int comp_location(location_t location1, location_t location2) {
  return strcmp(location1->label, location2->label);
}

location_t create_location(char *label, char *code, uncertainty_double_t latitude, uncertainty_double_t  longitude,
		uncertainty_double_t  elevation, double depth, char *description) {

	location_t result;

	result = mem(sizeof(struct location_s));

	result->label = mem(strlen(label) + 1);
	sprintf(result->label, "%s", label);
	result->code =  mem(strlen(code) + 1 < 3 ? 3 : strlen(code) + 1);
	sprintf(result->code, "%2s", code);
	result->lat = latitude;
	result->lon = longitude;
	result->elev = elevation;
	result->depth = depth;
	result->desc = description;


        debug(DEBUG_DATA_STRUCTURE, "%s : <%s> %e %e %e %e\n", 
              result->label, result->code, result->lat->value,
              result->lon->value , result->elev->value, result->depth);
	return result;
}
