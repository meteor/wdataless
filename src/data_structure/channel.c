/**\file channel.c
 *
 * \brief Channel structures filling and manipulation.
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include "main.h"
#include "seed.h"
#include "util.h"
#include "station.h"
#include "comment.h"
#include "network.h"
#include "errors.h"
#include "time_date.h"
#include "channel.h"
#include "parse.h"

struct dates {
  char network[3];
};

#define append_linklist_element(new, head, tail) \
        new->next = NULL; \
        if (head != NULL) tail->next = new; \
        else head = new; \
        tail = new;
#define EPSILON 1e-6

/**\var PZdir
 *
 * \brief The path to access poles and zero data bank root.
 *
 */
static char *PZdir;


/**\var superTypeListHead
 *
 * \brief Head of super type sensor list.
 *
 */
static struct sensorSuperType *superTypeListHead = NULL;

/**\var superTypeListTail
 *
 * \brief Tail of super type sensor list.
 *
 */
static struct sensorSuperType *superTypeListTail = NULL;



/**\var sensorListHead
 *
 * \brief Head of sensor list.
 *
 * This list contain information on sensor read in sensor_list files
 */
static struct sensorType *sensorListHead = NULL;

/**\var sensorListTail
 *
 * \brief Tail of sensor list.
 *
 * This list contain information on sensor read in sensor_list files
 */
static struct sensorType *sensorListTail = NULL;

struct sensorType *get_sensor_list() {
  return sensorListHead;
}

struct sensorSuperType *get_super_type_list() {
  return superTypeListHead;
}


int get_sensor_list_size() {
  struct sensorType *sensor_list = get_sensor_list();
  int size = 0;

  while (sensor_list != NULL) {
    size++;
    sensor_list = sensor_list->next;
  }
  return size;
}

int get_super_type_list_size() {
  struct sensorSuperType *sensor_list = get_super_type_list();
  int size = 0;

  while (sensor_list != NULL) {
    size++;
    sensor_list = sensor_list->next;
  }
  return size;
}

/**\fn void seed_channel_map(station_t station, char *sensor_type, char gain, char *channel_name, double srate, double orient_ofs, char *SeedChanName)
 *
 * \brief Determine the name of channel according to device characteristics.
 *
 * @param station
 * @param sensor       The type of sensor
 * @param gain         A single char. For exemple 'H', for "high gain"
 * @param channel_name The channel name given by user
 * @param srate        The sampling rate of the channel
 * @param orient_ofs   If the offset is greater that 5 degrees the component code is a number.
 * @param SeedChanName The computed Seed channel name
 *
 * This module starts by reading the "sensor_list" file to get the sensor types and
 * the corresponding bandCode.
 *
 */
static void seed_channel_map(station_t station, char *sensor_type, char gain,
    char *channel_name, double srate, double orient_ofs, char *SeedChanName) {
  char type[TYPE_STR_SIZE];
  char bandCode[PERIOD_STR_SIZE];
  int found_sensor;
  struct sensorType *pt;
  int dbug = 0;

  if (dbug) {
    printf("==== seed_channel_map: input: sensor=%s g='%c' ", sensor_type,
        gain);
    printf("comp=%s srate=%E\n", channel_name, srate);
  }

  // Looking for sensor
  found_sensor = FALSE;
  for (pt = sensorListHead; pt != NULL; pt = pt->next) {
    if (!strcmp(sensor_type, pt->model)) {
      found_sensor = TRUE;
      sprintf(type, "%s", pt->type);
      sprintf(bandCode, "%s", pt->period);
      break;
    }
  }
  if (found_sensor == FALSE) {
    errors("sensor '%s' not found in list\n", sensor_type);
  }

  if (dbug) printf("====                   found %s; band_code=%s\n",
      sensor_type, bandCode);

  /* SEED channel naming. Band Code = 1st char
   *          srate       corner period   code
   *      --------------    ---------      --
   *      >= 1000 to < 5000  >= 10 sec       F
   *      >= 1000 to < 5000  <  10 sec       G
   *      >= 250  to < 1000  <  10 sec       D
   *      >= 250  to < 1000  >= 10 sec       C
   *      >= 80   to < 250   <  10 sec       E
   *      <  80   to >= 10   <  10 sec       S
   *      >= 80              >= 10 sec       H
   *      <  80   to >= 10   >= 10 sec       B
   *      >  1    to <  10                   M
   *      >  0.55  to <= 1                   L
   *      >  0.055 to <= 0.55                V
   *      <= 0.055                           U
   */

  SeedChanName[3] = '\0';

  /* First letter */

  if (srate >= 10.0 && strcmp(bandCode, "LP") && strcmp(bandCode, "CP")) {
    errors("%s_%s when srate >= 10 (srate=%e) band code must specifier (LP/CP), band code = %s",
        station->name, channel_name, srate, bandCode);
  }
  SeedChanName[0] = '\0';
  if (srate >= 1000.0 && !strcmp(bandCode, "LP"))
    SeedChanName[0] = 'F';
  else if (srate >= 1000.0 && !strcmp(bandCode, "CP"))
    SeedChanName[0] = 'G';
  else if (srate >= 250.0 && !strcmp(bandCode, "CP"))
    SeedChanName[0] = 'D';
  else if (srate >= 250.0 && !strcmp(bandCode, "LP"))
    SeedChanName[0] = 'C';
  else if (srate >= 80.0 && !strcmp(bandCode, "CP"))
    SeedChanName[0] = 'E';
  else if (srate >= 10.0 && !strcmp(bandCode, "CP"))
    SeedChanName[0] = 'S';
  else if (srate >= 80.0 && !strcmp(bandCode, "LP"))
    SeedChanName[0] = 'H';
  else if (srate >= 10.0 && !strcmp(bandCode, "LP"))
    SeedChanName[0] = 'B';
  else if (srate - 1  >  EPSILON)
    SeedChanName[0] = 'M';
  else if (srate > 0.55)
    SeedChanName[0] = 'L';
  else if (srate > 0.055)
    SeedChanName[0] = 'V';
  else if (srate >= 0.001)
    SeedChanName[0] = 'U';
  else if (srate >= 0.0001)
    SeedChanName[0] = 'R';
  else if (srate >= 0.00001)
    SeedChanName[0] = 'P';
  else if (srate >= 0.000001)
    SeedChanName[0] = 'T';
  else {
    SeedChanName[0] = 'Q';
  }

  /* Second letter */

  SeedChanName[1] = gain; // Get the declared letter in dbird
  if (!strncmp(type, "ACC", 3)) {
    SeedChanName[1] = 'N'; /* Accelerometer */
  } else if (!strncmp(type, "MP", 2)) {
    SeedChanName[1] = 'M'; /* Mass position */
  } else if (!strncmp(type, "VEL", 3)) {
    if (SeedChanName[1] != 'H' && SeedChanName[1] != 'L') {
      errors("The sensor family letter (%c) of channel %s for station %s is incorrect, must be L or H\n",
             SeedChanName[1], channel_name, station->name);
    }
  } else if (!strncmp(type, "PRES", 4)) {
    SeedChanName[1] = 'D'; /* A barometer, or microbarometer measures pressure. */
  } else if (!strncmp(type, "WIND", 4)) {
    SeedChanName[1] = 'W';
  } else if (!strncmp(type, "TEMP", 4)) {
    SeedChanName[1] = 'K';
  } else if (!strncmp(type, "TILT", 4)) {
    SeedChanName[1] = 'A';
  } else if (!strncmp(type, "VSTRAIN", 7)) {
    SeedChanName[1] = 'V';
  } else if (!strncmp(type, "TIDE", 4)) {
    SeedChanName[1] = 'T';
  } else if (!strncmp(type, "ELEC", 4)) {
    SeedChanName[1] = 'E';
  } else if (!strncmp(type, "GEOPHONE", 8)) {
    SeedChanName[1] = 'P';
  } else if (!strncmp(type, "ROTATION", 8)) {
    SeedChanName[1] = 'J';
  } else {
    // In fact we can't go here
    // (type is verified during sensor_list reading)
    errors("The sensor type (%s) is unknown", type);
  }

  /* Third letter */
  /*
   * If STS2 Mass Position: triaxial: use A/B/C
   */
  if (!strncmp(sensor_type, "STS2P", 5) || !strncmp(sensor_type, "STS2LP", 6)) {
    if (channel_name[0] == '0' || channel_name[0] == 'Z'
        || channel_name[2] == 'Z') {
      SeedChanName[2] = 'A';
    } else if (channel_name[0] == '1' || channel_name[0] == 'N'
        || channel_name[2] == 'N') {
      SeedChanName[2] = 'B';
    } else if (channel_name[0] == '2' || channel_name[0] == 'E'
        || channel_name[2] == 'E') {
      SeedChanName[2] = 'C';
    } else {
      errors("%s unsupported component %s\n", station->name, channel_name);
    }
  } else if (!strncmp(type, "PRES", 4)) {
    if (channel_name[2] == 'O') {

    } else if (channel_name[2] == 'I') {

    } else if (channel_name[2] == 'D') {

    } else if (channel_name[2] == 'F') {

    } else if (channel_name[2] == 'H') {

    } else if (channel_name[2] == 'U') {

    } else {
      warnings("The orientation in not valid must in { O, I, D, F, H, U } and is %c (%s)\n",
               channel_name[2], station->name);
    }
    SeedChanName[2] = channel_name[2];
  } else if (!strncmp(type, "TEMP", 4)) {
    if (channel_name[2] == 'O') {

    } else if (channel_name[2] == 'I') {

    } else if (channel_name[2] == 'D') {

    } else {
      warnings("The orientation in not valid must in { O, I, D } and is %c (%s)\n",
          channel_name[2], station->name);
    }

    SeedChanName[2] = channel_name[2];
  } else if (!strncmp(type, "WIND", 4)) {
    if (channel_name[2] == 'S') {
    } else if (channel_name[2] == 'D') {
    } else {
      warnings("The orientation in not valid must in { S, D } and is %c (%s)\n",
          channel_name[2], station->name);
    }
    SeedChanName[2] = channel_name[2];
  } else if (!strncmp(type, "TILT", 4)) {
    SeedChanName[2] = channel_name[2];
    if (channel_name[2] == 'N' || channel_name[2] == '1') {
    } else if (channel_name[2] == 'E' || channel_name[2] == '2') {
    } else {
      warnings("The orientation in not valid must in { N, E } and is %c (%s)\n",
          channel_name[2], station->name);
    }
    if (orient_ofs > 5.0) {
      if (SeedChanName[2] == 'Z')
        SeedChanName[2] = 'Z';
      else if (SeedChanName[2] == 'N')
        SeedChanName[2] = '1';
      else if (SeedChanName[2] == 'E')
        SeedChanName[2] = '2';
    }
  } else if (!strncmp(type, "VSTRAIN", 7)) {
    SeedChanName[2] = channel_name[2];
  } else if (!strncmp(type, "TIDE", 4)) {
    if (channel_name[2] != 'Z') {
      warnings("The orientation in not valid must in Z and is %c (%s)\n",
          channel_name[2], station->name);
    }
    SeedChanName[2] = channel_name[2];
  } else if (!strncmp(type, "ELEC", 4)) {
    SeedChanName[2] = channel_name[2];
  } else {

    /*
     * If horizontal orientation is off by more than 5 degrees,
     * N and E should be replaced by 2 and 3 respectively.
     */
    SeedChanName[2] = channel_name[2];
    if (channel_name[2] == 'Z')
      ;
    else if (channel_name[2] == '1' || channel_name[2] == 'N')
      ;
    else if (channel_name[2] == '2' || channel_name[2] == 'E')
      ;
    else if (channel_name[2] == '3')
      warnings("%s: orientation code 3 seem to be replaced by 2\n", station->name);
    else {
      errors("unsupported component %s\n", channel_name);
    }
    // Verify that if orientation code is 1 or 2 orient_ofs > 5
    if ((channel_name[2] == '1' || channel_name[2] == '2') &&
        orient_ofs < 5) {
          warnings("%s: Orientation code is %c and orientation error is %f\n",
                   station->name, channel_name[2], orient_ofs);
    }

    // Set correct orientation code if orientation is > 5 degres
    if (orient_ofs > 5.0) {
      if (SeedChanName[2] == 'Z')
        SeedChanName[2] = 'Z';
      else if (SeedChanName[2] == 'N')
        SeedChanName[2] = '1';
      else if (SeedChanName[2] == 'E')
        SeedChanName[2] = '2';
    }
  }
}

void read_sensor_list() {
  static int sensor_list_loaded = 0;
  int lineno = 0;
  FILE *Fp = NULL;
  struct sensorType *pt;
  char fname[511];
  char line[255];
  char line_cp[255];
  char *token[50];
  int ntokens;

  if (!sensor_list_loaded) {
    sensor_list_loaded = 1;
    if ((PZdir = getenv("PZ"))) {
      if (!isdir(PZdir)) {
        errors("directory '%s' not found\n", PZdir);
      }
    } else {
      errors("%s",
          "\tERROR: seed_channel_map: read_sensor_list:\n\tThe 'sensor_list' file is mandatory to make the Seed channel name.\n\tThis file is located in the directory pointed to by the environment\n\tvariable 'PZ' or 'RESPDB' (responses information is not used here).\n");
      exit(1);
    }
    sprintf(fname, "%s/%s", PZdir, SENSORLIST);
    if (!(Fp = fopen(fname, "r"))) {
      errors("can't open %s\n", fname);
    }

    while (!getLine(Fp, line, 255, '#', &lineno)) {
      int i;

      trim(line);
      sprintf(line_cp, "%s", line);
      ntokens = sparse(line, token, " \t", 50);
      if (ntokens < 5) {
        errors("file '%s': need at least 4 tokens in line '%s'\n", fname,
            line_cp);
      }
      pt = (struct sensorType *) calloc(sizeof(struct sensorType), 1L);
      append_linklist_element(pt, sensorListHead, sensorListTail);
      if (strlen(token[0]) + 1 > MODEL_STR_SIZE) {
        errors("In <%s> model name length is greater than maximum (%i)\n",
            line_cp, MODEL_STR_SIZE-1);
      }
      if (strlen(token[1]) + 1 > MODEL_STR_SIZE) {
        errors("In <%s> model name length is greater than maximum (%i)\n",
            line_cp, MODEL_STR_SIZE-1);
      }
      if (strlen(token[2]) + 1 > TYPE_STR_SIZE) {
        errors("In <%s> type length is greater than maximum (%i)\n", line_cp,
            TYPE_STR_SIZE-1);
      }
      if (strlen(token[3]) + 1 > PERIOD_STR_SIZE) {
        errors("In <%s> period length is greater than maximum (%i)\n", line_cp,
            PERIOD_STR_SIZE-1);
      }

      sprintf(pt->model, "%s", token[0]);
      sprintf(pt->meta_model, "%s", token[1]);
      sprintf(pt->type, "%s", token[2]);
      sprintf(pt->period, "%s", token[3]);
      int desc_size = 0;
      for (i = 4; i < ntokens; i++) {
        strncpy(pt->description + desc_size, token[i],
            strlen(token[i]) >= (DESC_STR_SIZE - desc_size - 1) ? (DESC_STR_SIZE - desc_size)
                - 1 :
                strlen(token[i]));
        if (desc_size + strlen(token[i]) >= DESC_STR_SIZE - 1)
          break;
        *(pt->description + desc_size + strlen(token[i])) = ' ';
        desc_size += strlen(token[i]) + 1;
      }

      debug(DEBUG_SENSOR_LIST_PARSER, "Read :<%s> <%s> <%s> <%s> <%s>\n", pt->model, pt->meta_model,
            pt->type, pt->period, pt->description);
      // Verify validity
      if (strcmp(pt->type, "VEL") && strcmp(pt->type, "MP")
          && strcmp(pt->type, "ACC") && strcmp(pt->type, "PRES")
          && strcmp(pt->type, "WIND") && strcmp(pt->type, "TEMP")
          && strcmp(pt->type, "TILT") && strcmp(pt->type, "VSTRAIN")
          && strcmp(pt->type, "TIDE") && strcmp(pt->type, "ELEC")
          && strcmp(pt->type, "GEOPHONE") && strcmp(pt->type, "ROTATION") ) {
        errors("file '%s' : unsupported sensor type code '%s' in line '%s' (Valid one are : VEL, MP, ACC, PRES, WIND, TEMP, TILT, VSTRAIN, TIDE, ELEC, GEOPHONE, ROTATION)\n",
            fname, pt->type, line_cp);
      }

      if (strcmp(pt->period, "LP") && strcmp(pt->period, "CP")
          && ((strcmp(pt->type, "PRES") || strcmp(pt->type, "WIND")
              || strcmp(pt->type, "TEMP")) && strcmp(pt->period, "XX"))) {
        errors("file '%s' : unsupported band code '%s' in line '%s'\n", fname,
            pt->period, line_cp);
      }
      struct sensorSuperType *current = superTypeListHead;
      while(current) {
	       if (strcmp(current->code, token[1]) == 0)
	         break;
	       current = current->next;
      }
      if (current == NULL) {
	      // Add missing meta model
        struct sensorSuperType *superType;
        superType = (struct sensorSuperType *) calloc(sizeof(struct sensorSuperType), 1L);
        sprintf(superType->code, "%s", pt->meta_model);
        sprintf(superType->description, "%s", pt->description);
        append_linklist_element(superType, superTypeListHead, superTypeListTail);
      }
    }

    fclose(Fp);
    Fp = NULL;
    return;
  }
}

char *get_sensor_metamodel(char *string) {
  if (string == NULL) return NULL;
  struct sensorType *sensor_list = get_sensor_list();
  while (sensor_list != NULL) {
    if (strcmp(string, sensor_list->model) == 0) {
      return sensor_list->meta_model;
    }
    sensor_list = sensor_list->next;
  }
  errors("get_sensor_list: unsupported code: %s\n", string);
  return NULL;
}

char *get_sensor_datatype(char *string) {
  if (string == NULL) return NULL;
  debug(DEBUG_SEED_WRITE, "sensor name : %s\n", string);
  struct sensorType *sensor_list = get_sensor_list();
  while (sensor_list != NULL) {
    if (strcmp(string, sensor_list->model) == 0) {
      return sensor_list->type;
    }
    sensor_list = sensor_list->next;
  }
  errors("get_sensor_list: unsupported code: %s\n", string);
  return NULL;
}

int dbencode(char *string) {
  if (string == NULL) return -1;
  debug(DEBUG_SEED_WRITE, "sensor name : %s\n", string);
  int index = 1;
  struct sensorSuperType *sensor_list = get_super_type_list();
  while (sensor_list != NULL) {
    if (strcmp(string, sensor_list->code) == 0) {
      return index;
    }
    index++;
    sensor_list = sensor_list->next;
  }
  errors("dbencode: unsupported code: %s\n", string);
  return (UNKNOWN);
}

channel_t create_channel(char *location, char *seed_name, char *sensor_label,
    list_t analog_filter_labels, char *adc_label, list_t digital_filter_labels,
    char *channel_flags, char *encoding, char *begin_time, char *end_time, char *network_label,
    list_t comment_label) {
  channel_t result;

  int i;

  /* Alloc memory */
  result = (channel_t) mem(sizeof(struct channel_s));

  /* Location Label */
  result->location = (char *) mem(strlen(location) + 1);
  strcpy(result->location, location);

  /* Seed chan name */
  strcpy(result->name, seed_name);

  /* Sensor label */
  sprintf(result->sensor_label, "%s", sensor_label);

  /* Analog filter labels */
  result->nanalog_filter = get_list_size(analog_filter_labels);
  result->analog_filter_labels = NULL;
  list_t current_analog_label = analog_filter_labels;
  for (i = 0; i < result->nanalog_filter; i++) {
    char *label_copy = (char *) mem(strlen((char *)current_analog_label->element) + 1);
    sprintf(label_copy, "%s", (char *) current_analog_label->element);
    result->analog_filter_labels = append_element(label_copy, result->analog_filter_labels);
    current_analog_label = current_analog_label->next;
  }

  /* Digitizer label */
  sprintf(result->adc_label, "%s", adc_label);

  /* Digital filter labels */
  result->ndigital_filter = get_list_size(digital_filter_labels);
  debug(DEBUG_DATA_STRUCTURE, "Nb digital filter %d\n", result->ndigital_filter);
  result->digital_filter_labels = NULL;
  list_t current_label = digital_filter_labels;
  for (i = 0; i < result->ndigital_filter; i++) {
    debug(DEBUG_DATA_STRUCTURE, "Add label %s\n", current_label->element);
    char *label_copy = (char *) mem(strlen((char *)current_label->element) + 1);
    sprintf(label_copy, "%s", (char *) current_label->element);
    result->digital_filter_labels = append_element(label_copy, result->digital_filter_labels);
    current_label = current_label->next;
  }

  debug(DEBUG_DATA_STRUCTURE, "channel %s %s %s %s created\n", result->location,
      result->name, result->sensor_label, result->adc_label);
  /* Beg and end time */

  /* Save channel flags */
  result->channel_flags = (char *) mem(strlen(channel_flags) +1);
  strcpy(result->channel_flags, channel_flags);

  result->beg_utime = asc2dbtime(begin_time);
  if (!strcmp(end_time, "present"))
    result->end_utime = 0.0;
  else
    result->end_utime = asc2dbtime(end_time);

  network_t network = find_network(network_label);
  if (network == NULL) {
    errors("Can't find network with label %s", network_label);
  }
  sprintf(result->network, "%.2s", network->fdsn_code);

  result->comment_label = comment_label;
  result->comment = NULL;

  /* Set encoding */
  if (!strcmp(encoding, "STEIM1")) {
    result->encoding = STEIM1;
  } else if (!strcmp(encoding, "STEIM2")) {
    result->encoding = STEIM2;
  } else if (!strcmp(encoding, "FLOAT32")) {
    result->encoding = IEEEFLOAT;
  } else if (!strcmp(encoding, "INT32")) {
    result->encoding = INT32;
  } else {
    errors("The encoding format %s is not supported", encoding);
  }
  result->gain_err = 0.0;
  result->reported_sint = 0.0;
  result->ndigital_filter = 0;
  result->digital_filters = NULL;

  return result;
}

void mysort(char **string_arrray, int nbstr) {
  char *tmp;
  int   i,j;

  for(i=0; i < nbstr-1; i++) {
    for (j=i+1; j < nbstr; j++) {
      if (strcmp(*(string_arrray + i), *(string_arrray + j)) > 0) {
        tmp = *(string_arrray + i);
        *(string_arrray + i) = *(string_arrray + j);
        *(string_arrray + j) = tmp;
      }
    }
  }
}

//
// Split a given station in a list of station according to channel network
//
static station_t split_station_by_network(station_t station) {

  station_t result = NULL;
  char **DateStr;
  struct dates *Dates;
  station_t newSta;
  int i, j, k;
  int nDates;

  if (station->nchannel == 0) return station;

  Dates = (struct dates *) mem(sizeof(struct dates) * station->nchannel);

  DateStr = (char **) mem(sizeof(char *) * station->nchannel);
  for(i =0; i <station->nchannel; i++) {
    *(DateStr + i) = (char *) mem(sizeof(char) * 100);
  }

  /*
   * Make network strings
   */
  for (i = 0; i < station->nchannel; i++) {
    sprintf(*(DateStr + i), "%.2s", station->channel[i]->network);
    debug(DEBUG_DATA_STRUCTURE, "%s : %s\n", station->name, DateStr[i]);
  }

  /*
   * Sort channels network strings
   */
  mysort(DateStr, station->nchannel);
  debug(DEBUG_DATA_STRUCTURE, "%s\n", "After sorting channels");
  /*
   * Keep unique and convert back network strings
   */
  nDates = 0;
  for (i = 0; i < station->nchannel; i++) {
    debug(DEBUG_DATA_STRUCTURE, "%s : %s\n", station->name, DateStr[i]);
    if (i > 0 && (!strcmp(DateStr[i], DateStr[i - 1]))) continue;
    sscanf(DateStr[i], "%2s", Dates[nDates].network);
    ++nDates;
  }

  debug(DEBUG_SEED_WRITE, "Need to create %d stations for station %s\n", nDates,
      station->name);
  /*
   * Create station-channels
   */
  for (i = 0; i < nDates; i++) {
    debug(DEBUG_SEED_WRITE, "Create station for network %s\n",
        Dates[i].network);
    newSta = (station_t) mem(sizeof(struct station_s));

    strncpy(newSta->name, station->name, strlen(station->name));
    strncpy(newSta->network, Dates[i].network, 2);
    newSta->beg_utime = station->beg_utime;
    newSta->end_utime = station->end_utime;
    /*
     * Copy station owner
     */
    newSta->owner = (char *) mem(strlen(station->owner) + 1);
    strncpy(newSta->owner, station->owner, strlen(station->owner));

    newSta->comment = station->comment;
    /*
     * Copy station locations
     */
    newSta->nlocation = station->nlocation;
    newSta->location = (location_t *) mem(
        sizeof(location_t) * newSta->nlocation);
    for (j = 0; j < newSta->nlocation; j++) {
      newSta->location[j] = (location_t) mem(sizeof(struct location_s));
      memcpy(newSta->location[j], station->location[j],
          sizeof(struct location_s));
    }
    /*
     * Copy station sensors
     */
    newSta->nsensor = station->nsensor;
    newSta->sensor = (sensor_t *) mem(sizeof(sensor_t) * newSta->nsensor);
    for (j = 0; j < newSta->nsensor; j++) {
      newSta->sensor[j] = (sensor_t) mem(sizeof(struct sensor_s));
      memcpy(newSta->sensor[j], station->sensor[j], sizeof(struct sensor_s));
    }

    /*
     * Copy ampli_filters
     */
    newSta->nanalog_filter = station->nanalog_filter;
    newSta->analog_filter = (analog_filter_t *) mem(
        sizeof(analog_filter_t) * newSta->nanalog_filter);
    for (j = 0; j < newSta->nanalog_filter; j++) {
      newSta->analog_filter[j] = (analog_filter_t) mem(sizeof(struct analog_filter_s));
      memcpy(newSta->analog_filter[j], station->analog_filter[j],
          sizeof(struct analog_filter_s));
    }
    /*
     * Copy digitizers
     */
    newSta->ndigitizer = station->ndigitizer;
    newSta->digitizer = (digitizer_t *) mem(sizeof(digitizer_t) * newSta->ndigitizer);
    for (j = 0; j < newSta->ndigitizer; j++) {
      newSta->digitizer[j] = (digitizer_t) mem(sizeof(struct digitizer_s));
      memcpy(newSta->digitizer[j], station->digitizer[j], sizeof(struct digitizer_s));
    }
    /*
     * Copy selected station channels
     */
    newSta->nchannel = 0;
    for (j = 0; j < station->nchannel; j++) {
      if (!strncmp(station->channel[j]->network, newSta->network, 2)) {
        ++(newSta->nchannel);
      }
    }
    newSta->channel = (channel_t *) mem(sizeof(channel_t) * newSta->nchannel);
    k = 0;
    for (j = 0; j < station->nchannel; j++) {
      if (!strncmp(station->channel[j]->network, newSta->network, 2)) {
        newSta->channel[k] = (channel_t) mem(sizeof(struct channel_s));
        memcpy(newSta->channel[k], station->channel[j],
            sizeof(struct channel_s));
        ++k;
      }
    }

    if (result == NULL) {
      result = newSta;
    } else {
      station_t current = result;
      while (current->next != NULL)
        current = current->next;
      current->next = newSta;
    }
  }
  return result;
}

void check_seed_chan_name(int keep_channel_name) {
  station_t station;
  channel_t channel;
  char seed_chan[4];
  int i;

  for (station = stalist_head; station != NULL; station = station->next) {
    for (i = 0; i < station->nchannel; i++) {
      channel = station->channel[i];
      double orient_ofs = 0;
      if (channel->name[2] == 'E' || channel->name[2] == '1') {
        orient_ofs = abs(channel->sensor->azimuth - 90);
      } else if (channel->name[2] == 'N' || channel->name[2] == '2') {
        if (channel->sensor->azimuth > 180) {
          orient_ofs = abs(channel->sensor->azimuth - 360);
        } else {
          orient_ofs = channel->sensor->azimuth;
        }
      }
      seed_channel_map(station, channel->sensor->type, channel->name[1],
          channel->name, (double) (1.0 / channel->sint), orient_ofs, seed_chan);
      if (strcmp(channel->name, seed_chan)) {
        if (!keep_channel_name) {
          warnings("%s %s srate=%E : chan %s disagree with expected: %s\n",
              station->name, channel->sensor->type,
              (double ) (1.0 / channel->sint), channel->name, seed_chan);
          strncpy(channel->name, seed_chan, 3);
        }
      }
    }
  }
}

/**\fn static void set_normalization_frequency(station_t station)
 *
 * Update normalization frequency for all channels of the given station.
 *
 * @param station The station to process
 *
 */
static void set_normalization_frequency(station_t station) {
  int i, j, k;

  for (i = 0; i < station->nchannel; i++) {
    channel_t channel = station->channel[i];

    /* Get sensor value */
    double normalization_freq = channel->sensor->filter->fn;

    /* Get analog filter value if any */
    for (j = 0; j < channel->nanalog_filter; j++) {
      debug(DEBUG_DATA_STRUCTURE, "current_analog_filter %s",
            channel->analog_filters[j].label);
      for (k = 0; k < channel->analog_filters[j].nstage; k++) {
        if (channel->analog_filters[j].stages[k]->filter != NULL) {
          if (channel->analog_filters[j].stages[k]->filter->fn == 0) {
            channel->analog_filters[j].stages[k]->filter->fn = normalization_freq;
          }
        }
      }
    }
    /* Set normalization frequency of digitizer */
/**
    if (channel->adc->filt_cascade != NULL) {
      for (j = 0; j < channel->adc->filt_cascade->nstage; j++) {
        filter_t filter = channel->adc->filt_cascade->digital_filter[j]->filter;
        if (filter != NULL) {
          filter->fn = 0;
        }
      }
    }
*/

    /* Set digital filter normalization frequency */

    if (channel->ndigital_filter) {
      for (j = 0; j < channel->ndigital_filter; j++) {
        if (channel->digital_filters[j].filter->type != IIR_PZ) {
          channel->digital_filters[j].filter->fn = 0;
        }
      }
    }
  }
}

station_t fill_and_verify_channel(char *dbirdfile, station_t station) {

  station_t result_head = NULL;
  channel_t channel;
  int i, j, k, n;

  for (j = 0; j < station->ncascade; j++)
    debug(DEBUG_DATA_STRUCTURE, "'%s'\n", station->cascade[j]->label);

  /*
   *  Assign sensors to channels
   *  ==========================
   */
  for (i = 0; i < station->nchannel; i++) {
    channel = station->channel[i];

    channel->sensor = get_station_sensor(station, channel->sensor_label);
    /* check if exist */
    if (channel->sensor == NULL) {
      errors("unknown SENSOR %s in channel %d\n", channel->sensor_label, i);
    }
  }

  /*
   *  Assign analog filter to channels
   *  =========================
   */
  for (i = 0; i < station->nchannel; i++) {
    channel = station->channel[i];
    channel->analog_filters = NULL;

    channel->nanalog_filter = get_list_size(channel->analog_filter_labels);
    channel->analog_filters = (analog_filter_t) mem(
        sizeof(struct analog_filter_s) * channel->nanalog_filter);
    analog_filter_t current_analog_filter = channel->analog_filters;

    list_t current_analog_filter_label = channel->analog_filter_labels;
    while (current_analog_filter_label) {
      *current_analog_filter = *(get_station_analog_filter(station, (char *) current_analog_filter_label->element));
      if (!current_analog_filter) {
        errors("chan %d: found undefined analog filter %s\n", i,
            current_analog_filter_label->element);
      }
      current_analog_filter++;
      current_analog_filter_label = current_analog_filter_label->next;
    }
  }

  /*
   * Assign digitizers to channels
   * =============================
   */
  for (i = 0; i < station->nchannel; i++) {
    channel = station->channel[i];

    debug(DEBUG_DATA_STRUCTURE, "Channel %s : nadc= %d\n", channel->name,
        station->ndigitizer);
    channel->adc = get_station_digitizer(station, channel->adc_label);

    if (!channel->adc) {
      errors("chan %d: found undefined digitizer %s\n", i, channel->adc_label);
    }

  }

  /*
   * For each channel, compute number of digital filter:
   */
  for (i = 0; i < station->nchannel; i++) {
    n = 0;

    channel = station->channel[i];
    /* Digitizer digital filter stages cascade */
//    if (channel->digitizer->filt_cascade != NULL) {
//      n += channel->digitizer->filt_cascade->nstage;
//    }

    /* Digital filters stages cascade */
    if (channel->digital_filter_labels == NULL) {
      debug(DEBUG_DATA_STRUCTURE, "%s\n", "channel digital filter label list is empty");
    }
    list_t current_digital_filter_label = channel->digital_filter_labels;
    while (current_digital_filter_label) {

      debug(DEBUG_DATA_STRUCTURE, "digital filter label : %s\n",
            current_digital_filter_label->element);
      filter_cascade_t current_cascade = get_station_filter_cascade(station, (char *) current_digital_filter_label->element);
      if (!current_cascade) {
         errors("chan %d: found undefined digital filter %s for station %s\n", i,
            current_digital_filter_label->element, station->name);
      }
      if (!current_cascade->is_fake) {
         n += current_cascade->nstage;
      }
      current_digital_filter_label = current_digital_filter_label->next;
    }
    /* Alloc memory for this channel */
    debug(DEBUG_DATA_STRUCTURE, "%s.%s.%s.%s have %d digital filters\n",
          channel->network, station->name,
          channel->location, channel->name, n);
    channel->ndigital_filter = n;
    channel->digital_filters = (digital_filter_t) mem(
        n * sizeof(struct digital_filter_s));
  }

  for (i = 0; i < station->nchannel; i++) {
    n = 0;
    channel = station->channel[i];

    /*
     * Assign digitizer digital filters to channels
     * =================================
     */

    channel->sint = channel->adc->sint;
//    if (channel->digitizer->filt_cascade != NULL) {
//      for (j = 0; j < channel->digitizer->filt_cascade->nstage; j++) {
//        channel->digital_filters[n] =
//            *channel->digitizer->filt_cascade->digital_filter[j];
//        channel->sint *= channel->digital_filters[n].decim;
//        ++n;
//      }
//    }
    debug(DEBUG_SEED_WRITE, "%s.%s.%s.%s ADC sampling interval = %f\n",
          channel->network,
          station->name,
          channel->location,
          channel->name,
          channel->sint);
    /*
     * Assign cascade digital filters to channels
     * =================================
     */
    debug(DEBUG_SEED_WRITE, "station %s have %d digital filter\n",
                    station->name,
                    station->ncascade);
    list_t current_digital_filter_label = channel->digital_filter_labels;
    while (current_digital_filter_label) {
      filter_cascade_t current_cascade = get_station_filter_cascade(station, (char *)current_digital_filter_label->element);
      if (!current_cascade->is_fake) {
        debug(DEBUG_DATA_STRUCTURE, "Add digital filter %s (%d stage) to channel %s\n",
              current_cascade->label,
              current_cascade->nstage,
              channel->name);
        // Copy stage into channel
        for (k = 0; k < current_cascade->nstage; k++) {
          channel->digital_filters[n] = *(current_cascade->digital_filter[k]);
          if (fabs(current_cascade->digital_filter[k]->filter->sint - channel->sint) > EPSILON) {
            errors("In %s.%s.%s.%s:%s[stage=%d] Expecting input sampling interval %e and get %e\n(real-expected =%e) (real/expected =%e) (expected/real =%e\n",
                channel->network,
                station->name,
                channel->location,
                channel->name,
                current_cascade->label,
                k,
                current_cascade->digital_filter[k]->filter->sint,
                channel->sint,
                current_cascade->digital_filter[k]->filter->sint - channel->sint,
                current_cascade->digital_filter[k]->filter->sint / channel->sint,
                channel->sint / current_cascade->digital_filter[k]->filter->sint);
          }
          channel->sint *= channel->digital_filters[n].decim;
          ++n;
        }

        debug(DEBUG_SEED_WRITE, "Find digital filter %s for channel %s\n",
              current_cascade->label,
              channel->name);
        debug(DEBUG_SEED_WRITE, "%s.%s.%s.%s After %s sampling interval = %f\n",
              channel->network,
              station->name,
              channel->location,
              channel->name,
              current_digital_filter_label->element,
              channel->sint);
      }
      current_digital_filter_label = current_digital_filter_label->next;
    }
    debug(DEBUG_SEED_WRITE, "%s.%s.%s.%s sampling interval = %f\n",
          channel->network,
          station->name,
          channel->location,
          channel->name,
          channel->sint);
    /*
     * Set channel reported sint equal to sint
     */
    channel->reported_sint = channel->sint;
  }

  /*
   * Fix normalization frequency for all components channels of the current station.
   */
  set_normalization_frequency(station);

  /*
   *  Fill in channel channel gain for all channels in sta
   */
  calc_channel_gain(station);

  /*
   *  Fill in channel channel sint for all channels in sta
   */
  calc_channel_sint(station);

  /*
   * Check sensor azimuth, dip
   */
  check_sensor_orient(station);

  result_head = split_station_by_network(station);

  return result_head;
}

void calc_channel_gain(station_t station) {
  channel_t channel = NULL;
  double sens;
  int i, j, k;

  /*========  Check all gains which must have a unity value =======*/

  for (i = 0; i < station->nchannel; i++) {
    channel = station->channel[i];

    /* Check sensor */
    if (channel->sensor->filter) {
      if (channel->sensor->filter->gain != 1.0) {
        errors("sensor->filter->gain not 1 (%e)\n",
            channel->sensor->filter->gain);
      }
    }

    /* Check digital filter gain, if any */

    if (channel->ndigital_filter) {
      for (j = 0; j < channel->ndigital_filter; j++) {
        if (channel->digital_filters[j].filter->gain != 1.0) {
          errors("dig_filt->filter->gain not 1 (%e)\n",
              channel->digital_filters[j].filter->gain);
        }
      }
    }
  }

  /*======== Now calculate channel gain ========*/

  for (i = 0; i < station->nchannel; i++) {
    channel = station->channel[i];

    /*
     * BASIC RULE:  Sd*A0 = S*A where
     *          - S   standard sensitivity           (factory sensitivity)
     *          - A   standard normalization factor  (asymptotis equal to 1)
     *          - Sd  sensitivity at f=fnorm
     *          - A0  normalization factor at f=fnorm
     *
     * Note that all digital filters are assume to be "flat", with unity gain.
     */
    /* Overall sensitivity at f=form */
    /* ------------------------------*/
    sens = channel->sensor->filter->Sd;
    //if (channel->sensor->filter) {
    //  sens *= channel->sensor->filter->Sd;
    //}
    for (j = 0; j < channel->nanalog_filter; j++) {
      for (k = 0; k < channel->analog_filters[j].nstage; k++) {
        sens *= channel->analog_filters[j].stages[k]->ampli_G;
      }
    }
    sens *= channel->adc->fact;
    channel->Sens = sens;
  }
}
