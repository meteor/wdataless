/**\file network.c
 *
 * \brief Network structures filling and manipulation.
 */
#include <stdlib.h>
#include <string.h>
#include "util.h"
#include "network.h"
#include "time_date.h"

static network_t network_list = NULL;

int comp_network(network_t network1, network_t network2) {
  return strcmp(network1->label, network2->label);
}


network_t create_network(char *label, char *fdsn_code, char *description,
       char *open_time, char *close_time, char *code) {

  network_t result = mem(sizeof(struct network_s));

  sprintf(result->label, "%s", label);
  sprintf(result->fdsn_code, "%s", fdsn_code);
  result->description = mem(strlen(description) + 1);
  sprintf(result->description, "%s", description);

  result->open_utime = asc2dbtime(open_time);
  if (!strcmp(close_time, "present"))
    result->close_utime= 0.0;
  else
    result->close_utime= asc2dbtime(close_time);

  result->code = mem(strlen(code) + 1);
  sprintf(result->code, "%s", code);

  // Insert this new network at head of network list
  result->next = network_list;
  network_list = result;

  return result;
}


network_t find_network(char *label) {
  network_t current_network = network_list;
  while(current_network) {
    if(! strcmp(current_network->label, label))
      return current_network;
    current_network = current_network->next;
  }
  return NULL;
}
