/**\file sensor.h
 *
 *
 *
 */

#ifndef SENSOR_H_
#define SENSOR_H_

#include <string.h>
#include "filter.h"
#include "definition.h"
#include "units.h"


/**\struct sensor_s
 *
 */
struct sensor_s {
	char label[10];          /**< sensor label                      */
	char *type;              /**< sensor type (STS1, CMG3, etc...)  */
	char *serial_number;     /**< seismometer serial number         */
	char *sname;             /**< analog sensor filter file name    */
	filter_t filter;         /**< analog filter                     */
	unit_t inpUnits;         /**< units: ACC | DIS | VEL            */
	unit_t outUnits;         /**< units: volts                      */
	unit_t calibrationUnits; /**< Calibration units: Volts, Ampere  */
	float azimuth;
	float dip;
};

typedef struct sensor_s *sensor_t;


/**\fn int comp_sensor(sensor_t sensor1, sensor_t sensor2);
 *
 * \brief say if two sensors have the same label.
 *
 * sensor1 -- The first sensor
 * sensor2 -- The second sensor
 *
 * return  -- 0 if the sensors have the same label.
 */
int comp_sensor(sensor_t sensor1, sensor_t sensor2);

/**\fn void process_sensor(sensor_t sensor,definition_t definition_list)
 *
 * \brief Update a given sensor according to value present in the definitions list.
 *
 * \param sensor          The sensor to update
 * \param definition_list The definition list
 *
 */
void process_sensor(sensor_t sensor,definition_t definition_list);

/**\fn sensor_t create_sensor(char *label,char *type,char *serial_number,char *pz_file,double azimuth, double dip)
 *
 *
 *  \param label          The label of the sensor
 *  \param type           The type of the sensor
 *  \param serial_number  The serial number of the sensor
 *  \param pz_file        The pole and zero file
 *  \param azimuth        Azimuth of the sensor
 *  \param dip            Dip of the sensor
 *
 *  \return The new created sensor.
 *
 */
sensor_t create_sensor(char *label, char *type, char *serial_number,
		char *pz_file, double azimuth, double dip);
#endif /* SENSOR_H_ */
