/**\file units.h
 *
 */

#ifndef UNITS_H_
#define UNITS_H_

#include "list.h"

#define UNIT_NAME_SIZE 20
#define UNIT_DESC_SIZE 50
#define LINE_MAX_SIZE 1024

struct unit_s {
	int  lookup_code; /* The Blk34 lookup code*/
  char name[UNIT_NAME_SIZE]; /* Unit name  (V, M/S, ...) */
	char seed_code[UNIT_NAME_SIZE]; /* The standart Seed code */
  char description[UNIT_DESC_SIZE];
  struct unit_s *next;
};

typedef struct unit_s * unit_t;

/**\fn read_unit_list()
 *
 * \brief Read unit list file and load unit definition
 */
void read_unit_list();

/**\fn get_unit_list_head()
 *
 * \brief Give the head of unit list
 *
 * \return The unit list head
 */
list_t get_unit_list_head();

/**\fn unit_t get_unit_from_string(char *name)
 *
 * \brief Give the unit definition from is name
 *
 * \param   name The name of unit
 *
 * \return  The unit definition as unit_t
 */
unit_t get_unit_from_string(char *);

#endif /* UNITS_H_ */
