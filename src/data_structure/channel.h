/**\file channel.h
 *
 */

#ifndef CHANNEL_H_
#define CHANNEL_H_

#include "list.h"
#include "sensor.h"
#include "analog_filter.h"
#include "digitizer.h"
#include "digital_filter.h"

typedef enum data_format {
  STEIM1 = 1, STEIM2, IEEEFLOAT, INT32
} data_format_t;

/**
 * Define channel type.
 */
typedef struct channel_s *channel_t;

/**\struct channel_s
 *
 *  \brief Channel data representation.
 */
struct channel_s {
  char              name[4];              /**< Channel name                              */
  char              network[3];           /**< Network name                              */
  char              *location;            /**< location label                            */
  double            beg_utime;            /**< validity time begin                       */
  double            end_utime;            /**< validity time end                         */
  int               bps;                  /**< bytes per sample                          */

  char              sensor_label[10];     /**< sensor label                              */
  sensor_t          sensor;               /**< sensor                                    */

  list_t            analog_filter_labels; /**< amplifier-filter label                    */
  int               nanalog_filter;
  analog_filter_t   analog_filters;       /**< amplifier-filter                          */

  char              adc_label[10];        /**< digitizer label                           */
  digitizer_t       adc;                  /**< digitizer                                 */

  list_t            digital_filter_labels;/**< dig filter cascade label                  */
  int               ndigital_filter;      /* number of digital filter                    */
  digital_filter_t  digital_filters;      /* digital filters array                       */

  double            reported_sint;        /**< reported sample interval, seconds         */
  double            sint;                 /**< sample interval, seconds                  */
  double            gain_err;             /**< overall magnification  error              */
  double            Sens;                 /**< Channel sensitivity @ fn                  */
  double            fn;                   /**< Normalization frequency                   */
  data_format_t     encoding;             /**< Channel encoding format                   */
  char              *channel_flags;       /**< Channel flags                             */
  list_t            comment_label;        /**< The channel comment label                 */
  list_t            comment;              /**< The channel comment                       */
};


#define MODEL_STR_SIZE 50
#define TYPE_STR_SIZE 12
#define PERIOD_STR_SIZE 4
#define DESC_STR_SIZE 50

/**\struct sensorType
 *
 *
 */
struct sensorType {
  char model[MODEL_STR_SIZE]; /* sensor model (STS1, CMG3, etc...)  */
  char meta_model[MODEL_STR_SIZE]; /* sensor model (STS1, CMG3, etc...)  */
  char type[TYPE_STR_SIZE]; /* VEL, ACC, MP, ... */
  char period[PERIOD_STR_SIZE]; /* CP or LP */
  char description[DESC_STR_SIZE];
  struct sensorType *next;
};

struct sensorSuperType {
  char code[MODEL_STR_SIZE];
  char description[DESC_STR_SIZE];
  struct sensorSuperType *next;
};

/**\fn channel_t create_channel(char *location, char *seed_name, char *sensor_label, list_t analog_filter_labels, char *adc_label, list_t digital_filter_labels, char *triggered, char * encoding, char *begin_time, char *end_time, char *network)
 *
 * \brief Create a new channel structure.
 *
 * \param location
 * \param seed_name
 * \param sensor_label          The sensor label
 * \param analog_filter_labels  The analog_filter labels list
 * \param adc_label             The digitizer label
 * \param digital_filter_labels The digital filter labels list
 * \param channel_flag          Channel flags
 * \param begin_time            The begin time validity
 * \param end_time              The end time validity
 * \param network
 * \param comment_label         The comment label list
 *
 *
 * \return                      The new created channel
 */
channel_t create_channel(char *location, char *seed_name, char *sensor_label,
			 list_t analog_filter_labels, char *adc_label, list_t digital_filter_labels,
			 char *channel_flags, char * encoding, char *begin_time, char *end_time, char *network,
       list_t comment_label);

#include "station.h"

/**\fn void fill_and_verify_channel(char *dbirdfile,station_t station)
 *
 * \brief Fill channel structure and verify channel validity of a given station.
 *
 * This function split channel with different network in different station object.
 *
 * \param dbirdfile The name of the parsed dbirdfile
 * \param station  The station.
 *
 * \return The list of station object corresponding to the given station.
 */
station_t fill_and_verify_channel(char *filename,station_t station);

/**\fn void calc_channel_gain(station_t station)
 *
 *
 * BASIC RULE:  Sd*A0 = S*A where :
 *          - S   standard sensitivity           (factory sensitivity)
 *          - A   standard normalization factor  (asymptotis equal to 1)
 *          - Sd  sensitivity at f=fnorm
 *          - A0  normalization factor at f=fnorm
 *
 * Note that all digital filters are assume to be "flat", with unity gain.
 *
 * \param station The station
 *
 */
void calc_channel_gain(station_t station);


/**\fn void check_seed_chan_name()
 *
 * \param keep_channel_name Say if channel must be keep even if error is detected.
 *
 * \brief Verify that channel name is correct according to sensor type and sampling rate.
 */
void check_seed_chan_name(int keep_channel_name);

/**\fn void read_sensor_list()
 *
 * Read the file 'sensor_list' in $PZ directory.
 *
 */
void read_sensor_list();

/**\fn struct sensorType *get_sensor_list()
 *
 * Give the list of sensors defined in sensor_list file.
 *
 * \return The list of defined sensor
 *
 */
struct sensorType *get_sensor_list();

/**\fn int get_sensor_list_size()
 *
 * Give the number of sensors defined in sensor_list file.
 *
 * \return The size of defined sensor list
 *
 */
int get_sensor_list_size();

/**\fn struct sensorSuperType *get_super_type_list()
 *
 * Give the list of sensors super type defined in super_type_list file.
 *
 * \return The defined sensor list
 *
 */
struct sensorSuperType *get_super_type_list();

/**\fn int get_super_type_list_size()
 *
 * Give the number of sensors super type defined in super_type_list file.
 *
 * \return The size of defined sensor list
 *
 */
int get_super_type_list_size();

/**\fn int get_sensor_datatype()
 *
 * Give the data type letter associated with this type of sensor.
 * (G or W).
 *
 * \param  The sensor
 * \return The sensor data type letter
 *
 */
char *get_sensor_datatype(char *string);

/**\fn int get_sensor_metamodel()
 *
 * Give the meta model letter associated with this type of sensor.
 *
 * \param  The sensor
 * \return The sensor meta model
 *
 */
char *get_sensor_metamodel(char *string);

#endif /* CHANNEL_H_ */
