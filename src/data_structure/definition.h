/**\file definition.h
 *
 * \brief Header of definitions, a way for manipulating named values.
 */

#ifndef DEFINITION_H_
#define DEFINITION_H_

/**
 * definition_t type.
 */
typedef struct definition_s *definition_t;

/**\struct definition_s
 *
 * \brief A simple data structure to manipulate list of named values.
 *
 */
struct definition_s {
	char         *value_name; /**< The name of defined value            */
	void         *value;      /**< The value associated with value_name */
	definition_t  next;       /**< The next definition                  */
};

/**\fn definition_t new_definition(char *value_name,void *value)
 *
 * \brief create a new definition_s.
 *
 * \param value_name The defined keyword
 * \param value      The value associated with this name
 *
 */
definition_t new_definition(char *value_name,void *value);

#endif /* DEFINITION_H_ */
