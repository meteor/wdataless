/**\file definition.c
 *
 *
 */

#include "definition.h"
#include "util.h"

definition_t new_definition(char *value_name,void *value) {
	definition_t result = mem(sizeof(struct definition_s));

	result->value_name = value_name;
	result->value      = value;
	result->next       = NULL;
	return result;
}
