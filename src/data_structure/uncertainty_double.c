/**\file definition.c
 *
 *
 */

#include "uncertainty_double.h"
#include "util.h"

uncertainty_double_t new_uncertaimty_double(double value,double *min_error, double *plus_error) {
	uncertainty_double_t result = mem(sizeof(struct uncertainty_double_s));

	result->value = value;
	result->min_error = NULL;
	if (min_error) {
	  result->min_error = mem(sizeof(double));
	  *(result->min_error) = *min_error;
	}
	result->plus_error = NULL;
	if (plus_error) {
	  result->plus_error = mem(sizeof(double));
	  *(result->plus_error)=  *plus_error;
	}
	return result;
}
