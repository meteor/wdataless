/*
 * main.h
 *
 *  Created on: 16 sept. 2011
 *      Author: leon
 */

#ifndef MAIN_H_
#define MAIN_H_
#include "station.h"

#define PROGRAM_NAME "wdataless"
enum {FALSE = 0, TRUE};

#define STATIONSDIR  "stations"
#define SENSORSDIR   "ana_filters"
#define FILTERSDIR   "dig_filters"
#define SENSORLIST   "sensor_list"
#define UNITLIST     "unit_list"

extern char seed_label[100];
extern char  *vol_fname;


#endif /* MAIN_H_ */
