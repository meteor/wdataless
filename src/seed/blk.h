#ifndef __LIBBLK_H__
#define __LIBBLK_H__

#include "station.h"
#include "channel.h"
#include "comment.h"


void write_channel_response(station_t, channel_t);
void blk_010(char *, char *);
void blk_011(int);
void blk_012(int);
void blk_030();
void blk_031(comment_t);
void blk_033();
void blk_034();
void blk_050(station_t);
void blk_051(comment_t);
void blk_052(station_t, channel_t);
void blk_053(filter_t, int, unit_t, unit_t);
void blk_054(filter_t, int, unit_t, unit_t);
void blk_057(digital_filter_t, int);
void blk_058(double, double, int);
void blk_059(comment_t);
void blk_061(filter_t, int, unit_t, unit_t);
void blk_062(filter_t p, int stage_num, unit_t inp_units,
             unit_t out_units, double channel_sint);

/**\fn void add_blockette(char *blk, char rec_type)
 *
 * Append a given blockette to the current logical record and create a new one if need.
 *
 * \param blk      The blockette to append
 * \param rec_type The logical record type
 */
void add_blockette(char *, char );


extern int nc;
extern int nVH, nAH, nSH;
extern char logrec[5000];


#endif
