/*\file dataless.h
 *
 */

#ifndef DATALESS_H_
#define DATALESS_H_

/**\fn void write_dataless(char *vol_name, FILE *seed_file)
 *
 * \brief Write dataless according to the station list stalist_head.
 *
 * \param vol_name  The name of the seed dataless filename
 * \param seed_file The FILE in which seed dataless is write
 */
void write_dataless(char *vol_name, FILE *seed_file);

/**\fn void set_blk010(char *blk)
 *
 * \brief Set the content of blockette 010.
 *
 * \param blk The content of blockette 010
 */
void set_blk010(char *blk);


/**\fn size_t get_blk010_size()
 *
 * \brief Give the size in bytes of blockette 010.
 *
 * \return The size of blockette 010 in bytes.
 */
size_t get_blk010_size();


/**\fn int get_nVH();
 *
 * \brief Give the size in elements of volume index control headers list
 *
 * \return The number of volume index control headers.
 */
int get_nVH();


/**\fn void set_blk011(char *blk)
 *
 * \brief Set the content of blockette 011.
 *
 * \param blk The content of blockette 011
 */
void save_one_blk011(char *blk);

/**\fn void set_blk012(char *blk)
 *
 * \brief Set the content of blockette 012.
 *
 * \param blk The content of blockette 012
 */
void set_blk012(char *blk);

#endif /* DATALESS_H_ */
