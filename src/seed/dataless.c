/**\file dataless.c
 *
 */

#include <stdio.h>
#include <string.h>
#include "errors.h"
#include "station.h"
#include "blk.h"
#include "comment.h"
#include "write_seed.h"
#include "time_date.h"
#include "dataless.h"
#include "util.h"

static int   calculated_nVH;
static char *blk010;

static char *blk012;


int get_nVH() {
  return calculated_nVH;
}

void set_blk010(char *blk) {
  blk010 = (char *) mem(strlen(blk) + 2);
  sprintf(blk010, "%s", blk);
}

size_t get_blk010_size() {
  return strlen(blk010);
}

void set_blk012(char *blk) {
  blk012 = (char *) mem(strlen(blk) + 2);
  sprintf(blk012, "%s", blk);
}


static int make_control_headers() {
  blk_010("1982,001,00:00:00.0000", "");
  free_VH();
  add_blockette("init", 'V');
  add_blockette(blk010, 'V');

  int calculated_nVH = find_nVH();
  blk_011((calculated_nVH + nAH + 1));

  blk_012(0);
  add_blockette(blk012, 'V');

  add_blockette("end", 'V');
  return calculated_nVH;
}


static void make_abbreviation_headers() {
  station_t sta;

  free_AH();
  add_blockette("init", 'A');
  blk_030();
  for (sta = stalist_head; sta != NULL; sta = sta->next) {
    list_t current;
    for (current = sta->comment; current != NULL; current = current->next) {
      comment_t current_comment = (comment_t) current->element;
      blk_031(current_comment);
    }
  }
  blk_033();
  blk_034();
  add_blockette("end", 'A');
}

static void make_station_headers(station_t sta) {
  int i;

  char s1[24], s2[24];

  epoch2str(sta->beg_utime, s1);
  if (sta->end_utime == 0)
    sprintf(s2, "present          ");
  else
    epoch2str(sta->end_utime, s2);

  /* Write logical record number into structure */

  sta->lrec_num = nSH;

  /* start new logical record */

  add_blockette("init", 'S');

  /* Station blockette */

  blk_050(sta);

  /* Station comment blockettes */
  /* Here we can add comments generation (blockette 51)*/

  list_t current;
  for (current = sta->comment; current != NULL; current = current->next) {
    comment_t current_comment = (comment_t) current->element;
    if (current_comment->type == Station) {
      blk_051(current_comment);
    }
  }

  /* Channels blockettes */
  for (i = 0; i < sta->nchannel; i++) {
    write_channel_response(sta, sta->channel[i]);
  }

  /* Flush out last log rec */
  add_blockette("end", 'S');

}

void write_dataless(char *vol_name,FILE *seed_file) {
  station_t sta;

  make_abbreviation_headers();

  // Make station for compute station index used in blk 011
  for (sta = stalist_head; sta != NULL; sta = sta->next) {
    char start_date[30];
    char end_date[30];
    epoch2str(sta->beg_utime,start_date);
    epoch2str(sta->end_utime,end_date);
    debug(DEBUG_SEED_WRITE, "Make station header for %s (%s-%s)\n",sta->name,start_date,end_date);
    make_station_headers(sta);
  }

  calculated_nVH = make_control_headers();



  /*       +==================================================+       */
  /*=======|                 Write SEED volume                |=======*/
  /*       +==================================================+       */

  // Write control header volumes
  write_control_logical_volumes(vol_name,seed_file,calculated_nVH);

  // Write abbreviation volumes
  write_abbreviation_logical_volumes(vol_name,seed_file);

  // Write station volumes
  write_station_logical_volumes(vol_name,seed_file);

  close_dataless_file(vol_name,seed_file);

}
