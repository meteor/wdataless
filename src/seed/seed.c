/**\file seed.c
 *
 */
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include "main.h"
#include "errors.h"
#include "seed.h"


int calc_channel_sint(station_t station) {
	channel_t chan = NULL;
	double sint = 0.0;
	int i, k;

	for (i = 0; i < station->nchannel; i++) {
		chan = station->channel[i];


		if (chan->reported_sint == 0.0) {
		  errors("%s %s : missing reported sint\n", station->name,chan->name);
		}

		if (chan->sint == chan->reported_sint) {
			return TRUE;
		}

		if (chan->adc->sint == 0.0) {
			errors("%s : null adc sint\n", chan->name);
		}


		/*
		 * calculate chan->sint
		 */
		if (chan->sint == 0.0) {
			sint = chan->adc->sint;
			for (k = 0; k < chan->ndigital_filter; k++) {
			  sint *= (double) chan->digital_filters[k].decim;
			} /* for (k=0; ...*/
			chan->sint = sint;
		}

		if (chan->sint != chan->reported_sint) {
			errors("chan %s : sint %.10E differ from reported: %.10E diff %.10E \n", chan->name,chan->sint, chan->reported_sint,(chan->sint - chan->reported_sint));
		}
	} /* for (i=0; ...) */
	return TRUE;
}

void check_sensor_orient(station_t station) {
	channel_t chan;
	int i;

	for (i = 0; i < station->nchannel; i++) {
		chan = station->channel[i];
		if (chan->name[2] == 'Z') {
			if (chan->sensor->azimuth > 5.0 && chan->sensor->azimuth < 355) {
				messages("%s %s azim=%.2f differ from expected=0.0 (Seed convention)\n", station->name,
						chan->name, chan->sensor->azimuth);
			}
			if (fabs(chan->sensor->dip - (-90.0)) > 0.01) {
				messages("%s %s dip=%.2f differ from expected=-90.0 (Seed convention)\n", station->name,chan->name, chan->sensor->dip);
			}
		}

		if (chan->name[2] == 'N') {
			if (chan->sensor->azimuth > 5.0  && chan->sensor->azimuth < 355) {
				messages("%s %s azim=%.2f differ from expected=0.0 (Seed convention)\n", station->name,
						chan->name, chan->sensor->azimuth);
			}
			if (fabs(chan->sensor->dip) > 0.01) {
				messages("%s %s dip=%.2f differ from expected=0.0 (Seed convention)\n", station->name,
						chan->name, chan->sensor->dip);
			}
		}
		if (chan->name[2] == 'E') {
			if (fabs(chan->sensor->azimuth - 90.0) > 5.0) {
				messages("%s %s azim=%.2f differ from expected=90.0 (Seed convention)\n", station->name,
						chan->name, chan->sensor->azimuth);
			}
			if (fabs(chan->sensor->dip) > 0.01) {
				messages("%s %s dip=%.2f differ from expected=0.0 (Seed convention)\n", station->name,
						chan->name, chan->sensor->dip);
			}
		}
	} /* for (i=0;... */
}


int check_sym(filter_t filter) {
	double sum = 0.0;
	int nc, n0, k;

	nc = filter->ncoef;

	/* CHECK IF IF FILTER IS NORMALIZED TO 1 AT FREQ 0 */

	for (k = 0; k < nc; k++) {
		sum += filter->coef[k];
	}

	if (sum < (1.0 - FIR_NORM_TOL) || sum > (1.0 + FIR_NORM_TOL)) {
		warnings("FIR normalized: sum[coef]=%E nc=%d\n", sum,nc);
		for (k = 0; k < nc; k++) {
			filter->coef[k] /= sum;
		}
	}

	if (filter->type == FIR_ASYM) {
		return TRUE;
	}

	/* CHECK IF FILTER IS SYMETRICAL WITH EVEN NUM OF WEIGHTS */

	if ((nc % 2) == 0) {
		n0 = nc / 2;
		for (k = 0; k < n0; k++) {
			if (filter->coef[n0 + k] != filter->coef[n0 - k - 1]) {
				errors("check_sym: coefficients are not symmetrical\nn=%d coef=%.8E\nn=%d coef=%.8E\n",n0 + k,
						filter->coef[n0 + k],n0 - k - 1,
						filter->coef[n0 - k - 1]);
			}
		}
		return TRUE;
	} else {
		/* CHECK IF FILTER IS SYMETRICAL WITH ODD NUM OF WEIGHTS */
		n0 = (nc - 1) / 2;
		for (k = 1; k < nc - n0; k++) {
			if (filter->coef[n0 + k] != filter->coef[n0 - k]) {
				errors("check_sym: coefficients are not symmetrical\nn=%d coef=%.8E\nn=%d coef=%.8E\n", n0 + k,
						filter->coef[n0 + k], n0 - k,
						filter->coef[n0 - k]);
			}
		}
		return TRUE;
	}
}


int check_roots(double root[], int nroot, int type, char *p_z) {
	int i;
	double err;

	for (i = 0; i < nroot * 2; i += 2) {
		if (type != IIR_PZ && root[i] > 0.0) {
			warnings("check_roots: %s real part not negative\n", p_z);
		}
	}

	for (i = 0; i < nroot * 2;) {
		if (root[i + 1] == 0.) {
			i += 2;
			continue;
		}
		if (root[i] != root[i + 2]) {
			err = fabs((root[i] - root[i + 2]) / root[i]);
			if (err > 5.e-07) {
				errors("check_roots: %s real parts not equal %.10e %.10e\nerr = %.7e\n",
						p_z, root[i], root[i + 2],err);
			}
		}
		if ((root[i + 1] + root[i + 3]) != 0.) {
			err = fabs((root[i + 1] + root[i + 3]) / root[i + 1]);
			if (err > 5.e-07) {
				errors("check_roots: %s imag parts not opposite %.10e %.10e\nerr = %.7e\n",
						p_z, root[i + 1], root[i + 3],err);
			}
		}
		i += 4;
	}
	return TRUE;
}
