#include <string.h>
#include <time.h>
#include <stdlib.h>
#include <math.h>
#include "parse.h"
#include "blk.h"
#include "seed.h"
#include "units.h"
#include "owner.h"
#include "util.h"
#include "time_date.h"
#include "errors.h"
#include "main.h"
#include "dataless.h"
#include "write_seed.h"


char blk[20000];
double SeedClockTolerance;
int SeedDataformat;
int nc;
char logrec[5000];

void blk_010(char *vol_beg, char *vol_end) {
  int j;
  long time_now;
  char temp[24];

  j = 0;
  sprintf(&blk[j], "010");
  j += 3;
  sprintf(&blk[j], "%4d", 0);
  j += 4;
  sprintf(&blk[j], "%4.1f", SEED_REV);
  j += 4;
  sprintf(&blk[j], "%.2s", LREC_LEN);
  j += 2;
  sprintf(&blk[j], "%s~", vol_beg);
  j += strlen(vol_beg) + 1;
  sprintf(&blk[j], "%s~", vol_end);
  j += strlen(vol_end) + 1;
  time_now = time(NULL);
  epoch2str((double) time_now, temp);
  sprintf(&blk[j], "%s~", temp);
  j += strlen(temp) + 1;
  sprintf(&blk[j], "%s~", get_seed_organization());
  j += strlen(get_seed_organization()) + 1;
  sprintf(&blk[j], "%s~", seed_label);
  j += strlen(seed_label) + 1;

  sprintf(temp, "%04d", (int)strlen(blk));
  strncpy(&blk[3], temp, 4);

  set_blk010(blk);
}

void blk_011(int recn_ofs) {
  char *blk;
  station_t sta;
  int nsta = 0;
  char temp[12];
  int j;

  debug(DEBUG_SEED_WRITE, "logical record number offset = %d\n", recn_ofs);
  /* Template: 0110032002SSB  000003BNG  000006...*/

  /* Allocate memory */
  blk = (char *) mem(4097);

  j = 0;
  sprintf(blk, "0110000");
  j+= 7;

  j+= 3; // skipping numbers of stations
  for (sta = stalist_head; sta != NULL; sta = sta->next) {

    if(j + 11 >= 4096) {
      // If the blockette if full write it and
      // create a new one.

      /* Write number of stations */
      sprintf(temp,"%03d", nsta);
      strncpy(blk + 7, temp, 3);
      add_blockette(blk, 'V');
      memset(blk + 7,0,4096-7);
      nsta = 0;
      j = 10;
    }
    nsta++;
    /* Write station code */
    sprintf(temp, "     ");
    strncpy(temp, sta->name, strlen(sta->name));
    sprintf(blk + j, "%5s", temp);
    j+=5;
    /* Write corresponding station headers logical record number */
    sprintf(blk + j , "%06d", (recn_ofs + sta->lrec_num));
    j+=6;
  }
  sprintf(temp,"%03d", nsta);
  strncpy(&blk[7], temp, 3);
  add_blockette(blk, 'V');
  free(blk);
}

void blk_012(int ntheader) {
  char blk[20];
  int j;


  j = 0;
  sprintf(&blk[j], "012");
  j += 3;
  sprintf(&blk[j], "%04d", 0);
  j += 4;
  sprintf(&blk[j], "%04d", ntheader);
  j += 4;

  set_blk012(blk);
}

/*===================================================================*
 *  Build blockette 30: data format dictionary
 *
 * key 1: M0      no multiplexing
 * key 2: W3      copy 3 bytes in working buffer without reordering
 *        D0-23   extract bits 0 to 23
 *        C2      use 2's complement
 *
 0300104Geoscope gain-range on 3 bits
 ~000100104M3I0L2~W2 D0-11 A-2048~D12-14~E2:0:-1~
 ~000100104M0~W2 D0-11 A-2048~D12-14~E2:0:-1~
 0300085Geoscope-3 byte (switched gain)
 ~000200002M3I0L3~W3 D0-23 C2~
 ~000200002M0~W3 D0-23 C2~
 0300104Geoscope gain range on 4 bits
 ~000300104M3I0L2~W2 D0-11 A-2048~D12-15~E2:0:-1~
 ~000300104M0~W2 D0-11 A-2048~D12-15~E2:0:-1~

 "030 232Steim Integer Compression Format~"
 "0004050 6"
 "F1 P4 W4 D0-31 C2 R1 P8 W4 D0-31 C2~"
 "P0 W4 N15 S2,0,1~"
 "T0 X N0 W4 D0-31 C2~"
 "T1 N0 W1 D0-7 C2 N1 W1 D0-7 C2 N2 W1 D0-7 C2 N3 W1 D0-7 C2~"
 "T2 N0 W2 D0-15 C2 N1 W2 D0-15 C2~"
 "T3 N0 W4 D0-31 C2~"

 key 1:
 M0      no multiplexing
 key 2:
 W2      copy 2 bytes in working buffer without reordering
 D0-15   extract bits 0 to 15
 D0-23   extract bits 0 to 23
 C2      use 2's complement
 }

 *===================================================================*/
void blk_030() {
  int j;
  int dataformat_lookup; /* data format lookup code */
  int data_family_type; /* family type: 0=integers, 1=gain-ranged, 50=integer differences compression */
  int num_of_keys; /* number of keys */
  char desc[50];
  char keys[300];

  sprintf(desc, "Steim Integer Compression Format~");
  dataformat_lookup = STEIM1;
  data_family_type = 50;
  num_of_keys = 6;
  sprintf(keys, "M0~W2 D0-11 A-2048~D12-15~E2:0:-1~");
  j = 0;
  sprintf(&blk[j], "0300000");
  j += 7;
  sprintf(&blk[j], "%s", desc);
  j += strlen(desc);
  sprintf(&blk[j], "%04d", dataformat_lookup);
  j += 4;
  sprintf(&blk[j], "%03d", data_family_type);
  j += 3;
  sprintf(&blk[j], "%02d", num_of_keys);
  j += 2;
  sprintf(&blk[strlen(blk)], "F1 P4 W4 D0-31 C2 R1 P8 W4 D0-31 C2~");
  sprintf(&blk[strlen(blk)], "P0 W4 N15 S2,0,1~");
  sprintf(&blk[strlen(blk)], "T0 X N0 W4 D0-31 C2~");
  sprintf(&blk[strlen(blk)], "T1 N0 W1 D0-7 C2 N1 W1 D0-7 C2 N2 W1 D0-7 C2 N3 W1 D0-7 C2~");
  sprintf(&blk[strlen(blk)], "T2 N0 W2 D0-15 C2 N1 W2 D0-15 C2~");
  sprintf(&blk[strlen(blk)], "T3 N0 W4 D0-31 C2~");
  add_blockette(blk, 'A');


  ///////////////////

  sprintf(desc, "Steim2 Integer Compression Format~");
  dataformat_lookup = STEIM2;
  data_family_type = 50;
  num_of_keys = 14;
  j = 0;
  sprintf(&blk[j], "0300000");
  j += 7;
  sprintf(&blk[j], "%s", desc);
  j += strlen(desc);
  sprintf(&blk[j], "%04d", dataformat_lookup);
  j += 4;
  sprintf(&blk[j], "%03d", data_family_type);
  j += 3;
  sprintf(&blk[j], "%02d", num_of_keys);
  j += 2;
  sprintf(&blk[strlen(blk)], "F1 P4 W4 D C2 R1 P8 W4 D C2~");
  sprintf(&blk[strlen(blk)], "P0 W4 N15 S2,0,1~");
  sprintf(&blk[strlen(blk)], "T0 X W4~");
  sprintf(&blk[strlen(blk)], "T1 Y4 W1 D C2~");
  sprintf(&blk[strlen(blk)], "T2 W4 I D2~");
  sprintf(&blk[strlen(blk)], "K0 X D30~");
  sprintf(&blk[strlen(blk)], "K1 N0 D30 C2~");
  sprintf(&blk[strlen(blk)], "K2 Y2 D15 C2~");
  sprintf(&blk[strlen(blk)], "K3 Y3 D10 C2~");
  sprintf(&blk[strlen(blk)], "T3 W4 I D2~");
  sprintf(&blk[strlen(blk)], "K0 Y5 D6 C2~");
  sprintf(&blk[strlen(blk)], "K1 Y6 D5 C2~");
  sprintf(&blk[strlen(blk)], "K2 X D2 Y7 D4 C2~");
  sprintf(&blk[strlen(blk)], "K3 X D30~");
  add_blockette(blk, 'A');


  /////////////////////////
  sprintf(desc, "SUN IEEE Floats~");
  dataformat_lookup = IEEEFLOAT;
  data_family_type = 0;
  num_of_keys = 2;
  sprintf(keys, "M0~W4 D0-31 C2~");
  j = 0;
  sprintf(&blk[j], "0300000");
  j += 7;
  sprintf(&blk[j], "%s", desc);
  j += strlen(desc);
  sprintf(&blk[j], "%04d", dataformat_lookup);
  j += 4;
  sprintf(&blk[j], "%03d", data_family_type);
  j += 3;
  sprintf(&blk[j], "%02d", num_of_keys);
  j += 2;
  sprintf(&blk[j], "%s", keys);
  j += strlen(keys);
  add_blockette(blk, 'A');

  sprintf(desc, "32-bit Integers~");
  dataformat_lookup = INT32;
  data_family_type = 0;
  num_of_keys = 2;
  sprintf(keys, "M0~W4 D0-31 C2~");
  j = 0;
  sprintf(&blk[j], "0300000");
  j += 7;
  sprintf(&blk[j], "%s", desc);
  j += strlen(desc);
  sprintf(&blk[j], "%04d", dataformat_lookup);
  j += 4;
  sprintf(&blk[j], "%03d", data_family_type);
  j += 3;
  sprintf(&blk[j], "%02d", num_of_keys);
  j += 2;
  sprintf(&blk[j], "%s", keys);
  j += strlen(keys);
  add_blockette(blk, 'A');

}

void blk_031(comment_t comment) {
  sprintf(&blk[0], "0310000");
  sprintf(&blk[strlen(blk)], "%04d", comment->seed_key);
  if (comment->type == Station) {
    sprintf(&blk[strlen(blk)], "%c", 'S');
  } else if (comment->type == Channel) {
    sprintf(&blk[strlen(blk)], "%c", 'C');
  } else {
    errors("%s (%d)\n", "Unexpected network comment", comment->type);
  }
  sprintf(&blk[strlen(blk)], "%s~", comment->value);
  sprintf(&blk[strlen(blk)], "000");
  add_blockette(blk, 'A');
}

void blk_033() {
  char *owner_name;
  int lookup_code = 1;
  int j;


  // Add abbreviation for sensor description
  struct sensorSuperType *sensor_list = get_super_type_list();
  //struct sensorType *sensor_list = get_sensor_list();

  while(sensor_list != NULL) {
    j = 0;
    sprintf(&blk[j], "0330000%03d", lookup_code++);
    j += 10;
    char *desc;
    if (strlen(sensor_list->description) > 50) {
      messages("Cut description <%s> because size > 50\n", sensor_list->description);
    }
    desc = strcut_utf8(sensor_list->description, 50);
    sprintf(&blk[j], "%s~", desc);
    add_blockette(blk, 'A');
    sensor_list = sensor_list->next;
  }

  owner_iterator(TRUE);
  while ((owner_name = owner_iterator(FALSE)) != NULL) {
    // Add abbreviation for station owner
    j = 0;
    sprintf(&blk[j], "0330000%03d", lookup_code++);
    j += 10;

    /* Trunk owner name to 50 char max */
    char *owner_name_copy;
    if (strlen(owner_name) > 50) {
      messages("Cut owner <%s> because size > 50\n", owner_name);
    }
    owner_name_copy = strcut_utf8(owner_name, 50);

    sprintf(&blk[j], "%s~", owner_name_copy);
    add_blockette(blk, 'A');
  }
}

void blk_034() {
  int j;

  list_t current_node = get_unit_list_head();
  while(current_node) {
    unit_t current_unit = (unit_t) current_node->element;
    j = 0;
    sprintf(&blk[j], "0340000%03d", current_unit->lookup_code);
    j += 10;
    sprintf(&blk[j], "%s~%s~", current_unit->seed_code, current_unit->description);
    add_blockette(blk, 'A');
    current_node = current_node->next;
  }
}

/**\fn void blk_050(station_t sta)
 *
 */
void blk_050(station_t sta) {
  location_t loc;
  int owner_lookup_code; /* field 10 */
  char temp[24];
  char begtime[24], endtime[24];
  int i, j;

  epoch2str(sta->beg_utime, temp);
  sprintf(begtime, "%.17s~", temp);
  if (sta->end_utime == 0.0) {
    sprintf(endtime, "2500,365,23:59:59.9999~");
  } else {
    epoch2str(sta->end_utime, temp);
    sprintf(endtime, "%.17s~", temp);
  }

  if ((owner_lookup_code = get_owner_abbreviation_index(sta->owner)) == -1) {
    errors("can't find owner (%s) in the abbreviation list",
        sta->owner);
  }

  loc = NULL;
  for (i = 0; i < sta->nlocation; i++) {
    if (!strncmp(sta->location[i]->label, STALOCID, 2)) {
      loc = sta->location[i];
      break;
    }
  }
  if (loc == NULL) {
    errors("can't find station location for: %s\n", sta->name);
  }

  debug(DEBUG_SEED_WRITE, "Channel '%4i'\n",sta->nchannel);
  j = 0;
  sprintf(&blk[j], "0500000");
  j += 7;
  sprintf(&blk[j], "     ");
  strncpy(&blk[j], sta->name, strlen(sta->name));
  j += 5;
  sprintf(&blk[j], "%+10.6f", sta->location[0]->lat->value);
  j += 10;
  sprintf(&blk[j], "%+11.6f", sta->location[0]->lon->value);
  j += 11;
  sprintf(&blk[j], "%+7.1f", sta->location[0]->elev->value);
  j += 7;
  sprintf(&blk[j], "%4i", sta->nchannel);
  j += 4;
  sprintf(&blk[j], "%3i", 0); // Number of channel comment
  j += 3;
  /* Trunk description to 60 char max */
  char *desc;
  if (strlen(sta->location[0]->desc) > 60) {
    messages("Cut description <%s> because size > 60\n", sta->location[0]->desc);
  }
  desc = strcut_utf8(sta->location[0]->desc, 60);
  sprintf(&blk[j], "%s~", desc);
  j += strlen(desc) + 1;

  sprintf(&blk[j], "%3d", owner_lookup_code);
  j += 3;
  sprintf(&blk[j], "%s", "321010");
  j += 6;
  sprintf(&blk[j], "%s", begtime);
  j += strlen(begtime);
  sprintf(&blk[j], "%s", endtime);
  j += strlen(endtime);
  sprintf(&blk[j], "N");
  j += 1;
  sprintf(&blk[j], "  ");
  strncpy(&blk[j], sta->network, strlen(sta->network));
  add_blockette(blk, 'S');
}

void blk_051(comment_t comment) {
  char temp[255];

  sprintf(&blk[0], "0510000");
  if (comment->begin_time != NULL) {
    epoch2str(*(comment->begin_time), temp);
    sprintf(&blk[strlen(blk)], "%s~", temp);
  }
  if (comment->end_time == NULL || *(comment->end_time) == 0.0) {
    sprintf(&blk[strlen(blk)], "%s~", "2500,365,23:59:59.9999");
  } else {
    epoch2str(*(comment->end_time), temp);
    sprintf(&blk[strlen(blk)], "%s~", temp);
  }
  sprintf(&blk[strlen(blk)], "%04d", comment->seed_key);
  sprintf(&blk[strlen(blk)], "000000");
  add_blockette(blk, 'S');
}

void write_channel_response(station_t station, channel_t channel) {

  unit_t sensor_outUnits, inp_units, out_units;
  double global_gain = 1;

  blk_052(station, channel);

  /**================  Sensor ===================*/
  int stage_num = 1;
  sensor_outUnits = get_unit_from_string("V");
  if (strcmp(channel->sensor->outUnits->seed_code, "V")) {
    errors("unsupported sensor out units (%s) for: %s %s\n",
           channel->sensor->outUnits->seed_code, station->name,
           channel->name);
  }
  if (channel->sensor->filter && (channel->sensor->filter->type == ANALOG || channel->sensor->filter->type == LAPLACE)) {
    blk_053(channel->sensor->filter, stage_num, channel->sensor->inpUnits,
            sensor_outUnits);

    blk_058(channel->sensor->filter->Sd, channel->sensor->filter->fn, stage_num);
  } else if (channel->sensor->filter && (channel->sensor->filter->type == POLYNOMIAL)) {
    blk_062(channel->sensor->filter, stage_num,
            channel->sensor->inpUnits, sensor_outUnits,
            channel->sint);
  } else {
    if (channel->sensor->filter == NULL) {
        errors("%s\n", "Sensor filter is NULL");
    } else {
        errors("Sensor filter type is not supported %d\n", channel->sensor->filter->type);
    }
  }

  double normalization_frequency = channel->sensor->filter->fn;
  /*================  sensor analog filter  ===================*/
  //if (channel->sensor->filter && channel->sensor->filter->type == ANALOG) {
  //  ++stage_num;
  //
  //  inp_units_lookup = V;
  //  out_units_lookup = V;

  //  blk_053(channel->sensor->filter, stage_num, inp_units_lookup,
  //      out_units_lookup);

  //  blk_058(channel->sensor->filter->Sd, channel->sensor->filter->fn, stage_num);
  //}

  /*=================  amplifier ====================*/
  int i, j;
  for (i = 0; i < channel->nanalog_filter; i++) {
    analog_filter_t current_analog_filter = channel->analog_filters + i;
    /* blk_058 */
    for (j = 0; j < current_analog_filter->nstage; j++) {
      ++stage_num;
      if (current_analog_filter->stages[j]->filter) {
        debug(DEBUG_SEED_WRITE, "****analog filter gain = %e\n", current_analog_filter->stages[j]->filter->Sd);
        inp_units = get_unit_from_string("V");
        if (strcmp(current_analog_filter->stages[j]->inpUnits->seed_code, "V")) {
          errors("unsupported ampli units %s\n",
                current_analog_filter->stages[j]->inpUnits->seed_code);
        }
        if (strcmp(current_analog_filter->stages[j]->outUnits->seed_code, "V")) {
          errors("unsupported ampli output unit %s\n",
                current_analog_filter->stages[j]->outUnits->seed_code);
        }

        blk_053(current_analog_filter->stages[j]->filter, stage_num, inp_units,
                current_analog_filter->stages[j]->outUnits);

        blk_058(current_analog_filter->stages[j]->filter->Sd,
                current_analog_filter->stages[j]->filter->fn,
                stage_num);
        normalization_frequency = current_analog_filter->stages[j]->filter->fn;
        global_gain *= current_analog_filter->stages[j]->filter->Sd;
      } else {
        debug(DEBUG_SEED_WRITE, "****analog filter gain = %e\n", current_analog_filter->stages[j]->ampli_G);
        blk_058((current_analog_filter->stages[j]->ampli_G), normalization_frequency, stage_num);
        global_gain *= current_analog_filter->stages[j]->ampli_G;
      }
    }
  }

  /*=================  Digitizer ====================*/
  ++stage_num;
  inp_units = get_unit_from_string("V");
  out_units = get_unit_from_string("COUNTS");

  /* blk_054 */
  j = 0;
  sprintf(&blk[j], "0540000");
  j += 7;
  sprintf(&blk[j], "D");
  j += 1;
  sprintf(&blk[j], "%02d", stage_num);
  j += 2;
  sprintf(&blk[j], "%03d", inp_units->lookup_code);
  j += 3;
  sprintf(&blk[j], "%03d", out_units->lookup_code);
  j += 3;
  sprintf(&blk[j], "%04d", 0);
  j += 4;
  sprintf(&blk[j], "%04d", 0);
  j += 4;
  add_blockette(blk, 'S');

  /* blk_057 */
  j = 0;
  sprintf(&blk[j], "0570000");
  j += 7;
  sprintf(&blk[j], "%02d", stage_num);
  j += 2;
  if ((double) channel->adc->sint != (double) 0.0) {
    sprintf(&blk[j], "%10.4E", (1.0 / channel->adc->sint));
  } else {
    sprintf(&blk[j], "%10.4E", (1.0 / channel->sint));
  }
  j += 10;
  sprintf(&blk[j], "%05d", 1);
  j += 5;
  sprintf(&blk[j], "%05d", 0);
  j += 5;
  sprintf(&blk[j], "%11.4E", 0.0);
  j += 11;
  sprintf(&blk[j], "%11.4E", 0.0);
  j += 11;
  add_blockette(blk, 'S');

	/* blk_058 */
  blk_058(channel->adc->fact, 0 , stage_num);
  global_gain *= channel->adc->fact;
  /*=================  Digital filters  ====================*/

  debug(DEBUG_SEED_WRITE,"%s : %d digital filter\n", station->name, channel->ndigital_filter);
  for (i = 0; i < channel->ndigital_filter; i++) {
    ++stage_num;
    inp_units = get_unit_from_string("COUNTS");
    out_units = get_unit_from_string("COUNTS");

    if (channel->digital_filters[i].filter->type == IIR_PZ) {
      blk_053(channel->digital_filters[i].filter, stage_num, inp_units,
              out_units);
    } else if (channel->digital_filters[i].filter->type == IIR_COEF) {
    	blk_054(channel->digital_filters[i].filter,
    			    stage_num, inp_units, out_units);
    } else if (channel->digital_filters[i].filter->type == IIR_PZ) {
        blk_053(channel->digital_filters[i].filter, stage_num, inp_units,
                out_units);
    } else {
    	blk_061(channel->digital_filters[i].filter, stage_num,
              inp_units, out_units);
    }
    blk_057(channel->digital_filters + i, stage_num);
    if (channel->digital_filters[i].filter->type == IIR_PZ || channel->digital_filters[i].filter->type == IIR_COEF) {
      blk_058(channel->digital_filters[i].filter->Sd, channel->digital_filters[i].filter->fn, stage_num);
    } else {
      blk_058(1.0, 0, stage_num);
    }
  }

  /*==============  Last stage: 0, overall sensitivity ====*/
  if (channel->sensor->filter != NULL) {
      if (channel->sensor->filter->type == POLYNOMIAL) {
        filter_t global_filter = create_filter();
        out_units = get_unit_from_string("COUNTS");

        // Copy sensor filter parameters
        global_filter->lower_bound_approx = channel->sensor->filter->lower_bound_approx;
        global_filter->upper_bound_approx = channel->sensor->filter->upper_bound_approx;
        global_filter->valid_frequency_unit = channel->sensor->filter->valid_frequency_unit;
        global_filter->ncoef = channel->sensor->filter->ncoef;
        global_filter->coef = (double *)mem(global_filter->ncoef * sizeof(double));

        // apply global channel gain to each coef
        double current_factor = 1;
        for (i=0 ; i < global_filter->ncoef; i++) {
          global_filter->coef[i] = channel->sensor->filter->coef[i] / current_factor;
          current_factor *= global_gain;
        }
        blk_062(global_filter, 0,
                channel->sensor->inpUnits, out_units,
                channel->sint);
      } else {
        blk_058(channel->Sens, normalization_frequency, 0);
      }
  }

  /*=============== Write channel comments ===== */
  list_t current = channel->comment;
  while(current != NULL) {
    blk_059((comment_t)current->element);
    current = current->next;
  }
}

void blk_052(station_t sta, channel_t cha) {
  unit_t sensor_inpUnits = NULL;
  int sensor_lookup = 0;
  int i, j;
  char temp[24];
  char channel_flags[27];
  char begtime[24], endtime[24];
  location_t loc;
  unit_t calib_units;
  double max_clock_drift; /* this variable called clock_tolerance in rdseed */
  //int maxSamplesPerRecord = 0;

  /*
   * Set data records data format
   */
  cha->adc->dataformat = 0;

  if (cha->encoding != STEIM1 && /* Steim Integer Compression */
      cha->encoding != STEIM2 && /* Steim2 Integer Compression */
      cha->encoding != INT32 && /* 32-bit Integers */
      cha->encoding != IEEEFLOAT) { /* 32-bit Floats */
    errors("unsupported data format %d for %s %s\n",cha->encoding, sta->name,
        cha->name);
  }

  /**
   * Set maximum clock drift;
   * This variable is called clock_tolerance in rdseed.
   * Its value will be such as 'max_clock_drift' cumulated
   * over the number of samples in a record should be less than
   * 1/20 of the sample interval.
   * We assume that there are at most:
   *    4048 / 2 = 2000 samples in a compressed data record of 4096 bytes.
   *    4048 / 4 = 1000 samples in a Float or INT_4 data record.
   */

/*
  if (cha->encoding == IEEEFLOAT || cha->encoding == INT32) {
    maxSamplesPerRecord = 1020;
  } else if (cha->encoding == STEIM1) {
    maxSamplesPerRecord = 2000;
  } else if (cha->encoding == STEIM2) {
    warnings("%s\n", "Maximum sample per record for STEIM2 must be verified");
    maxSamplesPerRecord = 2000;
  }

  if (SeedClockTolerance == 0.0) {
    max_clock_drift = (cha->sint / 20.0) / (double) maxSamplesPerRecord;
  } else {
    max_clock_drift = SeedClockTolerance;
  }
*/
  max_clock_drift = .00005;
  sensor_inpUnits = cha->sensor->inpUnits;

  if (strlen(cha->channel_flags) > 26) {
    errors("Channel flags %s (%d) is too long", cha->channel_flags, strlen(cha->channel_flags));
  }
  sprintf(channel_flags, "%s~", cha->channel_flags);

  char *meta_model = get_sensor_metamodel(cha->sensor->type);
  sensor_lookup = dbencode(meta_model);
  if (sensor_lookup == UNKNOWN) {
    errors("unsupported sensor type %s\n",
        cha->sensor->type);
  }
  calib_units = cha->sensor->calibrationUnits;

  epoch2str(cha->beg_utime, temp);
  sprintf(begtime, "%.17s~", temp);
  if (cha->end_utime == 0.0) {
    sprintf(endtime, "2500,365,23:59:59.9999~");
  } else {
    epoch2str(cha->end_utime, temp);
    sprintf(endtime, "%.17s~", temp);
  }
  debug(DEBUG_SEED_WRITE,"Generate channel identifier for %s-%s (%s-%s)\n",cha->name, sta->name,begtime,endtime);
  /**
   * Check channel location ID
   */
  loc = NULL;
  if (sta->nlocation == 1) {
    loc = sta->location[0];
  } else {
    for (i = 0; i < sta->nlocation; i++) {
      if (!strcmp(cha->location, sta->location[i]->label)) {
        loc = sta->location[i];
        break;
      }
    }
    if (loc == NULL) {
      errors("can't find channel location %s for: %s %s\n", cha->location, sta->name, cha->name);
    }
  }

  j = 0;
  sprintf(&blk[j], "0520000");
  j += 7;
  sprintf(&blk[j], "  ");
  strncpy(&blk[j], loc->code, 2);
  j += 2;
  sprintf(&blk[j], "%.3s", cha->name);
  j += 3;
  sprintf(&blk[j], "0000");
  j += 4;
  sprintf(&blk[j], "%3d", sensor_lookup);
  j += 3;
  sprintf(&blk[j], "~");
  j += 1;
  sprintf(&blk[j], "%3d", sensor_inpUnits->lookup_code);
  j += 3;
  sprintf(&blk[j], "%3d", calib_units->lookup_code);
  j += 3;
  sprintf(&blk[j], "%+10.6f", loc->lat->value);
  j += 10;
  sprintf(&blk[j], "%+11.6f", loc->lon->value);
  j += 11;
  sprintf(&blk[j], "%+7.1f", loc->elev->value);
  j += 7;
  if (loc->depth < 0) {
    warnings("station %s %s The value of depth must be positive and his value is %e\n", sta->name, cha->name, loc->depth);
    warnings("%s", "Take the absolute value to conform seed 2.4 norm\n");
  }
  sprintf(&blk[j], "%5.1f", fabs(loc->depth));
  j += 5;
  sprintf(&blk[j], "%5.1f", (float)((int)round(cha->sensor->azimuth) % 360));
  // If azimuth = 000.0 then we must write 360.0
  // 000.0 is for unknown value (not yet supported)
  //if (strncmp(&blk[j], "000.0", 5) == 0) {
  //  sprintf(&blk[j], "360.0");
  //}
  j += 5;
  sprintf(&blk[j], "%+5.1f", cha->sensor->dip);
  j += 5;
  sprintf(&blk[j], "%04d", cha->encoding);
  j += 4;
  sprintf(&blk[j], "12");
  j += 2;
  sprintf(&blk[j], "%10.4E", 1.0 / cha->sint);
  j += 10;
  sprintf(&blk[j], "%10.4E", max_clock_drift);
  j += 10;
  sprintf(&blk[j], "%4d", 0);
  j += 4;
  sprintf(&blk[j], "%s", channel_flags);
  j += strlen(channel_flags);
  sprintf(&blk[j], "%s", begtime);
  j += strlen(begtime);
  sprintf(&blk[j], "%s", endtime);
  j += strlen(endtime);
  sprintf(&blk[j], "N");
  j += 1;

  add_blockette(blk, 'S');
}

void blk_053(filter_t p, int stage_num, unit_t inp_units,
             unit_t out_units) {
  int i, j;

  j = 0;
  sprintf(&blk[j], "0530000");
  j += 7;
  if (p->type == LAPLACE) {
    sprintf(&blk[j], "A");
  } else if (p->type == ANALOG) {
    sprintf(&blk[j], "B");
  } else if (p->type == IIR_PZ) {
    sprintf(&blk[j], "D");
  } else {
    errors("Unsupported filter type %d", p->type);
  }
  j += 1;
  sprintf(&blk[j], "%02d", stage_num);
  j += 2;
  sprintf(&blk[j], "%03d", inp_units->lookup_code);
  j += 3;
  sprintf(&blk[j], "%03d", out_units->lookup_code);
  j += 3;
  sprintf(&blk[j], "%+12.5E", p->A0);
  j += 12;
  sprintf(&blk[j], "%+12.5E", p->fn);
  j += 12;
  sprintf(&blk[j], "%03d", p->nzero);
  j += 3;
  for (i = 0; i < p->nzero * 2; i += 2) {
    sprintf(&blk[j], "%+12.5E%+12.5E", p->zero[i], p->zero[i + 1]);
    j += 12 * 2;
    sprintf(&blk[j], "%+12.5E%+12.5E", 0.0, 0.0);
    j += 12 * 2;
  }
  sprintf(&blk[j], "%03d", p->npole);
  j += 3;
  for (i = 0; i < p->npole * 2; i += 2) {
    sprintf(&blk[j], "%+12.5E%+12.5E", p->pole[i], p->pole[i + 1]);
    j += 12 * 2;
    sprintf(&blk[j], "%+12.5E%+12.5E", 0.0, 0.0);
    j += 12 * 2;
  }
  add_blockette(blk, 'S');
}


static void blk_054_init(int *current_index, int stage_num, unit_t inp_units,
                         unit_t out_units, filter_type_t type) {
  *current_index = 0;
  sprintf(&blk[*current_index], "0540000");
  *current_index += 7;
  if (type == ANALOG) {
    sprintf(&blk[*current_index], "B");
  } else if (type == LAPLACE) {
    sprintf(&blk[*current_index], "A");
  } else if (type == IIR_COEF) {
    sprintf(&blk[*current_index], "D");
  } else {
    errors("Unsupported filter type %d", type);
  }
  *current_index += 1;
  sprintf(&blk[*current_index], "%02d", stage_num);
  *current_index += 2;
  sprintf(&blk[*current_index], "%03d", inp_units->lookup_code);
  *current_index += 3;
  sprintf(&blk[*current_index], "%03d", out_units->lookup_code);
  *current_index += 3;
}

void blk_054(filter_t filter, int stage_num, unit_t inp_units,
             unit_t out_units) {
  int i, j;

  blk_054_init(&j, stage_num, inp_units, out_units, filter->type);
  if (filter->type == IIR_COEF) {
    sprintf(&blk[j], "%04d", filter->nzero);
    j += 4;
    for (i = 0; i < filter->nzero; i++) {
      sprintf(&blk[j], "%12.5E%12.5E", filter->zero[i], 0.0);
      j += 12 * 2;
    }
    sprintf(&blk[j], "%04d", filter->npole);
    j += 4;
    for (i = 0; i < filter->npole; i++) {
      sprintf(&blk[j], "%12.5E%12.5E", filter->pole[i], 0.0);
      j += 12 * 2;
    }
  } else if (filter->type == IIR_PZ) {
    errors("%s\n","blk_054: unsupported filter: IIR_PZ");
  } else {
    sprintf(&blk[j], "%04d", filter->ncoef);
    j += 4;
    for (i = 0; i < filter->ncoef; i++) {
      sprintf(&blk[j], "%12.5E%12.5E", filter->coef[i], 0.0);
      j += 12 * 2;
    }
    sprintf(&blk[j], "%04d", 0);
    j += 4;
  }
  add_blockette(blk, 'S');
}

void blk_057(digital_filter_t p, int stage_num) {
  int j;

  j = 0;
  sprintf(&blk[j], "0570000");
  j += 7;
  sprintf(&blk[j], "%02d", stage_num);
  j += 2;
  /* MUST BE 10.4E, not 10.3E */
  sprintf(&blk[j], "%10.4E", (1.0 / p->filter->sint));
  j += 10;
  sprintf(&blk[j], "%05d", p->decim);
  j += 5;
  sprintf(&blk[j], "%05d", p->filter->offset);
  j += 5;
  sprintf(&blk[j], "%11.4E", p->filter->delay);
  j += 11;
  sprintf(&blk[j], "%11.4E", p->filter->correction);
  j += 11;
  add_blockette(blk, 'S');
}

void blk_058(double gain, double freq_gain, int stage_num) {
  int j;

  j = 0;
  sprintf(&blk[j], "0580000");
  j += 7;
  sprintf(&blk[j], "%02d", stage_num);
  j += 2;
  sprintf(&blk[j], "%12.5E", gain);
  j += 12;
  sprintf(&blk[j], "%12.5E", freq_gain);
  j += 12;
  sprintf(&blk[j], "%02d", 0);
  j += 2;
  add_blockette(blk, 'S');
}

void blk_059(comment_t comment) {
  char temp[255];

  sprintf(&blk[0], "0590000");
  if (comment->begin_time != NULL) {
    epoch2str(*(comment->begin_time), temp);
    sprintf(&blk[strlen(blk)], "%s~", temp);
  }
  if (comment->end_time == NULL || *(comment->end_time) == 0.0) {
    sprintf(&blk[strlen(blk)], "%s~", "");
  } else {
    epoch2str(*(comment->end_time), temp);
    sprintf(&blk[strlen(blk)], "%s~", temp);
  }
  sprintf(&blk[strlen(blk)], "%04d", comment->seed_key);
  sprintf(&blk[strlen(blk)], "000000");
  add_blockette(blk, 'S');
}

static void fill_blk_061(int blk_index, int ncoef, double *coef) {
  int i;
  sprintf(&blk[blk_index], "%04d", ncoef);
  blk_index += 4;
  for (i = 0; i < ncoef; i++) {
    sprintf(&blk[blk_index], "%14.7E", coef[i]);
    blk_index += 14;
  }
}

void blk_061(filter_t p, int stage_num, unit_t inp_units,
             unit_t out_units) {

  int j = 0;
  sprintf(&blk[j], "0610000");
  j += 7;
  sprintf(&blk[j], "%02d", stage_num);
  j += 2;
  sprintf(&blk[j], "%s", "FILTER_FIR~");
  j += strlen("FILTER_FIR~");

  if (p->type == FIR_SYM_1) {
     sprintf(&blk[j], "B");
  } else if (p->type == FIR_SYM_2) {
     sprintf(&blk[j], "C");
  } else if (p->type == FIR_ASYM) {
     sprintf(&blk[j], "A");
  } else {
     errors("%s\n","blk_061: unsupported filter");
  }
  j += 1;
  sprintf(&blk[j], "%03d", inp_units->lookup_code);
  j += 3;
  sprintf(&blk[j], "%03d", out_units->lookup_code);
  j += 3;

  if (p->type == FIR_SYM_1) {
    fill_blk_061(j, (p->ncoef + 1) / 2, p->coef);
  } else if (p->type == FIR_SYM_2) {
    fill_blk_061(j, p->ncoef / 2, p->coef);
  } else if (p->type == FIR_ASYM) {
    fill_blk_061(j, p->ncoef, p->coef);
  } else {
    errors("%s\n","unsupported filter");
  }
  add_blockette(blk, 'S');
}

void blk_062(filter_t p, int stage_num, unit_t inp_units,
             unit_t out_units, double channel_sint) {
  int blk_index = 0;
  int i;

  sprintf(&blk[blk_index], "0620000");
  blk_index += 7;
  sprintf(&blk[blk_index], "%s", "P");
  blk_index += strlen("P");
  sprintf(&blk[blk_index], "%02d", stage_num);
  blk_index += 2;

  sprintf(&blk[blk_index], "%03d", inp_units->lookup_code);
  blk_index += 3;
  sprintf(&blk[blk_index], "%03d", out_units->lookup_code);
  blk_index += 3;

  sprintf(&blk[blk_index], "%s", "M");
  blk_index += strlen("M");

  sprintf(&blk[blk_index], "%c", p->valid_frequency_unit);
  blk_index += strlen("B");

  sprintf(&blk[blk_index], "%12.5E", 0.0);
  blk_index += 12;

  sprintf(&blk[blk_index], "%12.5E", 1.0 / (2.0 * channel_sint));
  blk_index += 12;

  sprintf(&blk[blk_index], "%12.5E", p->lower_bound_approx);
  blk_index += 12;

  sprintf(&blk[blk_index], "%12.5E", p->upper_bound_approx);
  blk_index += 12;

  sprintf(&blk[blk_index], "%12.5E", 0.0);
  blk_index += 12;

  sprintf(&blk[blk_index], "%03d", p->ncoef);
  blk_index += 3;
  for (i = 0; i < p->ncoef; i++) {
    sprintf(&blk[blk_index], "%12.5E", p->coef[i]);
    blk_index += 12;
    sprintf(&blk[blk_index], "%12.5E", (double)0);
    blk_index += 12;
  }
  add_blockette(blk, 'S');
}

void add_blockette(char *blk, char rec_type) {
  int blen;
  char temp[10];

  void (*new_volume)(char *,int);
  void (*save_volume)(char *);

  /*=========== Get pointers according to header type ============*/

  new_volume = NULL;
  save_volume = NULL;
  if (rec_type == 'V') {
    new_volume = new_volume_logical_volume;
    save_volume = save_volume_logical_volume;
  } else if (rec_type == 'A') {
    new_volume = new_abbreviation_logical_volume;
    save_volume = save_abbreviation_logical_volume;
  } else if (rec_type == 'S') {
    new_volume = new_station_logical_volume;
    save_volume = save_station_logical_volume;
  } else {
    errors("Internal error logical record type %c is unknown\n",rec_type);
  }

  debug(DEBUG_SEED_WRITE,"Add Blockette (%c): %s\n",rec_type,blk);

  /*============  First call : initializations =================*/

  if (!strcmp(blk, "init")) {
    new_volume(logrec,0);
    nc = 8;
    return;
  }

  if (blk == NULL) {
    errors("append %c: null pointer\nblk=%p\n",rec_type, blk);
  }

  /*==== Last call :  add spaces and copy last log record into array ====*/

  if (!strcmp(blk, "end")) {
    while (nc < LRECL) {
      logrec[nc++] = ' ';
    }

    save_volume(logrec);
    return;
  }

  /*==== Append blockette to logrec and eventually copy into array ===*/

  /* Remove trailing carriage return, if any, but not trailing blanks */
  trim_cr(blk);

  /* Find blockette lenght */
  blen = strlen(blk);

  /* write blockette lenght */
  sprintf(temp, "%04d", blen);
  strncpy(&blk[3], temp, 4);

  /* If not enough room to write blk type and lenght, flush logical record */
  if ((LRECL - nc) < 7) {
    while (nc < LRECL) {
      logrec[nc++] = ' ';
    }

    new_volume(logrec,1);
    nc = 8;
  }

  /* Check if we can write the entire blockette */
  if ((LRECL - nc) >= blen) {
    strncpy(&logrec[nc], blk, blen);
    nc += blen;
  } else {
    /* else blockette will cross log record boundary */
    char *ptr = blk;
    int n_cpy = LRECL - nc;
    int b_left = blen;

    while (b_left > 0) {
      strncpy(&logrec[nc], ptr, n_cpy);
      b_left -= n_cpy;
      ptr += n_cpy;
      nc += n_cpy;

      if (nc >= LRECL) {
        new_volume(logrec,1);
        logrec[7] = '*';
        nc = 8;
      }
      if (b_left > (LRECL - 8)) {
        n_cpy = LRECL - 8;
      } else {
        n_cpy = b_left;
      }
    }
  }
}
