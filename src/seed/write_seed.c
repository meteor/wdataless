/**\file write_seed.c
 *
 */

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <sys/mtio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include "station.h"
#include "util.h"
#include "errors.h"
#include "write_seed.h"
#include "dataless.h"



/**\var VH_list_head
 *
 * \brief The head of volume header volumes list.
 */
struct lrec *VH_list_head;


/**\var VH_list_tail
 *
 * \brief The tail of volume header volumes list.
 */
struct lrec *VH_list_tail;


/**\var AH_list_head
 *
 * \brief The head of abbreviation volumes list.
 */
struct lrec *AH_list_head;

/**\var AH_list_tail
 *
 * \brief The tail of abbreviation volumes list.
 */
struct lrec *AH_list_tail;


/**\var SH_list_head
 *
 * \brief The head of station volumes list.
 */
struct lrec *SH_list_head;

/**\var SH_list_tail
 *
 * \brief The tail of station volumes list.
 */
struct lrec *SH_list_tail;


/**\var nVH
 *
 * \brief Number of elements in VH_list_tail
 */
int nVH = 0;

/**\var nAH
 *
 * \brief Number of elements in AH_list_tail
 */
int nAH = 0;

/**\var nSH
 *
 * \brief Number of elements in SH_list_tail
 */
int nSH = 0;

struct lrec {
  char rec[5000];
  struct lrec *next;
};



#define append_linklist_element(new, head, tail) \
        new->next = NULL; \
        if (head != NULL) tail->next = new; \
        else head = new; \
        tail = new;

/**\fn static void free_volume_list(struct lrec **head,struct lrec **tail,int *list_size)
 *
 * \brief Clear list content and free memory
 *
 * \param head      The head of the list
 * \param tail      The tail of the list
 * \param list_size Pointer on list size
 *
 */
static void free_volume_list(struct lrec **head, struct lrec **tail,
    int *list_size) {
  struct lrec *p;
  struct lrec *t;
  int i = 0;
  p = *head;
  while (p != NULL) {
    ++i;
    t = p->next;
    free(p);
    p = t;
  }
  *head = *tail = NULL;
  *list_size = 0;
}

int find_nVH() {
  station_t sta;
  int nsta;
  int ntheader;
  int nbytes;
  nsta = 0;
  ntheader = 0;

  for (sta = stalist_head; sta != NULL; sta = sta->next)
    ++nsta;

  nbytes = 8;
  nbytes += get_blk010_size();
  nbytes += 10 + nsta * 11;
  nbytes += 11 + ntheader * 52;
  return (nbytes / (4096 - 8 - 7) + 1);
}

void free_VH() {
  free_volume_list(&VH_list_head, &VH_list_tail, &nVH);
}

void free_AH() {
  free_volume_list(&AH_list_head, &AH_list_tail, &nAH);
}

void free_SH() {
  free_volume_list(&SH_list_head, &SH_list_tail, &nSH);
}

void save_volume_logical_volume(char *data) {
  memcpy(VH_list_tail->rec, data, 4096);
}


void new_volume_logical_volume(char *data, int save) {
  if (save) {
    save_volume_logical_volume(data);
  }
  struct lrec *pt = (struct lrec *) mem(sizeof(struct lrec));
  append_linklist_element(pt, VH_list_head, VH_list_tail);
  nVH++;
  memset(data, 0, LRECL);
  sprintf(data, "%06d%s ", nVH, "V");
}

void save_abbreviation_logical_volume(char *data) {
  memcpy(AH_list_tail->rec, data, 4096);
}

void new_abbreviation_logical_volume(char *data, int save) {
  if (save) {
    save_abbreviation_logical_volume(data);
  }
  struct lrec *pt = (struct lrec *) mem(sizeof(struct lrec));
  append_linklist_element(pt, AH_list_head, AH_list_tail);
  nAH++;
  memset(data, 0, LRECL);
  sprintf(data, "%06d%s ", nAH , "A");
}

void save_station_logical_volume(char *data) {
  memcpy(SH_list_tail->rec, data, 4096);
}

void new_station_logical_volume(char *data, int save) {
  if (save) {
    save_station_logical_volume(data);
  }
  struct lrec *pt = (struct lrec *) mem(sizeof(struct lrec));
  append_linklist_element(pt, SH_list_head, SH_list_tail);
  nSH++;
  memset(data, 0, LRECL);
  sprintf(data, "%06d%s ", nSH, "S");
}

/**\var logrec_num
 *
 * \brief The number of logical number already write.
 */
static int logrec_num = 1;

static void write_logrec(char *vol_name,FILE *seed_file, char *buf) {
  int fd;
  struct stat filestat;
  static struct mtop mtfunc; /* For writing eofs */

  fd = fileno(seed_file);
  char logrec[5000];

  memcpy(logrec,buf,LRECL);
  sprintf(logrec, "%06d%s", logrec_num, buf + 6);
  if (fwrite(logrec, 1, LRECL, seed_file) != LRECL) {
    errors("%s\n",strerror(errno));
  }
  logrec_num++;
  //stat(vol_name, &filestat);
  fstat(fileno(seed_file), &filestat);
  if (!(filestat.st_size % 32768)) {
    messages("==== %d %d\n", logrec_num, (int) filestat.st_size);
    mtfunc.mt_op = MTWEOF;
    mtfunc.mt_count = 1;
    ioctl(fd, MTIOCTOP, &mtfunc);
  } else {
    messages("==== %d %d\n", logrec_num, (int) filestat.st_size);
  }

}

static void write_logical_volume(char *vol_name,FILE *seed_file, struct lrec *head,
    struct lrec *tail, int size) {
  struct lrec *pt;
  int i;

  for (i = 0, pt = head; i < size; i++, pt = pt->next) {
    write_logrec(vol_name,seed_file, pt->rec);
  }
}

void write_control_logical_volumes(char *vol_name,FILE *seed_file, int calculated_nVH) {

  char *logrec;
  int nc;

  write_logical_volume(vol_name,seed_file, VH_list_head, VH_list_tail, nVH);

  logrec = mem(5000);


  if (calculated_nVH > nVH) {
    for (nc = 0; nc < LRECL; nc++)
      logrec[nc] = ' ';
    while (nVH++ < calculated_nVH) {
      sprintf(logrec, "%06dV ", logrec_num+1);
      write_logrec(vol_name,seed_file, logrec);
    }
  }

  free(logrec);
}

void write_abbreviation_logical_volumes(char *vol_name,FILE *seed_file) {
  write_logical_volume(vol_name,seed_file, AH_list_head, AH_list_tail, nAH);
}

void write_station_logical_volumes(char *vol_name,FILE *seed_file) {
  write_logical_volume(vol_name,seed_file, SH_list_head, SH_list_tail, nSH);
}

void close_dataless_file(char *vol_name,FILE *seed_file) {
  struct stat filestat;
  char logrec[5000];
  int i;

  sprintf(logrec, "%06dV ", 0);
  for (i = 8; i < LRECL; i++)
    logrec[i] = ' ';
  logrec[LRECL] = 0;

//  while (((logrec_num - 1) % 8))
//    write_logrec(vol_name,seed_file, logrec);

  fflush(seed_file);
  //stat(vol_name, &filestat);
  fstat(fileno(seed_file), &filestat);

//  if ((filestat.st_size % PRECL) != 0) {
//    warnings("volume has a wrong size (size = %d)\n", (int) filestat.st_size);
//  }
  messages(" %3d  Volume  Header Records\n", get_nVH());
  messages(" %3d  Abbrev  Header Records\n", nAH);
  messages(" %3d  Station Header Records\n", nSH);

  if (fclose(seed_file) == EOF) {
    errors("fclose : %s\n", strerror(errno));
  }
  if (stat(vol_name, &filestat) != 0) {
    warnings("can't stat %s : %s\n", vol_name, strerror(errno));
  }
  free_VH();
  free_AH();
  free_SH();
}
