%define api.prefix {dbird}
//%name-prefix "dbird"
 %{
    int dbirderror(char *);
    #include <stdio.h>
    #include <errno.h>
    #include "util.h"
    #include "author.h"
    #include "network.h"
    #include "comment.h"
    #include "phone_number.h"
    #include "station.h"
    #include "uncertainty_double.h"
    #include "location.h"
    #include "sensor.h"
    #include "analog_filter.h"
    #include "digitizer.h"
    #include "digital_filter.h"
    #include "errors.h"
    #include "owner.h"
    #include "list.h"
    #include "dbirdparser.h"
    #include "dbirdlex.h"

#ifndef YY_BUF_SIZE
#ifdef __ia64__
/* On IA-64, the buffer size is 16k, not 8k.
 * Moreover, YY_BUF_SIZE is 2*YY_READ_BUF_SIZE in the general case.
 * Ditto for the __ia64__ case accordingly.
 */
#define YY_BUF_SIZE 32768
#else
#define YY_BUF_SIZE 16384
#endif /* __ia64__ */
#endif

    static char      *current_filename;
    static station_t  resulting_list;
    static list_t     author_list;
    static int        comment_lookup_key = 1;
    extern int        line_number;

    //	extern int yydebug;
 %}



 %token BEGIN_LIST
 %token END_LIST
 %token OPEN_BRACKETS
 %token CLOSE_BRACKETS
 %token COMMA
 %token VERSION
 %token ORIGINATING_ORGANIZATION
 %token BEGIN_AUTHOR END_AUTHOR
 %token BEGIN_COMMENT END_COMMENT
 %token BEGIN_NETWORK END_NETWORK
 %token CONTACT
 %token OWNER
 %token LOCATION
 %token BEGIN_STATION END_STATION
 %token ALTERNATE_CODE
 %token HISTORICAL_CODE
 %token BEGIN_LOCATION END_LOCATION
 %token BEGIN_SENSOR END_SENSOR
 %token BEGIN_ANA_FILTER END_ANA_FILTER
 %token BEGIN_DIGITIZER END_DIGITIZER
 %token BEGIN_DECIMATION END_DECIMATION
 %token BEGIN_CHANNEL END_CHANNEL
 %token NONE


%union {
 	int                   int_value;
 	double                double_value;
 	double               *double_ref;
 	char                 *string;
 	list_t	              list;
  author_t              author;
  comment_t             comment;
  phone_number_t        phone_number;
 	station_t             station;
 	uncertainty_double_t  uncertainty_double;
 	location_t            location;
 	sensor_t              sensor;
 	analog_filter_t       analog_filter;
 	digitizer_t           digitizer;
 	filter_cascade_t      cascade;
 	channel_t             channel;
 	station_t            *stations_list;
 }

 %token <int_value>       INTEGER
 %token <double_value>    DOUBLE
 %token <string>          DATE
 %token <string>          STRING
 %token <string>          WORD

 %type  <int_value>       			version
 %type  <author>                author
 %type  <list>                  author_serie
 %type  <list>                  author_decl
 %type  <list>                  comment_decl
 %type  <list>                  comment_list
 %type  <comment>               comment
 %type  <list>                  phone_list
 %type  <list>                  phone_serie
 %type  <phone_number>          phone_number
 %type  <station>         			stations_list
 %type  <station>          			station
 %type  <double_ref>            optional_float
 %type  <uncertainty_double>    uncertainty_double
 %type  <location>              station_infos
 %type  <string>                owner
 %type  <list>                  location_decl
 %type  <list>                  location_list
 %type  <location>        			location
 %type  <list>            			sensor_decl
 %type  <list>            			sensor_list
 %type  <sensor>          			sensor
 %type  <list>            			ana_filter_decl
 %type  <list>            			ana_filter_list
 %type  <analog_filter>   			ana_filter
 %type  <list>            			digitizer_decl
 %type  <list>            			digitizer_list
 %type  <digitizer>       			digitizer
 %type  <list>            			decimation_decl
 %type  <list>            			decimation_list
 %type  <cascade>         			decimation
 %type  <list>            			channel_decl
 %type  <list>            			channel_list
 %type  <channel>         			channel
 %type  <list>            			word_list
 %type  <list>            			word_serie
 %type  <list>                  string_list
 %type  <list>                  string_serie

 %%
 dbird : version origin author_decl network_decl stations_list
 {
 	resulting_list = $5;
 }
         | version origin network_decl stations_list
 {
  resulting_list = $4;
 }
 ;

 version : VERSION INTEGER
 {
   $$ = $2;
 }
 ;

 origin : ORIGINATING_ORGANIZATION STRING STRING STRING STRING
 {
 	set_seed_organization($2);
 }
 ;


////////////////////////////
//
// AUTHOR
//
////////////////////////////

 author_decl : BEGIN_AUTHOR author_serie END_AUTHOR
 {
   author_list = $2;
 }
             | BEGIN_AUTHOR END_AUTHOR
 {
  author_list = NULL;
 }
 ;

 author_serie : author
  {
    $$ = append_element($1, NULL);
  }
              | author_serie author
  {
    $$ = append_element($2, $1);
  }
  ;

//      dbird_code     name       agency      mail        phone
author:   WORD      string_list string_list string_list phone_list
{
  author_t result = create_author($1, $2, $3, $4, $5);
  $$ = result;
}
;

////////////////////////////
//
// Network comment
//
////////////////////////////
network_comment_decl : BEGIN_COMMENT network_comment_list END_COMMENT
                     | BEGIN_COMMENT END_COMMENT
;

network_comment_list: network_comment
                    | network_comment_list network_comment
;

//               dbird_code    comment  start    end   author
network_comment:   WORD        STRING    DATE   DATE  word_list
;

////////////////////////////
//
// Network
//
////////////////////////////
 network_decl : BEGIN_NETWORK network_comment_decl network_list END_NETWORK
              | BEGIN_NETWORK network_list END_NETWORK

;

 network_list : network
              | network_list network
 ;

 //       dbird_code  fdsn_code description  contact_email      open   close   altcode   comments
 network:  	WORD       STRING    STRING       STRING        DATE    DATE    STRING   word_list
 {
 	create_network($1, $2, $3, $5, $6, $7);
 }
 ;

 ////////////////////////////
 //
 // Station/Channel comment
 //
 ////////////////////////////
 comment_decl : BEGIN_COMMENT comment_list END_COMMENT
 {
   $$ = $2;
 }            | BEGIN_COMMENT END_COMMENT
 {
   $$ = NULL;
 }            |
 {
   $$ = NULL;
 }
 ;

 comment_list: comment
 {
   $$ = append_element($1, NULL);;
 }
             | comment_list comment
 {
   $$ = append_element($2, $1);
 }
 ;

 //       dbird_code    comment  start    end   author     type(C|S)
 comment:   WORD        STRING    DATE   DATE  word_list     WORD
 {
   list_t current_author_label = $5;
   list_t comment_author_list = NULL;

   while(current_author_label != NULL) {
     author_t author = find_elt(author_list, (int(*)(void *, void *))author_label_comp, current_author_label->element);
     if (author == NULL) {
       errors("Can't find author with label %s\n", (char *)current_author_label->element);
     }
     append_element(author, comment_author_list);
     current_author_label = current_author_label->next;
   }

   if (!strcmp($6, "S")) {
     $$ = create_comment($1, $2, $3, $4, comment_author_list, comment_lookup_key++, Station);
   } else if (!strcmp($6, "C")) {
     $$ = create_comment($1, $2, $3, $4, comment_author_list, comment_lookup_key++, Channel);
   } else {
     errors("The comment type <%s> is unknown (must be S or C)\n" , $6);
   }
}
 ;


 ////////////////////////////
 //
 // Station
 //
 ////////////////////////////
 stations_list : station
 {
 	$$ = $1;
 }
               | stations_list station
 {
 	station_t current = $1;
 	while(current->next != NULL)
 		current = current->next;
 	current->next = $2;
 }
 ;

 station: BEGIN_STATION STRING DATE DATE station_infos contact owner historical_code alternate_code comment_decl location_decl sensor_decl ana_filter_decl digitizer_decl decimation_decl channel_decl END_STATION
 {
   station_t result = create_station($2, $3, $4);

   /* Set the owner of the station */
   result->owner = (char *) mem(strlen($7) + 1);
   sprintf(result->owner, "%s", $7);
   add_owner_name(result->owner);


   /* Set comment list */
   comment_t duplicate_comment = find_same_elt($10, (int (*)(void *, void *))comp_comment);
   if (duplicate_comment) {
     errors("The comment label %s appear multiples time for %s\n" , duplicate_comment->label, $2);
   }
   result->comment = $10;

   /* Set location code list */
   location_t duplicate_location = find_same_elt($11, (int (*)(void *, void *))comp_location );
   if (duplicate_location) {
     errors("The location label %s appear multiples time for %s\n" , duplicate_location->label, $2);
   }

   result->location = (location_t *) mem(sizeof(location_t) * (get_list_size($11) + 1));

   result->nlocation = 0;
   result->location[result->nlocation++] = $5;
   list_t current = $11;
   while(current != NULL) {
     result->location[result->nlocation++] = current->element;
     current = current->next;
   }


   /* Set sensor list */
   sensor_t duplicate_sensor = find_same_elt($12, (int (*)(void *, void *))comp_sensor );
   if (duplicate_sensor) {
     errors("The sensor label %s appear multiples time for %s\n" , duplicate_sensor->label, $2);
   }

   result->sensor =  (sensor_t *) mem(sizeof(sensor_t) * get_list_size($12));

   result->nsensor = 0;
   current = $12;
   while(current != NULL) {
     result->sensor[result->nsensor++] = current->element;
     debug(DEBUG_DBIRD_PARSER,"SENSOR label = %s\n",((sensor_t)current->element)->label);
     current = current->next;
   }

   /* Set analog filter */
   analog_filter_t duplicate_afilter = find_same_elt($13, (int (*)(void *, void *))comp_analog_filter);
   if (duplicate_afilter) {
     errors("The analog filter label %s appear multiples time for %s\n" , duplicate_afilter->label, $2);
   }

   result->analog_filter = (analog_filter_t *) mem(sizeof(analog_filter_t) * get_list_size($13));

   result->nanalog_filter = 0;
   current = $13;
   while(current != NULL) {
     result->analog_filter[result->nanalog_filter++] = current->element;
     current = current->next;
   }

   /* Set digitizer list*/
   digitizer_t duplicate_digitizer = find_same_elt($14, (int (*)(void *, void *))comp_digitizer);
   if (duplicate_digitizer) {
     errors("The digitizer label %s appear multiples time for %s\n" , duplicate_digitizer->label, $2);
   }

   result->digitizer = (digitizer_t *) mem(sizeof(digitizer_t) * get_list_size($14));

   result->ndigitizer = 0;
   current = $14;
   while(current != NULL) {
     result->digitizer[result->ndigitizer++] = current->element;
     debug(DEBUG_DBIRD_PARSER,"ADC label = %s\n",((digitizer_t)current->element)->label);
     current = current->next;
   }


   /* Set digital filter */
   filter_cascade_t duplicate_cascade = find_same_elt($15, (int (*)(void *, void *))comp_cascade);
   if (duplicate_cascade) {
     errors("The digital filter label %s appear multiples time for %s\n" , duplicate_cascade->label, $2);
   }

   result->cascade = (filter_cascade_t *) mem(sizeof(filter_cascade_t) * get_list_size($15));

   result->ncascade = 0;
   current = $15;
   while(current != NULL) {
     if(!is_cascade(current->element)) {
		errors("Pointer %p is not cascade\n",current->element);
     }
     debug(DEBUG_DBIRD_PARSER,"Digital filter label = %s\n",((filter_cascade_t)current->element)->label);
     result->cascade[result->ncascade++] = current->element;
     current = current->next;
   }

   /* Set channel definition */
   result->channel = (channel_t *) mem(sizeof(channel_t) * get_list_size($16));

   result->nchannel = 0;
   current = $16;
   while(current != NULL) {
     debug(DEBUG_DBIRD_PARSER,"Storing channel %d\n",result->nchannel);
	   result->channel[result->nchannel++] = current->element;
     /* Get comment from label */
     list_t current_label = ((channel_t)current->element)->comment_label;
     while(current_label != NULL) {
      comment_t current_comment = find_elt(result->comment, (int (*)(void *, void *))have_label, current_label->element);
      if (current_comment == NULL) {
        errors("Can't find comment with label %s\n", current_label->element);
      }
      if (current_comment->type != Channel) {
        errors("The station comment %s is used as channel comment in %s\n", current_label->element, result->name);
      }
      /* Verify that channel and comment period overlap */
      if((current_comment->begin_time != NULL && *(current_comment->begin_time) > ((channel_t)current->element)->end_utime && ((channel_t)current->element)->end_utime != 0) ||
         (current_comment->end_time != NULL && *(current_comment->end_time) < ((channel_t)current->element)->beg_utime)) {
          errors("Period of comment %s don't overlap with channel period in %s\n", current_label->element, result->name);
      }
      ((channel_t)current->element)->comment = append_element(current_comment,
                                                              ((channel_t)current->element)->comment);
      current_label = current_label->next;
     }
     current = current->next;
   }

   /* Fill and verify channel definition */
   result = fill_and_verify_channel(current_filename,result);

   /* The station is initialized */
   $$ = result;
 }
 ;

////////////////////////////
//
// Uncertainty double
//
////////////////////////////
optional_float: DOUBLE
{
  double *result = mem(sizeof(double));
  *result = $1;
  $$ = result;
}
               | NONE
{
  $$ = NULL;
}
;

 uncertainty_double: OPEN_BRACKETS DOUBLE COMMA optional_float COMMA optional_float CLOSE_BRACKETS
 {
   $$ = new_uncertaimty_double($2, $4, $6);
 }
 ;


 ////////////////////////////
 //
 //  Station infos
 //
 ////////////////////////////
 //                LAT              LON                  ALT               TOPO
 station_infos : uncertainty_double uncertainty_double uncertainty_double STRING
 {
   $$ = create_location(STALOCID,STALOCID,$1,$2,$3,0,$4);
 }
 ;

 contact: CONTACT STRING
 ;

 owner : OWNER STRING
 {
   $$ = $2;
 }
 ;
 ////////////////////////////
 //
 // Alternate and historical code
 //
 ////////////////////////////

 historical_code: HISTORICAL_CODE STRING
                |
 ;
 alternate_code: ALTERNATE_CODE STRING
               |
 ;
 ////////////////////////////
 //
 // Location
 //
 ////////////////////////////
 location_decl : BEGIN_LOCATION  location_list END_LOCATION
 {
   $$ = $2;
 }
 ;


 location_list : location
 {
 	$$ = append_element($1,NULL);
 }
 		   | location_list location
 {
	$$ = append_element($2,$1);
 }

 //         Label Location code  Latitude           Longitude            Elevation        Depth     vault    geology
 location : WORD     STRING   uncertainty_double uncertainty_double uncertainty_double   DOUBLE     STRING    STRING
 {
 	$$ = create_location($1,$2,$3,$4,$5,$6,"");
 }
 ;

 ////////////////////////////
 //
 // Sensor
 //
 ////////////////////////////
 sensor_decl: BEGIN_SENSOR sensor_list END_SENSOR
 {
   $$ = $2;
 }
 ;

 sensor_list : sensor
 {
 	$$ = append_element($1,NULL);
 }
| sensor_list sensor
 {
	$$ = append_element($2,$1);
 }
 ;
 //      Label  Type  Serial number  pz file  azimuth   dip
 sensor: WORD  STRING     STRING     STRING   DOUBLE  DOUBLE
 {
 	$$ = create_sensor($1,$2,$3,$4,$5,$6);
 }
 ;

 ////////////////////////////
 //
 // Analog filter
 //
 ////////////////////////////
 ana_filter_decl:
 {
 	$$ = NULL;
 }
 | BEGIN_ANA_FILTER ana_filter_list END_ANA_FILTER
 {
 	$$ = $2;
 }

 ana_filter_list: ana_filter
 {
 	$$ = append_element($1,NULL);
 }

 | ana_filter_list ana_filter
 {
	$$ = append_element($2,$1);
 }
 ;

 ana_filter: WORD STRING STRING STRING
 {
 	$$ = create_analog_filter($1,$4);
 }

 ////////////////////////////
 //
 // Digitizer
 //
 ////////////////////////////
digitizer_decl:
 {
   $$ = NULL;
 }
| BEGIN_DIGITIZER digitizer_list END_DIGITIZER
 {
   $$ = $2;
 }
 ;

 digitizer_list: digitizer
 {
   check_is_adc($1);
   $$ = append_element($1,NULL);
 }
| digitizer_list digitizer
 {
   check_is_adc($2);
   $$ = append_element($2,$1);
 }
 ;

 digitizer : WORD STRING STRING STRING
 {
   $$ = create_adc($1,$4);
 }
 ;

 ////////////////////////////
 //
 // Decimation
 //
 ////////////////////////////
 decimation_decl:
 {
 	$$ = NULL;
 }
 | BEGIN_DECIMATION decimation_list END_DECIMATION
 {
 	$$ = $2;
 }

 decimation_list : decimation
 {
 	$$ = append_element($1,NULL);
 }
 | decimation_list decimation
 {
 	$$ = append_element($2,$1);
 }
 ;

 decimation:  WORD STRING STRING STRING
 {
 	$$ = create_digital_filter_cascade($1,$4);
 }
 ;

 ////////////////////////////
 //
 // Channel
 //
 ////////////////////////////
 channel_decl: BEGIN_CHANNEL channel_list  END_CHANNEL
 {
 	$$ = $2;
 }
 ;

 channel_list: channel
 {
 	$$ = append_element($1,NULL);
 }
| channel_list channel
 {
	$$ = append_element($2,$1);
 }
;
 //       label channel sensor ana filter   das   dig filter flag encoding  start  end   net    comment
 channel:  WORD  WORD    WORD   word_list   WORD  word_list  WORD   WORD    DATE   DATE  WORD  word_list
 {
 	$$ = create_channel($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12);
 }
 ;


////////////////////////////
//
// Phone list
//
////////////////////////////
phone_list : BEGIN_LIST phone_serie END_LIST
{
  $$ = $2;
}          | BEGIN_LIST END_LIST
{
  $$ = NULL;
}
;

phone_serie: phone_number
{
 $$ = append_element($1,NULL);
}
 	   | phone_serie COMMA phone_number
{
 $$ = append_element($3,$1);
}
;

phone_number: INTEGER INTEGER STRING
{
  int *country_code = mem(sizeof(int));
  *country_code = $1;
  $$ = create_phone_number(country_code, $2, $3);
}
            | INTEGER STRING
{
  $$ = create_phone_number(NULL, $1, $2);
}

 ////////////////////////////
 //
 // String list
 //
 ////////////////////////////
 string_list : BEGIN_LIST string_serie END_LIST
 {
    $$ = $2;
 }
           | BEGIN_LIST END_LIST
{
    $$ = NULL;
}
;
string_serie: STRING
{
 $$ = append_element($1,NULL);
}
 	   | string_serie COMMA STRING
{
 $$ = append_element($3,$1);
}
;

////////////////////////////
//
// Word list
//
////////////////////////////
 word_list : BEGIN_LIST word_serie END_LIST
 {
    $$ = $2;
 }
           | BEGIN_LIST END_LIST
{
    $$ = NULL;
}
;
 word_serie: WORD
 {
 	$$ = append_element($1,NULL);
 }
 	   | word_serie COMMA WORD
 {
	$$ = append_element($3,$1);
 }
;
 %%



void parse_dbird_file(char *filename,station_t *result) {
	YY_BUFFER_STATE  buffer;
	FILE            *dbird_file;

	current_filename = filename;
	debug(DEBUG_DBIRD_PARSER,"Begin parse %s\n",filename);
	// Open dbird file
	if((dbird_file = fopen(filename,"r")) == NULL) {
		errors("%s when try to open %s\n", strerror(errno), filename);
	}


	//	yydebug=1;
	buffer = dbird_create_buffer(dbird_file, YY_BUF_SIZE);
	dbird_switch_to_buffer(buffer);
	dbirdparse();
	*result = resulting_list;
}


int dbirderror(char *errmsg) {
  errors("%s:%d : %s when reading '%s'\n",current_filename,line_number,errmsg,dbirdtext);
  exit(1);
}
