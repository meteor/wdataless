%{

#include <stdlib.h>
#include "station.h"
#include "location.h"
#include "sensor.h"
#include "util.h"
#include "list.h"
#include "author.h"
#include "comment.h"
#include "phone_number.h"
#include "dbirdparser.h"
#include "errors.h"
#define YY_NO_INPUT
int line_number = 0;
%}
%option noyywrap
%option case-insensitive
%option prefix="dbird"
%option outfile="dbirdlex.c"
%option nounput
%%

"\""[^"]*"\"" {  dbirdlval.string = strdup(yytext+1) ; *(dbirdlval.string+strlen(dbirdlval.string)-1)='\0'; debug(DEBUG_DBIRD_PARSER,"STRING:%s\n",dbirdlval.string); return STRING ;}

"#".* /* ignore comments */

"[" {debug(DEBUG_DBIRD_PARSER,"%s\n","[");return BEGIN_LIST;}
"]" {debug(DEBUG_DBIRD_PARSER,"%s\n","]");return END_LIST;}
"(" {debug(DEBUG_DBIRD_PARSER,"%s\n","]");return OPEN_BRACKETS;}
")" {debug(DEBUG_DBIRD_PARSER,"%s\n","]");return CLOSE_BRACKETS;}
"," {debug(DEBUG_DBIRD_PARSER,"%s\n",","); return COMMA;}
"VERSION" {return VERSION;}
"ORIGINATING_ORGANIZATION" {return ORIGINATING_ORGANIZATION;}
"CONTACT" {return CONTACT;}
"OWNER" { return OWNER; }
"LOCATION" {return LOCATION;}
"BEGIN_AUTHOR" {return BEGIN_AUTHOR;}
"END_AUTHOR" {return END_AUTHOR;}
"HISTORICAL_CODE" {return HISTORICAL_CODE;}
"ALTERNATE_CODE" {return ALTERNATE_CODE;}
"BEGIN_COMMENT" {return BEGIN_COMMENT;}
"END_COMMENT" {return END_COMMENT;}
"BEGIN_NETWORK" {return BEGIN_NETWORK;}
"END_NETWORK" {return END_NETWORK;}
"BEGIN_STATION" {return BEGIN_STATION;}
"END_STATION" {return END_STATION;}
"BEGIN_LOCATION" {return BEGIN_LOCATION;}
"END_LOCATION" {return END_LOCATION;}
"BEGIN_SENSOR" {return BEGIN_SENSOR;}
"END_SENSOR" {return END_SENSOR;}
"BEGIN_ANA_FILTER" {return BEGIN_ANA_FILTER;}
"END_ANA_FILTER" {return END_ANA_FILTER;}
"BEGIN_DIGITIZER" {return BEGIN_DIGITIZER;}
"END_DIGITIZER" {return END_DIGITIZER;}
"BEGIN_DECIMATION" {return BEGIN_DECIMATION;}
"END_DECIMATION" {return END_DECIMATION;}
"BEGIN_CHANNEL" {return BEGIN_CHANNEL;}
"END_CHANNEL" {return END_CHANNEL;}
"NONE" {return NONE;}

[0-9]+  { debug(DEBUG_DBIRD_PARSER,"INTEGER:%d\n",atoi(yytext));dbirdlval.int_value = atoi(yytext) ; return INTEGER ;}
[-+]?([0-9]*\.?[0-9]+|[0-9]+\.)([eE]([+-])?[0-9]+)? {debug(DEBUG_DBIRD_PARSER,"DOUBLE:%e\n",atof(yytext));dbirdlval.double_value = atof(yytext) ; return DOUBLE;}
([0-9]{4}"-"[0-9]{2}"-"[0-9]{2}"T"[0-9]{2}":"[0-9]{2}":"[0-9]{2}Z)|("present") { debug(DEBUG_DBIRD_PARSER,"DATE:%s\n",yytext);  dbirdlval.string = strdup(yytext); return DATE;}
[A-Za-z_][A-Za-z0-9_]* {debug(DEBUG_DBIRD_PARSER,"WORD:%s\n",yytext);dbirdlval.string = strdup(yytext) ; return WORD;}
[:space:] /* skip space */
"\n" {line_number++;}
. /* skip not match c*/
%%
