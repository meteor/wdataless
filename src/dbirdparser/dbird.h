/**\file dbird.h
 *
 * Header file for functions present in dbird.y
 */

#ifndef DBIRD_H_
#define DBIRD_H_

/**\fn void parse_dbird_file(char *filename,station_t *result)
  *
  * \brief Parse a dbird file and store read station in given station array.
  *
  * \param filename The name of dbird file
  * \param result   The head of station linked list
  *
  */
void parse_dbird_file(char *filename,station_t *result);

#endif /* DBIRD_H_ */
