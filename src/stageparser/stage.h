/*
 * stage.h
 *
 *  Created on: 9 sept. 2011
 *      Author: leon
 */

#ifndef STAGE_H_
#define STAGE_H_

/**\fn int parse_stage_file(char *filename,void *object)
 *
 * Parse a stage file and store read data in a given object.
 *
 * \param filename The name of the stage file
 * \param object   Pointer to object
 *
 */
void parse_stage_file(char *filename,void *object);

#endif /* STAGE_H_ */
