RDSEED    = /home/leon/rdseedv5.0/rdseed 


all:wdataless

wdataless: 
	$(MAKE) -C src

verify: wdataless
	@./tests/verify.sh wdataless $(RDSEED)

.doc:
	@doxygen

clean:
	$(MAKE) -C src clean

