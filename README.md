[TOC]

# 1 - Presentation

- La chaîne de fabrication des métadonnées à l'OCA est constituée de 4 éléments :

  - **Le fichier dbird**
    Il décrit les chaînes d'acquisition et le matériel la composant pour un ensemble de réseaux, un ensemble de stations, un ensemble de location codes et un ensemble de canaux sur une période de temps donnée, dans le but de fabriquer les métadonnées correspondantes aux formats Seed dataless (avec wdataless) et stationXML (avec hermes).
    Le format stationXML étant plus riche que le format Seed dataless, le fichier dbird contient des informations pouvant ne pas être nécessaires à la génération des métadonnées au format Seed dataless.

  - **La banque de réponses instrumentales $PZ**
    Elle contient, répartis dans 5 sous-répertoires, les fichiers décrivant les éléments constitutifs
    des réponses intrumentales :

    * *$PZ/sensor*: les unités d'entrée et de sortie, les sensibilités, les types des réponses, les pôles et zéros ou coefficients polynomiaux des réponses des capteurs 

    * *$PZ/ana_filter*: les sensibilités et éventuels p$ôles et zéros des filtres analogiques, qu'ils soient intégrés aux unités d'acquisition ou indépendants 

    * *$PZ/digitizer*: les facteurs de conversion et fréquences de sortie des convertisseurs analogiques-digitaux 
    * *$PZ/dig_filter*: les étages des filtres de décimation avec les coefficients numériques 
    *  *$PZ/software_filter*: les étages des filtres de décimation des éléments informatiques hors de la chaîne d'acquisition proprement dite

  - **Le fichier annexes *$PZ/sensor_list*** 

    Décrit l'ensemble des capteurs référencés dans les fichiers dbird.

  - **Le fichier annexes *$PZ/unit_list*** 

    Décrit  l'ensemble des unités utilisées dans les fichiers de $PZ

  

  Le mode de création du fichier dbird n'a pas d'importance. Pour permettre une génération automatique les fichiers de $PZ doivent suivrent la nomenclature décrite ci-dessous

  .
  

# 2 - Structure du fichier Dbird

  

  ```
  version version_num
  originating_organization orga_name orga_mail orga_URL orga_phone
  
  begin_author
     auth_id [auth_names] [agency list] [phone list][mail list]
     [...]
  end_author
  
  begin_network
     begin_comment
        net_com_id comment_text com_debut com_fin [auth_id list]
        [...]
     end_comment
     net_id net_code net_name net_mail net_debut net_fin net_alternate [net_com_id list]
  end_network
  
  begin_station
     sta_code sta_debut sta_fin (sta_lat, sta_lat-lat_err, sta_lat+lat_err) (sta_long, sta_long-long_err, sta_long+long_err) (sta_alti, sta_alti-alti_err, sta_alti+alti_err) sta_toponyme
     Contact contact_mail
     Owner owner_name
  
     begin_comment
        com_id com_text com_debut com_fin [auth_id list] com_type
        [...]
     end_comment
  
     begin_location
        loc_id loc_code (loc_lat, loc_lat-lat_err, loc_lat+lat_err) (loc_long, loc_long-long_err, loc_long+long_err) (loc_alti, loc_alti-alti_err, loc_alti+alti_err) loc_prof loc_bâti loc_géol
     end_location
  
     begin_sensor
        sens_id sens_modèle sens_SN sens_fichier sens_azi sens_dip
        [...]
     end_sensor
  
     begin_ana_filter
        ana_id ana_modèle ana_SN ana_fichier
        [...]
     end_ana_filter
  
     begin_digitizer
        das_id das_modèle das_SN das_fichier
        [...]
     end_digitizer
  
     begin_decimation
        dec_id dec_modèle dec_SN dec_fichier
        [...]
     end_decimation
  
     begin_channel
        loc_id chan_code sens_id [ana_id list] das_id [dec_id list] data_type data_format chan_début chan_fin net_id [com_id list]
        [...]
     end_channel
  end_station
  ```

  Pour chaque fichier dbird :

  - 2 lignes d'entête (version et originating_organization),

  - 1 section begin_author / end_author (peut être vide),
    pour chaque auteur de commentaire :

    * label de l'auteur
    * Liste des noms associés 
    * Liste des agences associées à ce label
    * Liste des coordonnées téléphoniques de l'auteur, 
    * Liste des adresses électroniques de l'auteur

  - 1 section begin_network / end_network,
    pour chaque réseau :

    * 1 section begin_comment / end_comment (peut être vide)

       pour chaque commentaire réseau :
        label du commentaire, texte du commentaire, dates de début et de fin de validité du commentaire, liste des labels des auteurs du commentaire

    * label du réseau, code FDSN du réseau, intitulé du réseau, adresse mail du réseau, dates de début et de fin du réseau, code alternatif du réseau, liste des labels des commentaires du réseau

  - N sections begin_station / end_station,

  Pour chaque section begin_station / end_station :

  - 1 ligne de description,
    code de la station, dates de début et de fin de la station, coordonnées géographiques et altitude de la station avec incertitude, toponyme de la station

  - 1 ligne Contact,
  - 1 ligne Owner,

  - 1 section begin_comment / end_comment (peut être vide),
    pour chaque commentaire station ou channel :
    label du commentaire, texte du commentaire, dates de début et de fin de validité du commentaire, liste des labels des auteurs du commentaire, type du commentaire (S ou C)

  - 1 section begin_location / end_location,
    pour chaque location code :
    label du location code, valeur du location code, coordonnées géographiques et altitude du point avec incertitude, profondeur, code du bâti, code de la géologie

  - 1 section begin_sensor / end_sensor,
    pour chaque composante de capteur :
    label de la composante, modèle du capteur, S/N du capteur, nom du fichier contenant la réponse, azimut et dip de la composante sur le terrain

  - 1 section begin_ana_filter / end_ana_filter optionnelle,
    pour chaque composante de filtre analogique :
    label de la composante, modèle du filtre analogique, S/N du filtre analogique, nom du fichier contenant la réponse

  - 1 section begin_digitizer / end_digitizer,
    pour chaque entrée de l'unité d'acquisition :
    label de l'entrée de la DAS, modèle de la DAS, S/N de la DAS, nom du fichier contenant la réponse

  - 1 section begin_decimation / end_decimation optionnelle,
    pour chaque cascade de décimation :
    label de la cascade de décimation, modèle de la DAS ou du PC embarqué, S/N de la DAS ou du PC embarqué, nom du fichier contenant la réponse

  - 1 section begin_channel / end_channel.
    pour chaque canal, assemblage de ses éléments via leur label référencés au-dessus, enrichi de quelques informations supplémentaires :
    label du location code, code IRIS du canal, label de la composante du capteur, liste des labels des filtres analogiques, label de l'entrée de la DAS, liste des labels des cascades de décimation, type des données (selon Seed), encodage des données, date de début et de fin du canal, label du réseau d'appartenance, liste des labels de commentaires

  Les indentations et les lignes vides ne sont pas obligatoires, elles aident à la lecture.



**Mots-clés**: version, originating_organization, Contact, Owner, begin_author, begin_network, begin_station, begin_comment, begin_sensor, begin_ana_filter, begin_digitizer, begin_decimation, begin_channel, end_author,   end_network,   end_station,   end_comment,   end_sensor,   end_ana_filter,   end_digitizer,   end_decimation,   end_channel

**Labels**: net_id, loc_id, sens_id, ana_id, das_id, dec_id

**Chaines de caractères (encadrées par des doubles cotes)**: orga_name, orga_mail, orga_URL, auth_name, auth_tel, auth_mail, net_com_text, net_code, net_name, net_mail, net_alternate, sta_code, sta_toponyme, contact_mail, owner_name, com_text, loc_code, loc_bâti, loc_géol sens_modèle, sens_SN, sens_fichier, ana_modèle, ana_SN, ana_fichier, das_modèle, das_SN, das_fichier, dec_modèle, dec_SN, dec_fichier

**Valeurs numériques** (avec erreur) : sta_lat, sta_long, sta_alti, loc_lat, loc_long, loc_alti

**Valeurs numériques** : version_num, loc_prof, sens_azi, sens_dip

**Date** : sta_debut, sta_fin, chan_debut, chan_fin (Format: YYYY-MM-DDThh:mm:ssZ ou present)

  # 3 - Nomenclature des noms de fichiers

  Format des noms des fichiers de $PZ : de 3 à 7 champs délimités par un #.

  - sens_fichier : sensor/constructeur#modèle#paramétrisation#sn#composante#début#fin ou sensor/constructeur#modèle#paramétrisation#theoretical#composante
  - ana_fichier  : ana_filter/constructeur#modèle#anafilter_name#sn#composante#début#fin ou ana_filter/constructeur#modèle#anafilter_name#theoretical#composante
  - das_fichier  : digitizer/constructeur#modèle#paramétrisation#sn#connecteur-composante#début-fin ou digitizer/constructeur#modèle#paramétrisation#théoretical#connecteur-composante
  - dec_fichier  : dig_filter/constructeur#modèle#decimation_name

  

  On notera que le respect de la nomenclature n'est pas obligatoire pour le bon fonctionnement de wdataless. 

  # 4 - La banque de données PZ

  Nous allons maintenant décrire le format des fichiers de la banque de données PZ. 

**Régles communes**

  * Les mots-clés ne sont pas case sensitive.
  * L'emploi des dièses (#) pour commenter des lignes entières ou des portions

  ​       de lignes, à titre d'information par exemple, est possible.

  4.1 - sensor
  ----------

  pour des réponses de type "pôles et zéros" :

  ```
  begin sensor
    Response_name : "SENSOR RESPONSE"
    Response_type : "THEORETICAL" # optionel
    Transfer_function_type :	"LAPLACE" / "ANALOG" / "POLYNOMIAL"
    Input_unit :		   # parmi les unités listées dans $PZ/unit_list
    Output_unit :		V  # En général des Volt
    Sensitivity : 1500
    Transfer_normalization_frequency : 1.
    include  "sensor/include/streckeisen#sts-2.v2-120.pz.rad"
  end sensor
  ```

  ou

  ```
  begin sensor
    Response_name : "SENSOR RESPONSE"
    Response_type : "THEORETICAL" # optionel
    Transfer_function_type : "LAPLACE"/"ANALOG"/"POLYNOMIAL"
    Input_unit :			 # parmi les unités listées dans $PZ/unit_list
    Output_unit :		  V  # En général des Volt
    Sensitivity : 1.0e-03
    Transfer_normalization_frequency : 1.
    Number_of_zeroes    : 0			# ces 2 lignes en lieu et place de la référence
    Number_of_poles     : 0			# au fichier du répertoire include
  end sensor
  ```

  pour des réponses de type polynomial :

  ```
  begin sensor
    Response_name : "SENSOR RESPONSE"
    Response_type : "THEORETICAL" # optionel
    Transfer_function_type : "POLYNOMIAL"
    Input_unit :					parmi les unités listées dans $PZ/unit_list
    Output_unit :	  V  # En général des Volt
    Valid_Frequency_Unit : "Hz"
    Lower_Bound_Approximation : 0.
    Upper_Bound_Approximation : 359.999
    include  "sensor/include/lsi#dna727#D.poly"
  end sensor
  
  
  ```

  ou 

  ```
  begin sensor
    Response_name : "SENSOR RESPONSE"
    Response_type : "THEORETICAL" # optionel
    Transfer_function_type : "POLYNOMIAL"
    Input_unit :      # parmi les unités listées dans $PZ/unit_list
    Output_unit :  V  # En général des Volt 
    Valid_Frequency_Unit : "Hz"
    Lower_Bound_Approximation : 0.
    Upper_Bound_Approximation : 359.999
    Number_of_coefficients    : 2		# ces 3 lignes en lieu et place de la référence
     1.                               # au fichier du répertoire include
     72.
  end sensor
  ```


  4.2 - ana_filter
  --------------

  ```
  begin ana_filter
    Response_name : "AMPLIFIER_FILTER"
    Response_type : "THEORETICAL" #  optionel
    Transfer_function_type : "ANALOG"
    Input_unit : "V"				# parmi les unités listées dans $PZ/unit_list
    Output_unit : "V"			# parmi les unités listées dans $PZ/unit_list
    sensitivity : 1.0
    Number_of_zeroes : 0
    Number_of_poles : 0
  end ana_filter
  ```

  ou  (si plusieurs étages pour un même filtre)

  ```
  begin ana_filter
    Response_name : "AMPLIFIER_FILTER"
    Response_type : "THEORETICAL" #  optionel
    Transfer_function_type : "ANALOG"
    Input_unit : "V" 
    Output_unit : "V" 
    Sensitivity : 16.
    Transfer_normalization_frequency : 1.
  end ana_filter
  ```

  ```
  begin stage
    Response_name : "AMPLIFIER_FILTER"
    Response_type : "THEORETICAL" #  optionel
    Transfer_function_type : "ANALOG"
    Input_unit : "V"
    Output_unit : "V"
    Sensitivity : 1.
    Transfer_normalization_frequency : 1. #  optionel
   Number_of_zeroes : 1
   0.000000e+00 0.000000e+00
   Number_of_poles : 6
   -6.756750e-02  0.000000e+00
   -2.400000e+01  0.000000e+00
   -7.416000e+00  2.282549e+01
   -7.416000e+00 -2.282549e+01
   -1.941600e+01  1.410741e+01
   -1.941600e+01 -1.410741e+01
  end stage
  ```

  


  4.3 - digitizer
  -------------

  ```
  begin digitizer
    Response_name : "DIGITIZER" 
    Response_type : "THEORETICAL" #  optionel
    Transfer_function_type : "AD_CONVERSION"
    Input_unit : "V"			parmi les unité listées dans $PZ/unit_list
    Output_unit : "COUNTS"		parmi les unité listées dans $PZ/unit_list
    Output_sampling_interval : 6.2500000e-05
    Sensitivity : 107374182.4 #  counts/V (optionnel) ==> LSB = 9.31 nV
  end digitizer
  ```

  


  4.4 - dig_filter
  --------------

  Le pas d'échantillonnage en entrée de la 1ère section doit être
  égal au pas d'échantillonnage de sortie de l'étage de numérisation.

  16000 --> 2000

  ```
  begin digital_filter
    Response_name : "DECIMATION" 
    Response_type : "THEORETICAL" 
    Transfer_function_type :"FIR_SYM_1"/"FIR_SYM_2"/"FIR_ASYM"/"IIR_PZ"/"IIR_POLY"
    Input_sampling_interval : 6.2500000e-05
    Decimation_factor : 8
    Output_sampling_interval : 5.0000000e-04
    include "dig_filter/include/osiris.dec8.dat"
  end digital_filter 
  ```

  2000 --> 1000

  ```
  begin stage
    Response_name : "DECIMATION" 
    Response_type : "THEORETICAL" 
    Transfer_function_type :"FIR_SYM_1"/"FIR_SYM_2"/"FIR_ASYM"/"IIR_PZ"/"IIR_POLY"
    Input_sampling_interval : 5.0000000e-04
    Decimation_factor : 2
    Output_sampling_interval : 1.0000000e-03
    include "dig_filter/include/osiris.dec2.dat"
  end stage 
  ```

  1000 --> 500

  ```
  begin stage
    Response_name : "DECIMATION" 
    Response_type : "THEORETICAL" 
    Transfer_function_type : "FIR_SYM_1"/"FIR_SYM_2"/"FIR_ASYM"/"IIR_PZ" /"IIR_POLY"
    Input_sampling_interval : 1.0000000e-03
    Decimation_factor : 2
    Output_sampling_interval : 2.0000000e-03
    include "dig_filter/include/osiris.dec2.dat"
  end stage
  ```

   

  Les types de fonctions de transfert IIR_PZ et IIR_POLY peuvent impliquer d'autres mots-clés.


  4.5 - Fichier unit_list
  ---------------------

  Contenu actuel du fichier $PZ/unit_list, donnant la correspondance entre colonnes de gauche à droite,

  * **Code de l'unité**: Le code de l'unité utilisé dans les fichier de $PZ
  * **Code de l'unité (norme SEED)**: Le code de l'unité selon la norme SEED
  * **Description de l'unité**: Description de l'unité (utilisé dans la bloquette 34)

  

  ```
  M**3/M**3 M**3/M**3 Volumetric strain
  RADIANS RADIANS radians
  V/M/S V/M/S volt per meter per second (analogical seismometer sensitivity)
  MV MV milli-Volt
  PA PA Pascal
  S S second
  V/M/S2 V/M/S**2 volt per meter per second squared (analogical accelerometer sensitivity)
  CM CM centimeter
  COUNTS COUNTS digital counts
  T T Tesla
  COUNTS/V COUNTS/V count per volt
  DEG DEGREES degrees (angle)
  RAD/S RAD/S radian per second
  G G gramm
  M M meter
  HZ HZ Hertz
  KG KG kilogramm
  KM KM kilometer
  M/S2 M/S**2 meter per second squared (acceleration)
  RAD/S**2 RAD/S**2 radian per second squared
  C C centigrad degrees
  V V volt
  UV UV micro-Volt
  M/S M/S meter per second (velocity)
  DEGREES DEGREES degrees (angle)
  NANO-M NANO-M nano-metre
  A A Ampere
  BIT BIT binary unity (0/1)
  M/S/S M/S**2 meter per second per second (acceleration)
  RAD RADIANS radians
  VOLT V volt
  MICRO-M MICRO-M micro-meter (micron)
  MM MM millimeter
  ```

  


  4.6 - Fichier sensor_list
  -----------------------

  Extrait du fichier $PZ/sensor_list, donnant la correspondance entre, colonnes de gauche à droite,

  * **Modèle de capteur**: Le modèle du capteur (Ex: CMG-3ESP-90-80-2000)
  * **Super-modèle du capteur**: Le super modèle (Ex: CMG- 3ESP)
  * **Type de capteur**: VEL, ACC, PRES, TEMP, WIND, ...
  * **Largeur de bande**: (LP/CP/XX)
  * **Descriptif du capteur**: Le reste de la ligne constiue la description du capteur

  ```
  KS2000BH-60S KS2000 VEL LP KS2000BH 60 s seismometer
  STS-2.V2-120-1500 STS-2 VEL LP 2nd generation STS2 seismometer 1500 V/(m/s)
  E-2PD E-2PD PRES LP OAS High Pressure Hydrophon
  AC23.V2 AC23 ACC LP AC-23 GeoSig 2nd generation accelerometer
  KS2000M-30S KS2000 VEL LP KS2000M 30 s seismometer
  CMG-3ESP CMG-3ESP VEL LP CMG3-ESP 120s - 50 Hz seismometer
  PT1000OCA PT1000 TEMP XX Temperature sensor with OCA design
  CMG-3ESP-90-80-2000 CMG-3ESP VEL LP CMG3-ESP 90 s - 80 Hz seismomet
  ```

  
